#!/usr/bin/env python3

from distutils.core import setup

setup(name='phast',
      version='0.1',
      description='Phast is a reStructuredText-based media-focused static site generator that renders to HTML and Gopher.',
      author='S.W. Black',
      author_email='yam655@gmail.com',
      url='https://gitlab.com/yam655/phast',
      packages=['phast', ],
      scripts=['scripts/phast']
     )


