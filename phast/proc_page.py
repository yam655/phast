#!/usr/bin/env python3

from pathlib import Path

from .out_rst import rst
from .out_gmi import rst2gemini
from .out_gophermap import gophermap



def process(source, bucket):
    #raise ValueError(repr(bucket))
    rst_out = rst()
    src = bucket['contents']
    rst_out.add(src)
    html = rst_out.render(path=source)
    source = str(rst_out)
    gmi = rst2gemini(source)
    out = [{
            'rel_path':bucket.get('rel_path'), 
            'is_file': True,
            'html':html, 
            'source': source, 
            'gmi':str(gmi),
            'gopher': source}]
    return out

