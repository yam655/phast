#!/usr/bin/env python3

from pathlib import Path

from .out_rst import rst
from .out_gophermap import gophermap



def process(source, bucket):
    #raise ValueError(repr(bucket))
    rst_out = rst()
    src = bucket['contents']
    rst_out.add(src)
    html = rst_out.render(path=source)
    gopher = gophermap()
    gopher.add(src)
    out = [{
            'rel_path':bucket.get('rel_path'), 
            'is_file': True,
            'html':html, 
            'source':str(rst_out), 
            'gopher':str(gopher)}]
    return out

