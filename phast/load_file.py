#!/usr/bin/env python3

from .config import find_config_path, get_cache
from .config import rel_from_root, get_base_name, unconf
from .load_folder import dep_add, dep_changed
from .meta_utils import ignore_path

def find_something(path, something, folder_conf, dflt):
    name = path.name
    
    if folder_conf is not None:
        found = None
        if name in folder_conf:
            s = folder_conf[name]
            found = s.get(something)
        if found is not None:
            found = found.strip()
    if found:
        return found
    return dflt

def sensible_path_title(source):
    if not source.suffix:
        return '{}'.format(source.name.replace('_',' '))
    return '{} ({})'.format(source.stem.replace('_',' '), source.suffix[1:])


def load(source, data):
    if not source.is_file() or ignore_path(source):
        return None
    if data is None:
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']
    parent = source.parent

    rel = rel_from_root(source)
    data['rel_path'] = rel

    cache = get_cache()
    folder = cache.get(str(source.parent), {})
    changed = False

    if dep_changed(source, deps):
        dep_add(source, deps)
        data['copy'] = (source, rel)
    if 'conf' in folder:
        if dep_changed(parent, deps, secondhand=folder.get('deps',{})):
            dep_add(parent, deps, secondhand=folder.get('deps',{}))
            conf = folder['conf'].get(source.name,{})
            if 'hide' not in conf:
                data['abstract'] = find_something(source, 'abstract', conf, None)
                data['title'] = find_something(source, 'title', conf, sensible_path_title(source))
            else:
                data['hide'] = True
    if not data.get('title'):
        data['title'] = sensible_path_title(source)

    return data, changed


