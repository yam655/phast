
from .config import get_cache

def showcase_transcript(bucket, source, out, localcache):
    transcript = None
    if not transcript and 'transcript' in bucket.get('folder', {}):
        transcript = bucket['folder']['transcript']

    conf = bucket.get('conf', {})
    cache = get_cache()
    shorts = {}
    for f in conf:
        n = conf[f]
        if 'in-showcases' in n:
            if 'transcript' in n.get('in-showcases').splitlines():
                shorts[n.get('short', f)] = f

    if shorts or transcript:
        out.section('Transcript')
        out.nl()

    if transcript:
        out.add(transcript)

    if shorts:
        out.nl()
        out.add('Download:')
        out.nl()
        for lshort in shorts:
            lfilename = shorts[lshort]
            if isinstance(lfilename, str):
                link = source / lfilename
                brief = conf[lfilename]
            else:
                link = lfilename
                brief = localcache[lfilename]
                lfilename = link.name
            title = brief.get('title', lfilename)
            abstract = brief.get('abstract', '')
            cntx = '    - {}'
            if lshort not in title and lshort not in abstract:
                cntx = '    - ({}) {}'.format(lshort, '{}')
            out.rel_link(link, title, abstract, only_title=True, # absolute
                            is_file=True, add_suffix=False, context=cntx)
        out.nl()

