#!/usr/bin/env python3

from pathlib import Path
from configparser import ConfigParser

from .brief import get_brief
from .config import rel_from_root, verbose
from .cmd_render import render_from
from .load_audio import load_audio
from .load_ly import load_ly
from .meta_utils import ignore_path

def update(path):
    folder_conf = ConfigParser()
    folder_conf.optionxform = lambda option: option
    folder_conf.read(path)
    bestaudio = None
    bestimage = None
    mapping = {}
    prefix = None
    present = set()
    poem = None
    if 'poem' in folder_conf:
        poem = folder_conf['poem']['source']
    
    if 'folder' not in folder_conf:
        folder_conf['folder'] = {}
    folder = folder_conf['folder']
    audio_abstract = None
    some_brief = None
    for f in path.parent.glob('*'):
        present.add(f.name)
        if not f.is_file() or ignore_path(f) or f.stem == 'folder':
            continue
        suffixes = ''
        if f.suffixes:
            suffixes = ''.join(f.suffixes)[1:]
        mapping.setdefault(suffixes, [])
        mapping[suffixes].append(f.name)
        my_conf = None
        if f.name in folder_conf:
            my_conf = folder_conf[f.name]
            if 'isostamp' in my_conf:
                del my_conf['isostamp']
            if 'artifact_name' in my_conf and 'title' not in my_conf:
                my_conf['title'] = my_conf['artifact_name']
                del my_conf['artifact_name']
            if 'artifact_description' in my_conf and 'abstract' not in my_conf:
                my_conf['abstract'] = my_conf['artifact_description']
                del my_conf['artifact_description']
        if f.suffix in ('.mp3', '.flac', '.ogg', '.spx'):
            if bestaudio is None:
                bestaudio = f
            else:
                myPri = ['.spx', '.ogg', '.mp3', '.flac'].index(bestaudio.suffix)
                lastPri = ['.spx', '.ogg', '.mp3', '.flac'].index(f.suffix)
                if myPri < lastPri:
                    bestaudio = f
            d = {}
            if 'audio' not in folder_conf or f.name not in folder_conf:
                load_audio(d, f)
            if f.name not in folder_conf:
                folder_conf[f.name] = {}
            summary = folder_conf[f.name]
            some_brief = summary
            if d.get('title') and not summary.get('title'):
                summary['title'] = d['title']
            if d.get('abstract') and not summary.get('abstract'):
                summary['abstract'] = d['abstract']
            if 'audio' not in folder_conf:
                audio_abstract = d.get('abstract')
                d2 = {}
                audio = d.get('audio')
                for ak in audio:
                    if ak == 'poem':
                        continue
                    if isinstance(audio[ak], str):
                        d2[ak] = audio[ak]
                    else:
                        d2[ak] = '\n'.join(audio[ak]).strip()
                    if ak == 'TITLE' and 'title' not in folder:
                        folder['title'] = audio[ak]
                folder_conf['audio'] = d2
            default_block = {}
            if 'DEFAULT' in folder_conf:
                default_block = folder_conf['DEFAULT']
            if my_conf:
                for k1 in folder_conf['audio']:
                    if k1 in my_conf and k1 not in default_block:
                        del my_conf[k1]
            folder_conf[f.name]['type'] = 'audio'
            folder_conf[f.name]['in-showcases'] = 'audio'
        elif f.suffix in ('.png', '.jpeg', '.jpg', '.gif', '.svg'):
            if bestimage is None:
                bestimage = f
            elif len(f.suffixes) > 2:
                myPri = ['.front', '.cover', '.back'].index(bestimage.suffixes[0])
                lastPri = ['.front', '.cover', '.back'].index(f.suffixes[0])
                if myPri < lastPri:
                    bestimage = f
            elif len(bestimage.suffixes) == 1:
                myPri = ['.svg', '.jpeg', '.jpg', '.png', '.gif'].index(bestimage.suffix)
                lastPri = ['.svg', '.jpeg', '.jpg', '.png', '.gif'].index(f.suffix)
                if bestimage.stem == 'back':
                    bestimage = f
                elif f.stem == 'back':
                    pass
                elif myPri < lastPri:
                    bestimage = f
            folder_conf[f.name]['type'] = 'image'
            folder_conf[f.name]['in-showcases'] = 'image'
        elif suffixes == 'txt':
            if f.name == 'folder.txt':
                prefix = True
        elif suffixes in ('lyrics.txt', 'song'):
            folder_conf[f.name] = {'in-showcases': 'lyrics'}
            if 'showcase_lyrics' not in folder:
                folder['showcase_lyrics'] = f.name
        elif suffixes == 'poem.txt':
            if poem is None:
                poem = f.name
        elif f.suffix == '.ly':
            brief = {}
            load_ly(brief, f)
            print("Brief:", f, repr(brief))
            if f.name not in folder_conf:
                folder_conf[f.name] = {}
            summary = folder_conf[f.name]
            if not summary.get('title') and brief.get('title'):
                summary['title'] = brief['title']
            if not summary.get('abstract') and brief.get('abstract'):
                summary['abstract'] = brief['abstract']
            summary['in-showcases'] = 'audio'
            some_brief = summary

        elif f.suffix in ('.mma', '.vtt', '.mid', '.midi', 
                          '.kar', '.mmpz', '.wav'):
            brief = get_brief(f)
            if brief is None:
                brief = {}
            if f.name not in folder_conf:
                folder_conf[f.name] = {}
            summary = folder_conf[f.name]
            if not summary.get('title') and brief.get('title'):
                summary['title'] = brief['title']
            if not summary.get('abstract') and brief.get('abstract'):
                summary['abstract'] = brief['abstract']
            summary['in-showcases'] = 'audio'
    if 'title' not in folder:
        if some_brief and some_brief.get('title'):
            folder['title'] = some_brief['title']
    if 'abstract' not in folder:
        if some_brief and some_brief.get('abstract'):
            folder['abstract'] = some_brief['abstract']

    for m in mapping:
        if len(mapping[m]) == 1:
            folder_conf[mapping[m][0]]['short'] = m
        else:
            for fm in mapping[m]:
                folder_conf[fm]['short'] = fm

    if bestaudio:
        folder['showcase_audio'] = bestaudio.name
    elif 'showcase_audio' in folder:
        del folder['showcase_audio']
    if bestimage:
        folder['showcase_picture'] = bestimage.name
    elif 'showcase_picture' in folder:
        del folder['showcase_picture']
    if not 'abstract' in folder and audio_abstract:
        folder['abstract'] = audio_abstract
    present.add('folder')
    present.add('audio')
    present.add('poem')
    present.add('redirect')
    present.add('DEFAULT')
    for k in list(folder_conf.keys()):
        if k not in present:
            if '.poem.' in k and 'source' in folder_conf[k]:
                poem = {'source': folder_conf[k]['source']}
                folder_conf['poem'] = poem
                del folder_conf[k]
            elif not k.startswith('hide:'):
                print('WARNING:', k)

    with open(path, 'w') as configout:
        folder_conf.write(configout)


def cmd_update(args):
    do_recurse = args.recurse
    do_force = args.force
    
    try:
        render_from(Path('.').resolve())
    except:
        pass
    gs = 'folder.conf'
    if do_recurse:
        gs='**/folder.conf'
        if do_force:
            print('ERROR: --force and --recurse are mutually incompatible.')
            sys.exit(1)
    elif do_force:
        rel = rel_from_root(Path('.').resolve())
        if not rel or rel in ('.', 'p', 's'):
            if not rel or rel == '.':
                print('Error: Unable to --force in phast document root.')
            else:
                print('Error: Unable to --force in "{}".'.format(rel))
            sys.exit(1)
        fc = Path('./folder.conf')
        if not fc.exists():
            fc.write_text("")
    tot = 0
    for fc in Path('.').glob(gs):
        verbose('Updating:', str(fc))
        update(fc.resolve())
        tot += 1
    print('{} files updated.'.format(tot))
    return 0
