#!/usr/bin/env python3

from pathlib import Path
from configparser import ConfigParser

from .config import rel_from_root, verbose
from .cmd_render import render_from
from .meta_utils import ignore_path
from .update import update_folder_conf

def update(path):
    folder_conf = ConfigParser()
    folder_conf.optionxform = lambda option: option
    folder_conf.read(path)
    update_folder_conf(path, folder_conf)
    with open(path, 'w') as configout:
        folder_conf.write(configout)


def cmd_update(args):
    do_recurse = args.recurse
    do_force = args.force
    
    gs = 'folder.conf'
    if do_recurse:
        gs='**/folder.conf'
        if do_force:
            print('ERROR: --force and --recurse are mutually incompatible.')
            sys.exit(1)
    elif do_force:
        rel = rel_from_root(Path('.').resolve())
        if not rel or rel in ('.', 'p', 's'):
            if not rel or rel == '.':
                print('Error: Unable to --force in phast document root.')
            else:
                print('Error: Unable to --force in "{}".'.format(rel))
            sys.exit(1)
        fc = Path('./folder.conf')
        if not fc.exists():
            fc.write_text("")
    tot = 0
    for fc in Path('.').glob(gs):
        verbose('Updating:', str(fc))
        update(fc.resolve())
        tot += 1
    print('{} files updated.'.format(tot))
    return 0

