#!/usr/bin/env python3

from pathlib import Path

from .meta_utils import ignore_path
from .config import find_config_path, get_briefs, find_config_path

def brief_bits(bucket):
    bits = None
    if not bucket.get('is_file') and bucket.get('conf'):
        bits = {}
        for c in bucket['conf']:
            if ':' in c:
                continue
            s = c.rsplit('.', 1)
            if len(s) < 2:
                continue
            bits.setdefault(s[1], [])
            bits[s[1]].append(c)
    return bits

def save_brief(path, bucket):
    if not isinstance(path, str):
        path = str(path)
    index = get_briefs()
    bname = path
    brief = {
                'title': bucket.get('title'), 
                'abstract': bucket.get('abstract'),
                'rel_path': bucket.get('rel_path'),
                'add_suffix': bucket.get('add_suffix'),
                'categories': bucket.get('categories'),
                'is_file': bucket.get('is_file'),
                'hidden': bucket.get('hidden')
            }
    for b in brief:
        v = brief[b]
        if isinstance(v, (list, tuple)):
            brief[b] = v[0]
    bits = brief_bits(bucket)
    if bits:
        brief['bits'] = bits
    index[bname] = brief
    return brief


def get_brief(path):
    if not isinstance(path, str):
        path = str(path)
    if not path or path[0] != '/':
        path = str(find_config_path().parent / path)
    briefs = get_briefs()
    bname = path
    brief = briefs.get(bname)
    return brief

