#!/usr/bin/env python3

from pathlib import Path

from .meta_utils import ignore_path
from .config import find_config_path, get_cache



def save_brief(path, bucket):
    if not isinstance(path, str):
        path = str(path)
    cache = get_cache()
    bname = 'brief:' + path
    brief = {
                'title': bucket.get('title'), 
                'abstract': bucket.get('abstract'),
                'rel_path': bucket.get('rel_path'),
                'is_file': bucket.get('is_file')
            }
    cache[bname] = brief
    return brief

def get_brief(path):
    if not isinstance(path, str):
        path = str(path)
    cache = get_cache()
    bname = 'brief:' + path
    brief = cache.get(bname)
    return brief

