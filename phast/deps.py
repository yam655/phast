#!/usr/bin/env python3

# import glob
# from pathlib import Path
# from configparser import ConfigParser
# import re

# from .config import rel_from_root, unconf, get_product, save_product, get_service, save_service, get_cache
# from .config import get_keyword, save_keyword, get_category, save_category
# from .meta_utils import ignore_path, clean_gen_path
# from .load_rst import find_title_rst


def dep_add(path, where, *, secondhand=None):
    p = str(path)
    if secondhand is None:
        if path.exists():
            where[p] = path.stat().st_mtime
        elif p in where:
            del where[p]
    else:
        if p not in where:
            where[p] = 0
        for k in secondhand:
            if secondhand[k] > where[p]:
                where[p] = secondhand[k]
    return path


def dep_changed(path, where, *, secondhand=None):
    p = str(path)
    if secondhand is None:
        if not path.exists():
            if p in where:
                return True
            else:
                return False
        elif p not in where:
            return True
        elif path.stat().st_mtime > where[p]:
            return True
        return False
    else:
        changed = False
        if p not in where:
            where[p] = 0
        for k in secondhand:
            if secondhand[k] > where[p]:
                changed = True
                break
        return changed


