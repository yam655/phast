#!/usr/bin/env python3


def showcase_service(folder, out, localcache=None, hide_price=None):
    used = set()
    used.add('title')
    used.add('abstract')
    used.add('notes')
    used.add('ordinal')
    used.add('announcement')
    used.add('field')
    title = folder.get('title', '???')

    t = folder.get('abstract')
    if t and '|' in t:
        out.section(t.split('|',2)[1])

    price = folder.get('price')
    if hide_price and price is not None:
        out.list_or_not('Price', hide_price, split_char='|')
    elif price is not None:
        out.list_or_not('Price', price, split_char='|')
    used.add('price')

    out.list_or_not('Minimum', folder, 'min')
    used.add('min')
    out.list_or_not('Availability', folder, 'availability')
    used.add('availability')

    out.list_or_not('Released As', folder, 'released_as')
    used.add('released_as')
    out.list_or_not('Released On', folder, 'released_on')
    used.add('released_on')

    out.add_links(folder, 'stream_url', 'Stream')
    used.add('stream_url')
    out.add_links(folder, 'download_urls', 'Download')
    used.add('download_urls')
    out.add_links(folder, 'purchase_urls', 'Purcase',
                    extra = folder.get('purchase_price'))
    used.add('purchase_urls')
    used.add('purchase_price')
    #out.add_link_as('Download As', folder, 'download_as')
    #out.add_link_as('Purchase As', folder, 'purchase_as',
    #        extra = folder.get('product/purchase_price'))

    out.list_or_not('Total tracks', folder, 'tracks')
    used.add('tracks')
    out.list_or_not('Duration', folder, 'audio/duration')
    used.add('audio/duration')
    out.list_or_not('Duration', folder, 'duration')
    used.add('duration')
    out.list_or_not('Poets', folder, 'poets')
    used.add('poets')
    out.list_or_not('Artists', folder, 'artists')
    used.add('artists')

    out.list_or_not('Standard License', folder, 'license')
    used.add('license')
    out.list_or_not('Alternate Licenses', folder, 'alt_license', split_char='|', split_context='(Fee: {})')
    used.add('alt_license')

    out.list_or_not('Announcement', folder, 'announcement')

    used.add('inside')
    used.add('inside/as')
    used.add('image')
    used.add('image.txt')
    used.add('image.txt/body')
    used.add('image.alt')
    out.nl()

    first = True
    for x in folder:
        if x in used or x and x[0] == '_':
            continue
        if first:
            out.add('.. Additional metadata')
            out.nl()
            first = False
        print('WARNING: service {}; unrecognized attribute: {}'.format(title, x))
        txt = x.replace('_',' ').replace('/', ' ').title()
        out.list_or_not(txt, folder, x)

    if 'inside' in folder:
        out.section(folder.get('inside/as','Inside Look'))
        out.add(folder['inside'])
        out.nl()

    status_transcription = None
    if 'transcription' in folder:
        out.nl()
        status_transcription = folder['transcription']
        out.add('Transcription status: ' + status_transcription)

    if 'image' in folder or 'image.txt' in folder:
        out.section('Album Images')
        out.covers(graphic=folder.get('image'),
                    text=folder.get('image.txt'),
                    text_body=folder.get('image.txt/body'),
                    alt=folder.get('image.alt'), embed=True)


def showcase_option(folder, name, out, localcache=None, hide_price=None):
    used = set()
    used.add('title')
    used.add('abstract')
    used.add('notes')
    used.add('ordinal')
    used.add('announcement')
    used.add('description')
    used.add('group')

    title = folder.get('title')
    if not title:
        return
    out.add('* {}'.format(folder.get('title', name)))
    out.nl()

    t = folder.get('description')
    if t:
        out.nl()
        out.under(t, amount=4)
        out.nl()

    price = folder.get('price')
    if hide_price and price is not None:
        out.list_or_not('Price', hide_price,
                    context = '    {}', split_char='|')
    elif price is not None:
        out.list_or_not('Price', price,
                    context = '    {}', split_char='|')
    used.add('price')

    out.list_or_not('Limits', folder, 'limits',
                    context = '    {}')
    used.add('limits')
    out.list_or_not('Frequency', folder, 'frequency',
                    context = '    {}')
    used.add('frequency')
    out.list_or_not('Length', folder, 'length',
                    context = '    {}')
    used.add('length')
    out.list_or_not('Music Genre', folder, 'genre/music')
    used.add('genre/music')
    out.list_or_not('Audio Genre', folder, 'genre/audio')
    used.add('genre/audio')
    out.list_or_not('Released By', folder, 'released_by')
    used.add('released_by')

    out.list_or_not('Released As', folder, 'released_as')
    used.add('released_as')
    out.list_or_not('Released On', folder, 'released_on')
    used.add('released_on')

    out.add_links(folder, 'stream_url', 'Stream')
    used.add('stream_url')
    out.add_links(folder, 'download_urls', 'Download')
    used.add('download_urls')
    out.add_links(folder, 'purchase_urls', 'Purcase',
                    extra = folder.get('purchase_price'))
    used.add('purchase_urls')
    used.add('purchase_price')
    #out.add_link_as('Download As', folder, 'download_as')
    #out.add_link_as('Purchase As', folder, 'purchase_as',
    #        extra = folder.get('product/purchase_price'))

    out.list_or_not('Total tracks', folder, 'tracks')
    used.add('tracks')
    out.list_or_not('Duration', folder, 'audio/duration')
    used.add('audio/duration')
    out.list_or_not('Duration', folder, 'duration')
    used.add('duration')
    out.list_or_not('Poets', folder, 'poets')
    used.add('poets')
    out.list_or_not('Artists', folder, 'artists')
    used.add('artists')

    out.list_or_not('Standard License', folder, 'license')
    used.add('license')
    out.list_or_not('Alternate Licenses', folder, 'alt_license', split_char='|', split_context='(Fee: {})')
    used.add('alt_license')

    out.list_or_not('Announcement', folder, 'announcement')

    used.add('inside')
    used.add('inside/as')
    used.add('image')
    used.add('image.txt')
    used.add('image.txt/body')
    used.add('image.alt')
    out.nl()

    first = True
    for x in folder:
        if x in used or x and x[0] == '_':
            continue
        if first:
            out.add('.. Additional metadata')
            out.nl()
            first = False
        print('WARNING: option {}; unrecognized attribute: {}'.format(title, x))
        txt = x.replace('_',' ').replace('/', ' ').title()
        out.list_or_not(txt, folder, x)

    if 'inside' in folder:
        out.section(folder.get('inside/as','Inside Look'))
        out.add(folder['inside'])
        out.nl()

    status_transcription = None
    if 'transcription' in folder:
        out.nl()
        status_transcription = folder['transcription']
        out.add('Transcription status: ' + status_transcription)

    if 'image' in folder or 'image.txt' in folder:
        out.section('Album Images')
        out.covers(graphic=folder.get('image'),
                    text=folder.get('image.txt'),
                    text_body=folder.get('image.txt/body'),
                    alt=folder.get('image.alt'), embed=True)


