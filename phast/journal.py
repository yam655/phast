


def process_file_source(*, path, meta, title, mod):
    addendum = []
    mod.output = addendum
    title_block = mod.find_title_block(path, title)

    if 'showcase_product' in meta:
        ptype = meta['showcase_product']
        if ptype == 'music':
            mod.list_or_not('Music Genre: ', meta,
                            'genre/music', addendum)
            mod.list_or_not('Released By: ', meta,
                            'music/released_by', addendum)
            mod.list_or_not('Released As: ', meta,
                            'music/released_as', addendum)
            mod.add_links('product/download_urls',
                            ':Download Album:', meta, addendum)
            price = meta.get('product/purchase_price')
            mod.add_links('product/purchase_urls', ':Purchase Album:', meta, addendum, price=price)

            product_image(meta, 'product/image', 'product/image.txt', 'product/image.alt', path.parent, addendum, embed_img=True)

            if 'music/tracks' in meta:
                t = 'Track Listing'
                addendum.append('')
                addendum.append('*' * len(t))
                addendum.append(t)
                addendum.append('*' * len(t))
                addendum.append('')
                for line in meta['music/tracks'].splitlines():
                    addendum.append(line.strip())
                addendum.append('')

            status_transcription = None
            if 'music/transcription' in meta:
                addendum.append('')
                status_transcription = meta['music/transcription']
                addendum.append(textwrap.fill(
                    'Transcription status: ' + status_transcription))

    if 'showcase_audio' in meta:
        addendum.append('')
        t = 'Audio'
        if 'genre/music' in meta:
            t = 'Music'
        addendum.append('*' * len(t))
        addendum.append(t)
        addendum.append('*' * len(t))
        addendum.append('')

        if 'vtt' in meta['audio/formats'] or 'chapters.vtt' in meta['audio/formats']:
            if 'ogg' in meta['audio/formats'] or 'mp3' in meta['audio/formats'] or 'flac' in meta['audio/formats']:
                addendum.append('')
                addendum.append('.. raw:: html')
                addendum.append('')

                addendum.append('   <figure><video style="max-height:100%;max-width:100%" controls="controls"')
                addendum.append('    poster="{}"'.format('poster.png'))
                #if 'audio/cover' in meta:
                    #addendum.append('    poster="{}"'.format(meta['audio/cover']))
                #elif 'audio/poster' in meta:
                    #addendum.append('    poster="{}"'.format(meta['audio/poster']))
                addendum.append('    Your browser does not support the <code>video</code> element.')
                for ext in ('ogg', 'mp3', 'flac'):
                    fname = meta.get('audio/format/' + ext)
                    if fname:
                        addendum.append('   <source src="{}" type="audio/{}">'.format(fname, ext))
                vttfile = meta.get('audio/format/vtt')
                if vttfile:
                    addendum.append('   <track kind="captions" default src="{}" srclang="en" label="English">'.format(vttfile))
                vttchfile = meta.get('audio/format/chapters.vtt')
                if vttchfile:
                    addendum.append('   <track kind="chapters" label="Chapters" src="{}" srclang="en">'.format(vttchfile))
                addendum.append('   </video></figure>')
                addendum.append('')
        else:

            if 'ogg' in meta['audio/formats'] or 'mp3' in meta['audio/formats'] or 'flac' in meta['audio/formats']:
                addendum.append('')
                addendum.append('.. raw:: html')
                addendum.append('')
                addendum.append('   <audio style="width:100%" controls="controls">')
                addendum.append('      Your browser does not support the <code>audio</code> element.')
                for ext in ('ogg', 'mp3', 'flac'):
                    fname = meta.get('audio/format/' + ext)
                    if fname:
                        addendum.append('   <source src="{}" type="audio/{}">'.format(fname, ext))
                addendum.append('   </audio>')
                addendum.append('')

        if 'audio/formats' in meta:
            addendum.append(':Download:')
            spaces = ' ' * (len(addendum[-1]) + 2)
            dash = '-'
            formats = meta['audio/formats']
            if len(formats) == 1:
                dash = ' '
            for ext in formats:
                fname = meta['audio/format/' + ext]
                desc = meta.get('audio/format/' + fname, ext)
                addendum.append('{}{} `{} <{}>`_'.format(spaces, dash, desc, fname))
            addendum.append('')

        for k in meta:
            hn = human_names.get(k, -1)
            hout = meta[k]
            if isinstance(hout, str):
                n = hout.splitlines()
                if len(n) > 1:
                    hout = n
            if not hout:
                continue
            if k == 'audio/released_on':
                ro1 = meta[k].split('/',1)[-1].replace('_', ' ')
                ro2 = '/' + meta[k]
                hout = ['`{} <{}>`_'.format(ro1, ro2)]
            elif hn == 'Album' and 'audio/released_on' in meta:
                hn = None
            if hn == -1:
                if k.startswith('audio/format'):
                    pass
                elif k.startswith('audio/'):
                    print("'{}': '{}',".format(k, k.split('/',1)[-1].replace('_', ' ').title()), path)
            elif hn is not None:
                if isinstance(hout, (list, tuple, set)):
                    addendum.append(':{}:'.format(hn))
                    spaces = ' ' * (len(hn) + 2)
                    dash = '-'
                    if len(hout) == 1:
                        dash = ' '
                    for h2 in hout:
                        addendum.append('{}{} {}'.format(spaces, dash, h2))
                    addendum.append('')
                else:
                    addendum.append(':{}: {}'.format(hn, hout))
        addendum.append('')

    if 'text/lyrics' in meta:
        addendum.append('')
        addendum.append('******')
        addendum.append('Lyrics')
        addendum.append('******')
        addendum.append('')
        for line in meta['text/lyrics'].splitlines():
            addendum.append('| ' + line)
        addendum.append('')
    contents = ""
    source_path = path
    if path and path.exists():
        contents = path.read_text()
    return title_block + contents + '\n'.join(addendum)


