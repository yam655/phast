#!/usr/bin/env python3

from pathlib import Path

from .meta_utils import ignore_path
from .config import find_config_path, get_cache
from .brief import save_brief
from . import load_audio
from . import load_rst
from . import load_file
from . import load_ly
#from . import load_p
#from . import load_s
from . import load_site
from . import load_song
from . import load_folder
from . import load_directory


def load_bucket_something(source, *, old=None):
    if not source:
        return None, None
    changed = None
    if old is None:
        cache = get_cache()
        old = cache.get(source)

    for mod in (load_rst, load_audio, load_ly, load_song,
                #load_p, load_s, 
                load_site, load_folder, load_directory, load_file):
        loaded = mod.load(source, old)
        if not loaded:
            continue
        bucket, changed = loaded
        k = str(source)
        if bucket is None:
            continue
        if changed or k not in cache:
            cache[str(source)] = bucket
            save_brief(str(source), bucket)
            if k not in cache:
                raise RuntimeError('Can not find just saved item.')
            if 'title' not in bucket:
                raise ValueError("{} missing title".format(repr(mod)))
            if 'rel_path' not in bucket:
                raise ValueError("{} missing rel_path".format(repr(mod)))
            break
#    if k == '/Users/yam655/src/yam655/j/2019-01/07_r2052':
#        raise ValueError(repr([changed,bucket]))
    if bucket is None or changed is None:
        return None, None
    return bucket, changed


def build_list(source, *, updated=None):
    if not source:
        raise ValueError('source is None')
    if isinstance(source, str):
        source = Path(source).expanduser()
    else:
        source = source.expanduser()
    recurse = [source]
    deferred = []
    listout = []
    while recurse:
        source = recurse.pop()
        # print(source)
        bucket, changed = load_bucket_something(source)
        if not bucket:
            continue
        if changed and updated:
            updated.append(source)
        listout.append(source)
        if source.is_dir():
            for rfile in source.glob('*'):
                if ignore_path(rfile):
                    continue
                if rfile.is_file():
                    deferred.append(rfile)
                else:
                    recurse.append(rfile)
        # print(recurse, deferred)
        if not recurse and deferred:
            recurse = deferred
            deferred = []
    return listout


def build_upstream(source, *, updated=None):
    root = find_config_path().parent
    listout = build_bucket_list(source.expanduser(), updated=updated)
    parent = source.parent
    while parent != root:
        source = parent
        bucket, updated = load_bucket_something(source)
        if updated:
            updated.append(source)
        listout.append(source)
    return listout

