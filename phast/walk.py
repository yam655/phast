#!/usr/bin/env python3

from pathlib import Path
import magic

from .meta_utils import ignore_path
from .config import find_config_path, get_cache, verbose
from .brief import save_brief
from .deps import dep_add, dep_changed
from . import load_media
from . import load_rst
from . import load_file
from . import load_ly
from . import load_site
from . import load_song
from . import load_folder
from . import load_directory


def load_bucket_something(source, *, old=None):
    if not source:
        return None, None
    changed = None
    if old is None:
        cache = get_cache()
        old = cache.get(source)
    consider = True

    if not source.is_dir():
        ignore = source / "IGNORE"
        if ignore.exists():
            return None, None

    for mod in (load_rst, load_media, load_ly, load_song,
                load_site, load_folder, load_directory, load_file):
        loaded = mod.load(source, old)
        if not loaded:
            continue
        bucket, changed = loaded
        k = str(source)
        if bucket is None:
            continue
        if changed or k not in cache:
            cache[str(source)] = bucket
            if k not in cache:
                raise RuntimeError('Can not find just saved item.')
            if 'title' not in bucket:
                raise ValueError("{} missing title".format(repr(mod)))
            if 'rel_path' not in bucket:
                raise ValueError("{} missing rel_path".format(repr(mod)))
        if mod is load_folder:
            consider = False
        break

    if bucket is not None and consider:
        source_parent = source.parent
        fconf = source_parent / 'folder.conf'
        parent_got = None
        if fconf.exists():
            if 'deps' not in bucket:
                bucket['deps'] = {}
                changed = True
            deps = bucket['deps']
            if dep_changed(source_parent / 'folder.conf', deps):
                dep_add(source_parent, deps)
                changed += 1
                parent_got = load_folder.load(source_parent, None)
        if parent_got is not None:
            if 'rel_path' not in bucket:
                raise ValueError(repr(bucket))
            parent_bucket = parent_got[0]
            override = parent_bucket.get('_override',{}).get(source.name)
            for k in override:
                v = override[k].strip()
                if not v:
                    continue
                if v == '-':
                    del bucket[k]
                else:
                    bucket[k] = override[k]
    if bucket is None or changed is None:
        return None, None
    save_brief(str(source), bucket)
    return bucket, changed


def build_list(source, *, updated=None):
    if not source:
        raise ValueError('source is None')
    if isinstance(source, str):
        source = Path(source).expanduser()
    else:
        source = source.expanduser()
    recurse = [source]
    listout = []

    nrecurse = []
    verbose('Scanning...')
    while recurse:
        source = recurse.pop()
        if source.name[0] in '_.':
            continue
        nrecurse.append(source)
        if source.is_dir():
            for rfile in source.glob('*'):
                recurse.append(rfile)
    verbose('{} files and directories.'.format(len(nrecurse)))
    generated_files = set()

    recurse = nrecurse
    recurse.reverse()
    tot = len(nrecurse)
    act = 0
    while recurse:
        act += 1
        source = recurse.pop()
        srcstr = str(source)
        parent = source.parent
        if len(srcstr) > 70:
            verbose('* {:02d}% ...{:67.67}'.format(act * 100 // tot, srcstr[-67:]), end='\r')
        else:
            verbose('* {:02d}% {:70.70}'.format(act * 100 // tot, srcstr), end='\r')
        bucket, changed = load_bucket_something(source)
        if not bucket:
            continue

        if changed and 'keywords' in bucket:
            keywords = bucket.get('keywords',[])
            for keyword in keywords:
                whichid = Path(f'keyword:{keyword.lower()}')

                if whichid not in generated_files:
                    generated_files.add(whichid)
                    if updated is not None:
                        updated.append(whichid)
                    listout.append(whichid)
                    allid = Path('keyword:__ALL__')
                    if allid not in generated_files:
                        generated_files.add(allid)
                        if updated is not None:
                            updated.append(allid)
                        listout.append(allid)

        if changed and 'categories' in bucket:
            categories = bucket.get('categories',[])
            for category in categories:
                whichid = Path(f'category:{category.lower()}')

                if whichid not in generated_files:
                    generated_files.add(whichid)
                    if updated is not None:
                        updated.append(whichid)
                    listout.append(whichid)
                    allid = Path('category:__ALL__')
                    if allid not in generated_files:
                        generated_files.add(allid)
                        if updated is not None:
                            updated.append(allid)
                        listout.append(allid)


        conf = bucket.get('conf',{}) 
        if changed and conf:
            for which in ('product', 'service', 'review'):
                if 'title' in conf.get(which, {}):
                    whichid = '{}:{}'.format(which, conf[which]['title']).lower()
                    whichid = Path(whichid)
                    if whichid not in generated_files:
                        generated_files.add(whichid)
                        if updated is not None:
                            updated.append(whichid)
                        listout.append(whichid)
                        allid = Path('{}:__ALL__'.format(which))
                        if allid not in generated_files:
                            generated_files.add(allid)
                            if updated is not None:
                                updated.append(allid)
                            listout.append(allid)

        if changed and updated is not None:
            updated.append(source)
        listout.append(source)
    verbose(' {:10} {:70}'.format('', ''), end='\r')
    return listout


def build_upstream(source, *, updated=None):
    root = find_config_path().parent
    listout = build_bucket_list(source.expanduser(), updated=updated)
    parent = source.parent
    while parent != root:
        source = parent
        bucket, updated = load_bucket_something(source)
        if updated:
            updated.append(source)
        listout.append(source)
    return listout

