#!/usr/bin/env python3

from pathlib import Path

from .config import root_path, rel_from_root, get_cache, get_product

def showcase_product(folder, source, out, *, localcache=None):
    used = set()
    used.add('title')
    used.add('abstract')
    used.add('notes')
    used.add('ordinal')
    used.add('announcement')
    used.add('incomplete')
    used.add('category')

    section_name = 'Product'
    t = folder.get('abstract')
    if t and '|' in t:
        section_name = t.split('|',2)[1]
    out.push()

    out.list_or_not('Version', folder, 'version')
    used.add('version')
    out.list_or_not('Platforms', folder, 'platforms')
    used.add('platforms')

    out.list_or_not('Game Genre', folder, 'genre/game')
    used.add('genre/game')
    out.list_or_not('Music Genre', folder, 'genre/music')
    used.add('genre/music')
    out.list_or_not('Audio Genre', folder, 'genre/audio')
    used.add('genre/audio')
    out.list_or_not('Fiction Genre', folder, 'genre/fiction')
    used.add('genre/fiction')
    out.list_or_not('Nonfiction Genre', folder, 'genre/nonfiction')
    used.add('genre/nonfiction')
    out.list_or_not('Genre', folder, 'genre')
    used.add('genre')
    some_genre = None
    for g in 'genre/game', 'genre/music', 'genre/audio', 'genre/fiction', 'genre/nonfiction', 'genre':
        if g in folder:
            some_genre = folder[g]
            break
    if not some_genre and 'category' in folder:
        out.list_or_not('Category', folder, 'category')

    out.list_or_not('Released By', folder, 'released_by')
    used.add('released_by')

    out.list_or_not('Released As', folder, 'released_as')
    used.add('released_as')
    out.list_or_not('Released On', folder, 'released_on')
    used.add('released_on')

    out.add_links(folder, 'jam_url', 'Jam URL')
    used.add('jam_url')
    out.add_links(folder, 'stream_url', 'Stream')
    used.add('stream_url')
    out.add_links(folder, 'download_urls', 'Download')
    used.add('download_urls')
    out.add_links(folder, 'purchase_urls', 'Purcase',
                    extra = folder.get('purchase_price'))
    used.add('purchase_urls')
    used.add('purchase_price')
    #out.add_link_as('Download As', folder, 'download_as')
    #out.add_link_as('Purchase As', folder, 'purchase_as',
    #        extra = folder.get('product/purchase_price'))

    labels = {
        'bash': 'GNU BASH',
        'sh': 'Bourne Shell',
        'linux': 'Linux',
        'macos': 'macOS',
        'python': 'Python',
        'windows': 'Windows',
    }
    for k in labels.keys():
        used.add(k)

    first = True
    cache = get_cache()
    for thing in 'bash', 'sh', 'linux', 'macos', 'python', 'windows':
        filename = folder.get(thing)
        if not filename:
            continue
        if first:
            out.nl()
            out.add('*Downloads:*')
            out.nl()
            first = False
        used.add(thing)
        label = labels[thing]
        thing = cache.get(str(filename))
        if thing:
            rel_path = thing.get('rel_path')
            out.rel_link(rel_path, Path(filename).name, thing.get('abstract'), context=f'- {"{}"} (for {label})')
    out.nl()

    out.list_or_not('Total tracks', folder, 'tracks')
    used.add('tracks')
    out.list_or_not('Duration', folder, 'audio/duration')
    used.add('audio/duration')
    out.list_or_not('Duration', folder, 'duration')
    used.add('duration')
    out.list_or_not('Poets', folder, 'poets')
    used.add('poets')
    out.list_or_not('Album Artist', folder, 'album_artist')
    used.add('album_artist')
    out.list_or_not('Artists', folder, 'artists')
    used.add('artists')
    out.list_or_not('Copyright Year', folder, 'year')
    used.add('year')
    out.list_or_not('License', folder, 'license')
    used.add('license')

    see_also = folder.get('see_also')
    if see_also:
        if len(see_also) == 1:
            sa_entry = see_also.pop()
            alsoprod = get_product(sa_entry)
            if alsoprod:
                alsorel = Path(alsoprod['rel_path'])
                out.rel_link(alsorel, alsoprod.get('title', alsorel.name), alsoprod.get('abstract'), context=f':See Also: {"{}"}', only_title=True)
            else:
                print (f'WARNING: see_also entry missing product definition: {sa_entry}')
                out.add(f':See Also: {sa_entry}')
        else:
            out.nl()
            out.add(':See Also:')
            out.nl()
            for sa_entry in see_also:
                alsoprod = get_product(sa_entry)
                if alsoprod:
                    alsorel = Path(alsoprod['rel_path'])
                    out.rel_link(alsorel, alsoprod.get('title', alsorel.name), alsoprod.get('abstract'), context=f'   - {"{}"}', only_title=True)
                else:
                    print (f'WARNING: see_also entry missing product definition: {sa_entry}')
                    out.add(f'- {sa_entry}')
        out.nl()

    out.list_or_not('Announcement', folder, 'announcement')

    used.add('see_also')
    used.add('transcription')
    used.add('inside')
    used.add('inside/as')
    used.add('image')
    used.add('image.txt')
    used.add('image.txt/body')
    used.add('image.alt')
    used.add('image.thumb')
    used.add('image.gif')
    used.add('hidden')
    out.nl()

    first = True
    for x in folder:
        if x in used or x and x[0] == '_':
            continue
        if first:
            out.add('.. Additional metadata')
            out.nl()
            first = False
        print('WARNING: product {} unrecognized attribute: {}'.format(folder['title'], x))
        txt = x.replace('_',' ').replace('/', ' ').title()
        out.list_or_not(txt, folder, x)

    out.pop(section=section_name)

    if 'inside' in folder:
        out.section(folder.get('inside/as','Inside Look'))
        out.add(folder['inside'])
        out.nl()

    status_transcription = None
    if 'transcription' in folder:
        out.nl()
        status_transcription = folder['transcription']
        out.add('Transcription status: ' + status_transcription)

    graphic = folder.get('image')
    text = folder.get('image.txt')
    # raise ValueError('"source" should be defined. "{}" "{}"'.format(folder.get('image'), folder.get('image.txt')))
    if graphic or text:
        root = root_path()
        out.section('Images')
        if graphic:
            if isinstance(graphic, str) or not graphic.is_absolute():
                if not Path(graphic).exists():
                    raise ValueError('"{}" does not exist'.format(graphic))
        else:
            graphic = None
        if text:
            if isinstance(text, str) or not text.is_absolute():
                if not Path(text).exists():
                    print (repr(folder))
                    raise ValueError('"{}" does not exist'.format(text))
        else:
            text = None
        out.covers(graphic=graphic, text=text,
                    text_body=folder.get('image.txt/body'),
                    alt=folder.get('image.alt'), embed=True)

