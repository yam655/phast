#!/usr/bin/env python3

def showcase_product(bucket, out, localcache):
    folder = bucket.get('folder', {})
    if 'showcase_product' in folder:
        ptype = folder['showcase_product']
        if ptype == 'music':
            out.list_or_not('Music Genre', folder, 'genre/music')
            out.list_or_not('Audio Genre', folder, 'genre/audio')
            out.list_or_not('Released By', folder, 'music/released_by')
            out.list_or_not('Released As', folder, 'music/released_as')
            out.add_links('Download Album', folder, 'product/download_urls')
            out.add_links('Purchase Album', folder, 'product/purchase_urls',
                    extra = folder.get('product/purchase_price'))

            if 'product/image' in folder or 'product/image.txt' in folder:
                out.section('Album Images')
                out.covers(graphic=folder.get('product/image'),
                            text=folder.get('product/image.txt'),
                            text_body=folder.get('product/image.txt/body'),
                            alt=folder.get('product/image.alt'), embed=True)

            if 'music/tracks' in folder:
                out.section('Track Listing')
                out.add(meta['music/tracks'])
                out.nl()

            status_transcription = None
            if 'music/transcription' in folder:
                out.nl()
                status_transcription = folder['music/transcription']
                out.add(textwrap.fill(
                    'Transcription status: ' + status_transcription))
        else:
            raise ValueError('unknown showcase_product: {}'.format(ptype))

