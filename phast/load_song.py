#!/usr/bin/env python3

import re

from .config import rel_from_root
from .deps import dep_add, dep_changed

SONG_METADATA = re.compile(r'^\s*:([^:]+):\s*(.+)\s*$', re.MULTILINE) 


def clean_song_line(inl):
    out = []
    # FIXME: does not properly join words
    for word in inl.split():
        if not word:
            continue
        if word in ('--', '__'):
            continue
        if word.startswith(r'/'):
            continue
        out.append(word)
    return ' '.join(out)

def song2lyrics(contents):
    out = []
    last_empty = True
    for inl in contents.splitlines():
        inl = inl.strip()
        if inl and inl[0] in '%#:':
            inl = ''
        if not inl:
            if not last_empty:
                last_empty = True
                out.append('')
            continue
        last_empty = False
        out.append(clean_song_line(inl))
    return '\n'.join(out)


def songTags(song_contents, tags):
    matches = SONG_METADATA.findall(song_contents)
    for m in matches:
        k = re.sub(r'\s+','', m[0].upper())
        if k in tags:
            if not isinstance(tags[k], list):
                box = [tags[k]]
                tags[k] = box
            tags[k].append(m[1])
        else:
            tags[k] = m[1]
    return tags

songFolderMapping = {
    'ALBUM': 'Album',
    'ALBUMTITLE': 'Album',
    'BOOKTITLE': 'Album',
    'TITLE': 'Title',
    'POET': 'Lyricist',
    'COLLABORATORS': 'Lyricist',
    'POSTDATE': '',
    'TAGS': 'genre/music',
    'DEMO': '',
    'TRACKNUMBER': 'TrackNumber',
    'ORDINAL': 'TrackNumber',
    'COMMENT': 'Comment',
}

def song_to_folder(song, source):
    fldr = {}
    for k in song.keys():
        v = song[k]
        if not v:
            continue
        if isinstance(v, str):
            v = [v.strip()]
        if k not in songFolderMapping:
            raise ValueError('"{}" : <{}> did not have a Folder mapping in {}'.format(k, v, source))
        k1 = songFolderMapping[k]
        if k1:
            if k1 not in fldr:
                fldr[k1] = v
            else:
                fldr[k1].extend(v)
    return fldr


def load_song(data, path):

    meta = {}
    contents = path.read_text()
    songTags(contents, meta)
    data['song'] = meta
    data['showcase'] = song_to_folder(meta, path)

    title = meta.get('TITLE')
    if title is not None and not isinstance(title, str):
        title = title[0].strip()
    if not title:
        title = path.stem.replace('_',' ')
    data['title'] = title

    artist = None
    if 'POET' in meta:
        artist = meta.get('POET')
        if not isinstance(artist, str):
            artist = '; '.join()

    album = None
    if 'BOOKTITLE' in meta:
        album = meta.get('BOOKTITLE')
    elif 'ALBUMTITLE' in meta:
        album = meta.get('ALBUMTITLE')
    elif 'ALBUM' in meta:
        album = meta.get('ALBUM')
    if album and not isinstance(album, str):
        album = album[0]

    lyrics = song2lyrics(contents).strip()
    if lyrics:
        meta['lyrics'] = lyrics
        meta['poem'] = lyrics

    abstract = []
    abstract.append('|Lyrics| song lyrics')
    if artist:
        abstract.append('by')
        abstract.append(artist)
    if album:
        abstract.append('from')
        abstract.append('"{}"'.format(album))
    if not artist and not album:
        if 'POSTDATE' in meta:
            abstract.append('posted on')
            abstract.append(meta['POSTDATE'])

    data['abstract'] = ' '.join(abstract)
    if album is not None:
        data['product-info'] = {
            'title': album,
        }
        if meta.get('TRACKNUMBER') is not None:
            data['product-info']['ordinal'] = meta.get('TRACKNUMBER')

def load(source, data):
    if source.is_dir():
        return None
    if not source.suffix in ['.song']:
        return None

    if data is None:
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']
    changed = False
    data['is_file'] = True
    rel = rel_from_root(source)
    data['rel_path'] = rel

    if dep_changed(source, deps):
        changed = True
        dep_add(source, deps)
        load_song(data, source)

    return data, changed



