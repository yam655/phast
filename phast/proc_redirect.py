#!/usr/bin/env python3

import argparse
from pathlib import Path
import sys
from configparser import ConfigParser
import time
import yaml
import json
import shutil
import os

from .config import get_config
from .out_rst import rst
from .out_gmi import gemini
from .out_gophermap import gophermap

def make_redirect_gophermap(title, rel):
    content = []
    if not title:
        title1 = 'Requested document moved!'
    else:
        title1 = '"{}" moved!'.format(title)

    content.append('')
    content.append(title1)
    content.append('#' * len(title1))
    content.append('')
    content.append('1{}\t/{}'.format(title, rel))
    content.append('')

    gophermap = '\n'.join(content)
    return gophermap

def make_redirect_gemini(title, rel):
    content = []
    if not title:
        title1 = 'Requested document moved!'
    else:
        title1 = '"{}" moved!'.format(title)

    content.append('')
    content.append(title1)
    content.append('#' * len(title1))
    content.append('')
    content.append('=> /{}\t{}'.format(rel, title))
    content.append('')
    content.append('----')
    content.append('')
    content.append('=> /\t🏡Home')

    gophermap = '\n'.join(content)
    return gophermap



def make_redirect_html(title, url):
    if not title:
        title1 = 'Requested document moved!'
    else:
        title1 = '&quot;{}&quot; moved!'.format(title)
    content = []
    content.append('<!doctype html>\n<html><head>\n<title>')
    content.append(title1)
    content.append('</title>\n')
    content.append('<link rel="stylesheet" href="/css/minimal.css" type="text/css"/>')
    content.append('<link rel="stylesheet" href="/css/local.css" type="text/css"/>')
    content.append('<meta http-equiv="Refresh" content="5; url={}">\n'.format(url))
    content.append('</head><body>\n')
    logo_img = get_config('logo-img')
    logo_alt = get_config('logo-alt')
    title = get_config('title')
    url = get_config('url')
    content.append('<section class="pageheader">')
    content.append('<header class="header">')
    content.append('<span>\n\n<span class="parentlinks">')
    content.append('<a href="/"><img style="height: 1em; width: auto;" ')
    content.append('src="{}"'.format(logo_img))
    content.append(' alt="{}"></a>'.format(logo_alt))
    content.append('\n</span>\n')
    content.append('<span>{}</span>\n'.format(title))
    content.append('<div style="float: right">\n')
    content.append('<form method="get" action="https://www.google.com/search" id="searchform">')
    content.append('<input name="sitesearch" value="{}" type="hidden">'.format(url))
    content.append('<input name="q" value="" id="searchbox" size="16" '
                    'maxlength="255" type="text" placeholder="search">')
    content.append('</form>\n</div>\n</span>\n\n</header>\n</section>')
    content.append('<h1>')
    content.append(title1)
    content.append('</h1>\n<p>You will be redirected to\n')
    content.append('<a href="{}">{}</a>\n'.format(url, url))
    content.append('in five seconds.</a></p>\n</body></html>\n')

    content.append('''
<hr/>

<section class="align-center">
<a href="/">🏡Home</a>
</section>
''')
    html = ''.join(content)
    return html


def process(source, data):
    out = []
    redirect = data['redirect']
    title = data.get('title')
    if not redirect or not title:
        raise ValueError('Missing required field: {}'.format(repr(data)))
    rel = data['rel_path']
    base_url = get_config('url')
    if base_url[-1] == '/':
        base_url = base_url[:-1]
    url = '{}/{}'.format(base_url, rel)
    html = make_redirect_html(title, url)
    gmi = make_redirect_gemini(title, rel)
    
    for redirect_file in redirect:
        redirect_file = Path(redirect_file)
        if redirect_file.name != '__DIR__':
            out.append({'rel_path': redirect_file, 
                        'is_file': True,
                        'gmi':str(gmi),
                        'html': html})
        else:
            redirect_file1 = redirect_file.parent
            gophermap = make_redirect_gophermap(title, rel)
            out.append({'rel_path': redirect_file1, 
                        'is_file': False,
                        'gophermap': gophermap,
                        'index.gmi':str(gmi),
                        'html': html})
        
    return out

