#!/usr/bin/env python3

from urllib.parse import urlparse
import codecs
from pathlib import Path
import fnmatch
import re

from .config import find_config_path, get_config

_ignore_endswith = None
_ignore_fnmatch = None
_ignore_absolute = None
_ignore_suffix = None

MAGIC_DAY_CONSTANT = 24 * 60 * 60 * 0.0001 # aka 8.64

def _prep_patterns():
    global _ignore_endswith
    global _ignore_fnmatch
    global _ignore_absolute
    global _ignore_suffix
    if _ignore_endswith is None:
        _ignore_endswith = set()
        _ignore_fnmatch = set()
        _ignore_absolute = set()
        _ignore_suffix= set()
        docroot = find_config_path().parent
        pignore = docroot / '.phast-ignore'
        if pignore.exists():
            for x in pignore.read_text().splitlines():
                x = x.strip()
                if not x or x[0] == '#':
                    continue
                if x[0] == '/':
                    _ignore_absolute.add(str(docroot / x[1:]))
                elif x.startswith('**'):
                    _ignore_endswith.add(x[2:])
                elif x.startswith('*.') and '/' not in x and '[' not in x and '*' not in x[2:]:
                    _ignore_suffix.add(x[1:])
                elif '*' in x:
                    _ignore_fnmatch.add(x)
                if '/' in x:
                    ignore_endswith.add(x)
        _ignore_fnmatch.add('*/[._]*')
        _ignore_endswith.add('~')
        _ignore_endswith.add('#')
        _ignore_endswith.add('/gophermap')
        _ignore_endswith.add('/folder.txt')
        _ignore_endswith.add('/folder.conf')
        _ignore_endswith.add('/project.json')
        _ignore_endswith.add('/service.json')
        for suffix in ('.title', '.abstract', '.category', '.keyword',
                '.genre', '.autokw', '.bak', '.mdwn', '.js'):
            _ignore_suffix.add(suffix)

def ignore_path(rfile):
    global _ignore_endswith
    global _ignore_fnmatch
    global _ignore_absolute
    global _ignore_suffix
    if isinstance(rfile, Path):
        rsfile = str(rfile)
    else:
        rsfile = rfile
        rfile = Path(rfile)
    if _ignore_endswith is None:
        _prep_patterns()
    if rsfile in _ignore_absolute:
        return True
    if rfile.suffix in _ignore_suffix:
        return True
    for pat in _ignore_endswith:
        if rsfile.endswith(pat):
            return True
    for pat in _ignore_fnmatch:
        if fnmatch.fnmatchcase(rsfile, pat):
            return True
    return False

def url_label(urls):
    netloc = urlparse(urls).netloc
    if ':' in netloc:
        netloc = netloc.split(':')[-1]
        lead = netloc.split('.',1)
        for lead[0] in ('www', 'w3', 'web'):
            netloc = lead[-1]
    return netloc

def clean_gen_path(what):
    s = what.lower().split(':')
    b1 = re.sub(r'\W+','-',str(codecs.encode(s[1], encoding='US-ASCII', errors='backslashreplace'), encoding='UTF-8').replace(' ','_'))
    x = 0
    while x < len(b1) and b1[x] in '-_':
        x += 1
    if x == len(b1):
        return None
    y = -1
    while b1[y] in '-_':
        y -= 1
    if y == -1:
        b2 = b1[x:]
    else:
        b2 = b1[x:y+1]
    return '{}/{}'.format(s[0][0], b2)

def secs_to_time(just_time_in_seconds, *, no_days = False):
    j = just_time_in_seconds
    secs = int(j % 60)
    j = j // 60
    mins = int(j % 60)
    j = j // 60
    hours = int(j % 24)
    j = j // 24
    days = int(j)
    if days > 0 and no_days:
        raise ValueError('Value was too large, and days were excluded.')
    r = [f'{secs:02}', f'{mins:02}']
    if hours:
        r.append(f'{hours:02}')
    if days:
        r.append(f'{days:02}')
    r.reverse()
    return ':'.join(r)

def j_to_isodatetime(path, *, mode=None):
    if mode is None:
        mode = get_config('journal', 'revision_mode')
    if not isinstance(path, str):
        path = str(path)
    if mode == 'day':
        # Thought: Would this be better with a Match object?
        # Con: More lines of code.
        date = re.sub(r'.*j/([0-9]{4})-([0-9]{2})/([0-9]{2})_r.*',
                      r'\1-\2-\3', path)
        tpart = re.sub(r'.*j/[0-9]{4}-[0-9]{2}/[0-9]{2}_r([0-9]{4}).*',
                       r'\1', path)
        tpart = int(tpart) * MAGIC_DAY_CONSTANT
        time = secs_to_time(tpart)
        ex = '{}T{}'.format(date, time)
    elif mode == 'time' or mode == 'just-date':
        ex = re.sub(r'.*j/([0-9]{4})-([0-9]{2})/([0-9]{2})_r([0-9]{2})([0-9]{2}).*',
                    r'\1-\2-\3T\4:\5', path)
    else:
        raise ValueError('No way to convert "{}" to a datetime in "{}" mode'
                         .format(path, mode))
    if '/' in ex:
        raise ValueError('Could not find stamp for "{}" ({})'.format(path, ex))
    zed_time = get_config('journal', 'zed_time')
    if zed_time is None:
        zed_time = False
    elif zed_time.title() in ('On', 'True', '1'):
        zed_time = True
    elif zed_time.title() in ('Off', 'False', '0'):
        zed_time = False
    else:
        raise ValueError('Unknown "zed_time" setting: {}'.format(zed_time))
    if zed_time:
        return ex + 'Z'
    else:
        raise ValueError('Real timezones not currently supported.')
    return ex

def j_to_stamp(path, *, mode=None):
    if mode is None:
        mode = get_config('journal', 'revision_mode')
    if not isinstance(path, str):
        path = str(path)
#   if 'day':
#       # Thought: Would this be better with a Match object?
#       # Con: More lines of code.
#       date = re.sub(r'.*j/([0-9]{4})-([0-9]{2})/([0-9]{2})_r.*',
#                     r'\1-\2-\3', path)
#       tpart = re.sub(r'.*j/[0-9]{4}-[0-9]{2}/[0-9]{2}_r([0-9]{4}).*',
#                      r'\1', path)
#       tpart = int(tpart) * MAGIC_DAY_CONSTANT
#       time = secs_to_time(tpart)
#       return '{} {}'.format(date, time)
    if mode == 'just-date':
        ex = re.sub(r'.*j/([0-9]{4})-([0-9]{2})/([0-9]{2})'
                     '_r([0-9]{2})([0-9]{2}).*',
                    r'\1-\2-\3', path)
    elif mode == 'time':
        ex = re.sub(r'.*j/([0-9]{4})-([0-9]{2})/([0-9]{2})'
                     '_r([0-9]{2})([0-9]{2}).*',
                    r'\1-\2-\3 \4:\5', path)
    else:
        ex = re.sub(r'.*j/([0-9]{4})-([0-9]{2})/([0-9]{2})_(r[0-9]{4}).*',
            r'\1-\2-\3 (\4)', path)
    if '/' in ex:
        raise ValueError('Could not find stamp for "{}"'.format(path))
    return ex


