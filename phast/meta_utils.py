#!/usr/bin/env python3

from pathlib import Path

def ignore_path(rfile):
    if isinstance(rfile, Path):
        rfile = Path(rfile)
    if rfile.name[0] in '._' or rfile.name[-1] in '~#':
        return True
    if rfile in ('gophermap', 'folder.txt', 'folder.conf', 
                 'project.json', 'service.json'):
        return True
    if rfile.suffix and rfile.suffix in ('.title', '.abstract', 
                '.genre', '.autokw', '.bak', '.mdwn', '.js'):
        return True
    return False

def url_label(urls):
    netloc = urlparse(urls).netloc
    if ':' in netloc:
        netloc = netloc.split(':')[-1]
        lead = netloc.split('.',1)
        for lead[0] in ('www', 'w3', 'web'):
            netloc = lead[-1]
    return netloc


