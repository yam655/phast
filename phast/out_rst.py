#!/usr/bin/env python3

from urllib.parse import urlparse
from docutils import core, io
from pathlib import Path
import sys
import time
import os
import json
import argparse
from configparser import ConfigParser, ExtendedInterpolation
import shutil
import codecs
import textwrap

from .config import get_config, base_names, rel_from_root
from .brief import get_brief
from .meta_utils import url_label

class rst:
    def __init__(self):
        self.url = get_config('url')
        if self.url and self.url[-1]:
            self.url = self.url[:-1]
        self.journal_text = None
        self.products_text = None
        self.services_text = None
        self.users_text = None
        footer = set(get_config('sections').split())

        self.home_text = get_config('labels:unicode', 'home', 
                                        default=base_names['home'])
        if 'j' in footer:
            self.journal_text = get_config('labels:unicode', 'journal', 
                                        default=base_names['journal'])
        if 'p' in footer:
            self.products_text = get_config('labels:unicode', 'products', 
                                        default=base_names['products'])
        if 's' in footer:
            self.services_text = get_config('labels:unicode', 'services', 
                                        default=base_names['services'])
        if 'u' in footer:
            self.users_text = get_config('labels:unicode', 'users', 
                                        default=base_names['users'])
        self.body = []
        self.body.append('')

    def enhance_parts(self, parts, rel):
        urlout = self.url
        parentlinks = ''
        url = None
        if urlout:
            url = urlparse(urlout)
        if not rel or str(rel) == '.':
            parentlinks = '<span>{}</span>'.format(get_config('title'))
        else:
            dirs = str(rel).split('/')
            pl = []
            for x in range(len(dirs)):
                pl.append('<a href="{}/{}">{}</a>'.format(urlout, 
                            '/'.join(dirs[:x+1]), dirs[x]))
            if pl:
                parentlinks = '/ ' + '\n/ '.join(pl) + ' /'
            else:
                parentlinks = '/'

        ret = []
        ret.append(parts['head_prefix'])
        ret.append(parts['head'])
        ret.append(parts['stylesheet'])
        ret.append(parts['body_prefix'])
        ret.append('''
<section class="pageheader">
<header class="header">
<span>

<span class="parentlinks">''')
        logo_img = get_config('logo-img')
        logo_alt = get_config('logo-alt')

        if logo_img:
            ret.append('<a href="/"><img style="height: 1em; width: auto;" ')
            ret.append('src="{}{}"'.format(self.url, logo_img))
            if logo_alt:
                ret.append(' alt="{}"'.format(logo_alt))
            ret.append('></a>')
        else:
            ret.append(self.home_text)
        ret.append('</span>\n')
        ret.append(parentlinks)
        # ret.append('''<div style="float: right">
#<form method="get" action="https://www.google.com/search" id="searchform">
#  <input name="sitesearch" value="{}" type="hidden">
#  <input name="q" value="" id="searchbox" size="16" maxlength="255" type="text" placeholder="search">
#</form>
#</div>'''.format(self.url))
        ret.append('</span>\n\n</header>\n</section>\n')

        ret.append(parts['body_pre_docinfo'])
        ret.append(parts['docinfo'])
        ret.append(parts['body'])

        ret.append('\n<hr/>\n')

        ret.append('<section class="align-center">')

        bar = ""
        for name, path in [(self.home_text,'/'), 
                            (self.journal_text,'/j/'),
                            (self.products_text,'/p/'),
                            (self.services_text,'/s/'),
                            (self.users_text,'/u/')]:
            if name:
                ret.append('{}<a href="{}">{}</a>'.format(bar, path, name))
                bar = '| '
        ret.append('</section>')
        ret.append(parts['body_suffix'])

        whole = '\n'.join(ret)
        parts['whole'] = whole
        return whole

    def html_parts(self, file_contents, destination_path=None, *, source_path=None):
        if isinstance(file_contents, Path):
            raise ValueError("file_contents should not be a Path")
        url = get_config('url')
        overrides = {'embed_stylesheet': False,
                     'stylesheet_path': '{}/css/local.css,{}/css/minimal.css'.format(url, url),
                    'initial_header_level': 2,
                     }

        mapping = '''

.. |Audio| replace:: 🎧
.. |Lyrics| replace:: 📔
.. |Sheetmusic| replace:: 🎼
.. |Phlog| replace:: 📓
.. |Done| replace:: 🍳
.. |Early| replace:: 🥚
.. |Gift| replace:: 🎁
.. |Shipped| replace:: 📦

'''
        bucket = [file_contents, mapping]
        parts = core.publish_parts(
            source='\n'.join(bucket), source_path=str(source_path),
            destination_path=destination_path,
            writer_name='html5', settings_overrides=overrides)
        return parts

    def render(self, *, path):
        parts = self.html_parts(str(self), source_path=path)
        rel = rel_from_root(Path(path))
        if rel.name == 'folder.rst':
            rel = rel.parent
        return self.enhance_parts(parts, rel)

    def __str__(self):
        if isinstance(self.body, str):
            return self.body
        self.footer()
        self.body = '\n'.join(self.body)
        return self.body

    def title(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.body.append('#' * len(title))
        self.body.append(title)
        self.body.append('#' * len(title))
        self.body.append('')

    def section(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append('=' * len(title))
        self.body.append(title)
        self.body.append('=' * len(title))
        self.nl()

    def rel_link(self, lrel, ltitle, labstract, *, is_file=False, context=None):
        if isinstance(lrel, str):
            relpath = lrel
            lrel = Path(lrel)
        else:
            relpath = str(lrel)
        lname = lrel.name
        if is_file:
            relpath = relpath + '.html'
            
        if not ltitle or ltitle == lname or lname.replace('_', ' ') == ltitle:
            link = '`{} <{}>`__'.format(lname, relpath)
        else:
            link = '({}) `{} <{}>`__'.format(lname, ltitle, relpath)
        if context:
            self.body.append(context.format(link))
        else:
            self.body.append(link)
        return

    def list_or_not(self, humankey, meta, key=None):
        values = None
        if key is None:
            if isinstance(meta, (list, tuple)):
                values = meta
            elif isinstance(meta, str):
                if meta.strip() == '':
                    values = None
                else:
                    values = [meta]
            elif meta is not None:
                raise ValueError('called with no key and invalid data: {}'.format(repr(meta)))
        else:
            values = meta.get(key)
        if not values:
            return

        if isinstance(values, str):
            values = values.strip().splitlines()
        if len(values) == 1:
            self.body.append(':{}: {}'.format(humankey, values[0]))
        else:
            humankey = humankey.strip()
            spaces = ' ' * (len(humankey) + 2)
            self.body.append(humankey)
            for value in values:
                self.body.append('{}- {}'.format(spaces, value))
            self.nl()
        return

    def under(self, what):
        n = -1
        maxn = -len(self.body)
        while n > maxn:
            if self.body[n].strip():
                break
            n -= 1
        if n == maxn:
            self.body.extend(what.splitlines())
        else:
            first_blank = True
            for x in range(len(self.body[n])):
                if not self.body[n][x].isspace():
                    break
            if x == len(self.body[n]):
                x = 2
                word = ''
            else:
                word = self.body[n].strip().split()[0]
            for w in word:
                if w not in '0123456789-.+*':
                    word = None
            if word is not None:
                x += len(word) + 1
            self.body.append('')
            
            first = True
            for par in what.split('\n\n'):
                if first:
                    first = False
                else:
                    self.body.append('')
                self.body.extend(textwrap.wrap(par, 
                        initial_indent=' '*x, subsequent_indent=' '*x))

    def covers(self, *, graphic=None, text=None, text_body=None, caption=None, alt=None, embed=False):
        embed = None
        if caption and not (graphic and embed):
            self.body.append(textwrap.fill(
                'Caption: ' + caption,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.nl()
            
        if graphic:
            fname = graphic
            self.body.append('- {}\t{}'.format(
                        "Cover Image (graphic):", fname, fname))
        if text:
            fname = text
            embed = fname
            self.body.append('{}- {}\t{}'.format(
                        "Cover Image (text):", fname, fname))
        if embed and graphic:
            self.nl()
            self.body.append('.. figure:: {}'.format(graphic))
            self.body.append('   :width: 50 %')
            if alt:
                self.body.append('   :alt: {}'.format(alt.replace('\n', ' ')))
            if caption:
                self.nl()
                self.add(textwrap.fill(caption,
                    initial_indent=' '*3, subsequent_indent=' '*3))
            self.nl()
            self.add('..')
            self.nl()
        elif alt:
            self.body.append(textwrap.fill(
                'Description of cover image: ' + alt,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.body.append('')

    def _usable_short(self, audio_shorts, suffix, *, avoid=None):
        if suffix in audio_shorts and (avoid is None or audio_shorts[suffix] != avoid):
            return audio_shorts[suffix]
        else:
            psuffix = '.' + suffix
            found = None
            for k in audio_shorts:
                if k.endswith(psuffix) and (avoid is None or audio_shorts[k] != avoid):
                    found = audio_shorts[psuffix]
                    break
            return found

    def playable_audio(self, audio_shorts):
        url = get_config('url')
        if url and url[-1] == '/':
            url = url[:-1]
        poster = get_config('poster')
        poster_url = None
        if url and poster:
            poster_url = '{}/{}'.format(url, poster)
        chapters_vtt = self._usable_short(audio_shorts, 'chapters.vtt')
        vtt = self._usable_short(audio_shorts, 'vtt', avoid=chapters_vtt)
        spx = self._usable_short(audio_shorts, 'spx')
        ogg = self._usable_short(audio_shorts, 'ogg')
        mp3 = self._usable_short(audio_shorts, 'mp3')
        flac = self._usable_short(audio_shorts, 'flac')
        if (vtt or chapters_vtt) and poster_url:
            if spx or ogg or mp3 or flac:
                self.nl()
                self.add('.. raw:: html')
                self.nl()

                self.body.append('   <figure><video '
                            'style="max-height:100%;max-width:100%" '
                            'controls="controls"')
                self.body.append('    poster="{}">'.format(poster_url))
                self.body.append('    Your browser does not support '
                                 'the <code>video</code> element.')
                for fname in (spx, ogg, mp3, flac):
                    if fname:
                        self.body.append(
                            '   <source src="{}" type="audio/{}">'
                            .format(fname, ext))
                if vtt:
                    self.body.append('   <track kind="captions" '
                            'default src="{}" srclang="en" '
                            'label="English">'.format(vtt))
                vttchfile = meta.get('audio/format/chapters.vtt')
                if chapters_vtt:
                    self.body.append('   <track kind="chapters" '
                            'label="Chapters" src="{}" '
                            'srclang="en">'.format(chapters_vtt))
                self.body.append('   </video></figure>')
                self.body.append('')
        else:

            if spx or ogg or mp3 or flac:
                self.nl()
                self.add('.. raw:: html')
                self.nl()
                self.body.append('   <audio style="width:100%" controls="controls">')
                self.body.append('      Your browser does not support the <code>audio</code> element.')
                for fmtfile, fmtmime in [(spx, 'audio/speex'),
                                        (ogg, 'audio/ogg'),
                                        (mp3, 'audio/mpeg'),
                                        (mp3, 'audio/flac')]:
                    if fmtfile:
                        self.body.append('   <source src="{}" type="{}">'
                                        .format(fmtfile, fmtmime))
                self.body.append('   </audio>')
                self.body.append('')

    def add_links(self, meta, metakey, humankey, *, extra=None):
        if meta.get(metakey):
            items = meta[metakey]
            if isinstance(items, str):
                items = items.splitlines()
            if extra:
                extra = ' ({})'.format(extra)
            else:
                price = ''
            if len(items) == 1:
                self.body.append('h{} {}{}\t/URL:{}\t+\t+'.format(humankey, 
                                url_label(items[0]),
                                extra, items[0]))
            else:
                self.body.append(humankey.expandtabs() + extra)
                for urls in items:
                    self.body('h{}\t/URL:{}\t+\t+'.format(
                                url_label(urls), urls))
                self.lf()
        return
    
    def poetry(self, text):
        if not text:
            return
        if isinstance(text, str):
            text = text.splitlines()
        for line in text:
            self.body.append('| {}'.format(line))
    
    def add(self, text):
        if text and not isinstance(text, str):
            raise ValueError(repr(text))
        if text:
            self.body.extend(text.splitlines())

    def nl(self):
        if self.body and self.body[-1]:
            self.body.append('')
        return

    def footer(self):
        self.nl()
        return

