#!/usr/bin/env python3

from urllib.parse import urlparse
from docutils import core, io
from pathlib import Path
import sys
import time
import os
import json
import argparse
from configparser import ConfigParser
import shutil
import codecs
import textwrap
from docutils.utils import column_width

from .config import get_config, base_names, rel_from_root, read_config, get_render_cache
from .brief import get_brief
from .meta_utils import url_label
from .out_text import text_base

class rst(text_base):
    def __init__(self):
        super().__init__()
        self.url = get_config('url')
        if self.url and self.url[-1]:
            self.url = self.url[:-1]
        self._set_footer_text('labels:unicode')
        self._desectioned = False

        self.body = []
        self.body.append('')

    def enhance_parts(self, parts, rel):
        urlout = self.url
        parentlinks = ''
        url = None
        if urlout:
            url = urlparse(urlout)
        if not rel or str(rel) == '.':
            parentlinks = '<span>{}</span>'.format(get_config('title'))
        else:
            dirs = str(rel).split('/')
            pl = []
            for x in range(len(dirs)):
                pl.append('<a href="{}/{}">{}</a>'.format(urlout, 
                            '/'.join(dirs[:x+1]), dirs[x]))
            if pl:
                parentlinks = '/ ' + '\n/ '.join(pl)
            else:
                parentlinks = ''

        rels = str(rel)
        get_render_cache()[rels] = parts['body']

        ret = []
        ret.append(parts['head_prefix'])
        ret.append(parts['head'])
        ret.append('<meta name="viewport" content="width=device-width, initial-scale=1" />')
        ret.append(parts['stylesheet'])
        ret.append(parts['body_prefix'])
        ret.append('''
<section class="pageheader">
<header class="header">
<span>

<span class="parentlinks">''')
        logo_img = get_config('logo-img')
        logo_alt = get_config('logo-alt')

        if logo_img:
            ret.append('<a href="/"><img style="height: 1em; width: auto;" ')
            ret.append('src="{}{}"'.format(self.url, logo_img))
            if logo_alt:
                ret.append(' alt="{}"'.format(logo_alt))
            ret.append('></a>')
        else:
            ret.append(self.home_text)
        ret.append('</span>\n')
        ret.append(parentlinks)
        # ret.append('''<div style="float: right">
#<form method="get" action="https://www.google.com/search" id="searchform">
#  <input name="sitesearch" value="{}" type="hidden">
#  <input name="q" value="" id="searchbox" size="16" maxlength="255" type="text" placeholder="search">
#</form>
#</div>'''.format(self.url))
        ret.append('</span>\n\n</header>\n</section>\n')

        ret.append(parts['body_pre_docinfo'])
        ret.append(parts['docinfo'])
        ret.append(parts['body'])

        ret.append('\n<hr/>\n')

        ret.append('<section class="align-center">')

        bar = ""
        for name, path in [(self.home_text,'/'), 
                            (self.journal_text,'/j/'),
                            (self.products_text,'/p/'),
                            (self.services_text,'/s/'),
                            (self.users_text,'/u/'),
                            (self.reviews_text,'/r/'),
                            (self.categories_text,'/c/'),
                            (self.keywords_text,'/k/')]:
            if name:
                ret.append('{}<a href="{}">{}</a>'.format(bar, path, name))
                bar = '| '
        ret.append('</section>')
        ret.append(parts['body_suffix'])

        whole = '\n'.join(ret)
        parts['whole'] = whole
        return whole

    def html_parts(self, file_contents, destination_path=None, *, source_path=None):
        if isinstance(file_contents, Path):
            raise ValueError("file_contents should not be a Path")
        url = get_config('url')
        overrides = {'embed_stylesheet': False,
                     'stylesheet_path': '/css/local.css,/css/minimal.css',
                    'initial_header_level': 2,
                     }

        config = read_config()
        mapping = []
        if 'replacements' in config:
            replacements = config['replacements']
            mapping.append('')
            for k in replacements:
                v = replacements[k].strip()
                mapping.append('.. |{}| replace:: {}'.format(k.strip(), v))
            mapping.append('')

        bucket = [file_contents, '\n'.join(mapping)]
        parts = None
        try:
            parts = core.publish_parts(
                source='\n'.join(bucket), source_path=str(source_path),
                destination_path=destination_path,
                writer_name='html5', settings_overrides=overrides)
        except:
            print(source_path)
            print(destination_path)
            print('\n'.join(bucket))
            raise ValueError()
        return parts

    def render(self, *, path):
        parts = self.html_parts(str(self), source_path=path)
        rel = rel_from_root(Path(path))
        if rel.name == 'folder.rst':
            rel = rel.parent
        elif rel.suffix == '.rst':
            rel = rel.parent / rel.stem
        return self.enhance_parts(parts, rel)

    def __str__(self):
        if isinstance(self.body, str):
            return self.body
        self.footer()
        self.body = '\n'.join(self.body)
        return self.body

    def rel_link(self, lrel, ltitle, labstract, *, add_suffix=False,
                only_title=False, italic=False,
                is_file=False, context=None, include_parent=False):
        if isinstance(lrel, str):
            relpath = lrel
            lrel = Path(lrel)
        else:
            relpath = str(lrel)
        if lrel.is_absolute():
            if not lrel.exists() and not Path(str(lrel.parent / lrel.stem) + '.rst').exists():
                raise ValueError('Absolute paths should exist: {}'.format(lrel))
            lrel = rel_from_root(lrel)
            relpath = str(lrel)
        lname = lrel.name
        if include_parent:
            lname = '{}/{}'.format(lrel.parent.name, lrel.name)
        if add_suffix and not lrel.suffix:
            relpath = relpath + '.html'

        if context:
            if italic:
                context1 = []
                for c in context.split():
                    if '{}' not in c:
                        context1.append('*{}*'.format(c))
                    else:
                        context1.append(c)
                context = ' '.join(context1)
            elif context[0].isalnum():
               if ':' in context:
                    context = ':' + context 
            elif context[0] in '-+*':
                # bulletted
                pass
        if not ltitle:
            ltitle = lname

        # TODO relative links with '..' would work, were it not for Gopher's lack of support
        if only_title or ltitle == lname or lname.replace('_', ' ') == ltitle:
            link = '`{} </{}>`__'.format(ltitle, relpath)
        else:
            link = '({}) `{} </{}>`__'.format(lname, ltitle, relpath)
        if context:
            self.body.append(context.format(link))
        else:
            self.body.append(link)
        if labstract:
            self.nl()
            self.under(labstract.strip())
            self.nl()
        return

    def desectioned(self, state):
        self._desectioned = state

    def list_or_not(self, humankey, meta, key=None, split_char=None,
                    split_context=None, context=None):
        values = None
        humankey = humankey.strip()
        if key is None:
            values = meta
        else:
            values = meta.get(key)
        if isinstance(values, (list, tuple)):
            pass
        elif isinstance(values, str):
            if values.strip() == '':
                values = None
            else:
                values = values.strip().splitlines()
        elif values:
            if meta != values:
                raise ValueError('called with "{}" having invalid data: {}'.format(repr(meta), repr(values)))
            else:
                raise ValueError('called invalid data: {}'.format(repr(values)))
        if not values:
            return
        if split_char and not split_context:
            split_context = ' ({})'

        if len(values) == 1:
            splt = ''
            if split_char:
                s = values[0].split(split_char,1)
                v = s[0].strip()
                if len(s) > 1:
                    splt = split_context.format(s[1].strip())
            else:
                v = values[0]
            lne = ':{}: {}{}'.format(humankey, v, splt)
            if not context:
                self.body.append(lne)
            else:
                self.body.append(context.format(lne))
        else:
            humankey = humankey.strip()
            lne = ':{}:'.format(humankey)
            if not context:
                self.body.append(lne)
            else:
                lne = context.format(lne)
                self.body.append(lne)
            spaces = ' ' * len(lne)
            for value in values:
                splt = ''
                if split_char:
                    s = value.split(split_char,1)
                    v = s[0].strip()
                    splt = split_context.format(s[1].strip())
                else:
                    v = value.strip()
                self.body.append('{}- {}{}'.format(spaces, v, splt))
            self.nl()
        return

    def covers(self, *, graphic=None, text=None, text_body=None, caption=None, alt=None, embed=False):
        if caption and not (graphic and embed):
            self.body.append(textwrap.fill(
                'Caption: ' + caption,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.nl()
            
        if graphic:
            graphic = Path(graphic)
            if not graphic.is_absolute() and not graphic.exists():
                raise ValueError('Graphics should be absolute paths and exist: {}'.format(graphic))
            fname = graphic
            self.rel_link(fname, None, None, add_suffix=False, is_file=True, # absolute
                            context='- Cover Image (graphic): {}')
        if text:
            text = Path(text)
            if not text.is_absolute() and not text.exists():
                raise ValueError('Graphics should be absolute paths and exist: {}'.format(text))
            fname = text
            self.rel_link(fname, None, None, add_suffix=False, is_file=True, # absolute
                            context='- Cover Image (text): {}')
        if embed and graphic:
            self.nl()
            self.body.append('.. figure:: /{}'.format(self.rel_ref(graphic)))
            self.body.append('   :width: 50 %')
            if alt:
                self.body.append('   :alt: {}'.format(alt.replace('\n', ' ')))
            if caption:
                self.nl()
                self.add(textwrap.fill(caption,
                    initial_indent=' '*3, subsequent_indent=' '*3))
            self.nl()
            self.add('..')
            self.nl()
        elif alt:
            self.body.append(textwrap.fill(
                'Description of cover image: ' + alt,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.body.append('')

    def _usable_short(self, shorts, suffix, *, avoid=None):
        if suffix in shorts and (avoid is None or shorts[suffix] != avoid):
            return shorts[suffix]
        else:
            psuffix = '.' + suffix
            found = None
            for k in shorts:
                if k.endswith(psuffix) and (avoid is None or shorts[k] != avoid):
                    found = shorts[k]
                    break
            return found

    def playable_audio(self, audio_shorts):
        url = get_config('url')
        if url and url[-1] == '/':
            url = url[:-1]
        poster = get_config('poster')
        poster_url = None
        if url and poster:
            poster_url = '{}/{}'.format(url, poster)
        chapters_vtt = self._usable_short(audio_shorts, 'chapters.vtt')
        vtt = self._usable_short(audio_shorts, 'vtt', avoid=chapters_vtt)
        spx = self._usable_short(audio_shorts, 'spx')
        ogg = self._usable_short(audio_shorts, 'ogg')
        mp3 = self._usable_short(audio_shorts, 'mp3')
        flac = self._usable_short(audio_shorts, 'flac')

        need_figure = False
        if (vtt or chapters_vtt) and poster_url:
            need_figure = True

        if spx or ogg or mp3 or flac:
            self.nl()
            self.add('.. raw:: html')
            self.nl()

            if need_figure:
                self.body.append('   <figure><audio '
                            'style="max-height:100%;max-width:100%" '
                            'controls="controls"')
                if poster_url:
                    self.body.append('    poster="{}">'.format(poster_url))
                else:
                    self.body.append('>')
            else:
                self.body.append('   <audio style="width:100%" controls="controls">')
            self.body.append('      Your browser does not support the <code>audio</code> element.')

            for fmtfile, fmtmime in [(spx, 'audio/speex'),
                                     (ogg, 'audio/ogg'),
                                     (mp3, 'audio/mpeg'),
                                     (flac, 'audio/flac')]:
                if fmtfile:
                    self.body.append('   <source src="{}" type="{}">'
                                    .format(fmtfile, fmtmime))

            if poster_url:
                # Without a poster, these are invisible
                if vtt:
                    self.body.append('   <track kind="captions" '
                            'default src="{}" srclang="en" '
                            'label="English">'.format(vtt))
                if chapters_vtt:
                    self.body.append('   <track kind="chapters" '
                            'label="Chapters" src="{}" '
                            'srclang="en">'.format(chapters_vtt))
            if need_figure:
                self.body.append('   </audio></figure>')
            else:
                self.body.append('   </audio>')
            self.body.append('')

    def inline_images(self):
        return True

    def rst_only(self, with_rst, without_rst):
        return with_rst

    def playable_video(self, video_shorts):
        url = get_config('url')
        if url and url[-1] == '/':
            url = url[:-1]
        poster = get_config('poster')
        poster_url = None
        if url and poster:
            poster_url = '{}/{}'.format(url, poster)
        chapters_vtt = self._usable_short(video_shorts, 'chapters.vtt')
        vtt = self._usable_short(video_shorts, 'vtt', avoid=chapters_vtt)
        mp4 = self._usable_short(video_shorts, 'mp4')

        if mp4:
            self.nl()
            self.add('.. raw:: html')
            self.nl()

            self.body.append('   <video '
                            'style="max-height:100%;max-width:100%" '
                            'controls="controls"')
            if poster_url:
                self.body.append('    poster="{}">'.format(poster_url))
            else:
                self.body.append('    >')
            self.body.append('    Your browser does not support '
                                'the <code>video</code> element.')

            for fmtfile, fmtmime in [(mp4, 'video/mp4'),
                                    ]:
                if fmtfile:
                    self.body.append('   <source src="{}" type="{}">'
                                    .format(fmtfile, fmtmime))
            if vtt:
                self.body.append('   <track kind="captions" '
                        'default src="{}" srclang="en" '
                        'label="English">'.format(vtt))
            if chapters_vtt:
                self.body.append('   <track kind="chapters" '
                        'label="Chapters" src="{}" '
                        'srclang="en">'.format(chapters_vtt))
            self.body.append('   </video>')
            self.body.append('')


    def add_links(self, meta, metakey, humankey, *, extra=None, as_par=False):
        if meta.get(metakey):
            items = meta[metakey]
            if isinstance(items, str):
                items = items.splitlines()
            if extra:
                extra = ' ({})'.format(extra)
            else:
                extra = ''
            marker = ':'
            if as_par:
                marker = '*'
            if len(items) == 1:
                link = self._web_link(items[0])
                self.body.append(f'{marker}{humankey}{marker} {link}{extra}')
            else:
                self.body.append(f'{marker}{humankey}{marker}{extra}')
                self.nl()
                spaces = ' ' * (len(humankey) + 2)
                for url in items:
                    link = self._web_link(url, f'{url_label(href)}{extra}')
                    self.body.append(f'{spaces} - {link}')
                self.nl()
        return

    def _web_link(self, href, ltitle=None, *, context=None, extra=None):
        relpath = None
        lrel = None

        ctitle = ltitle
        if ctitle is None:
            ctitle = url_label(href)

        if extra is None:
            extra = ''
        if extra and not extra[0].isspace():
            extra = ' ' + extra
        link = f'`{ctitle} <{href}>`__{extra}'
        if context:
            # No "Italic" support for Gopher
            link = context.format(link)

        return link

    def web_link(self, href, ltitle, labstract, *, context=None, extra=None):
        link = self._web_link(href, ltitle, context=context, extra=extra)
        self.add(link)
        if labstract:
            self.nl()
            self.add(textwrap.fill(labstract,
                    initial_indent=' '*3, subsequent_indent=' '*3))
            self.nl()
        return 


    def html(self, *text):
        self.nl()
        self.add('.. raw:: html')
        self.nl()
        for txt in text:
            self.add(f'   {txt}')
        self.nl()
        return

    def poetry(self, text):
        self._add_lines(text, prefix='| ')

    def force_end(self):
        self.nl()
        self.add('..')
        self.nl()

    def footer(self):
        self.nl()
        return

    def title(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        if self._desectioned:
            raise ValueError('desectioned output should not also use "title"')
        else:
            self.body.append('#' * column_width(title))
            self.body.append(title)
            self.body.append('#' * column_width(title))
        self.nl()

    def subtitle(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        if self._desectioned:
            raise ValueError('desectioned output should not also use "title"')
        else:
            self.body.append(title)
            self.body.append('*' * column_width(title))
        self.nl()

    def section(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        if self._desectioned:
            self.html(f'<h2>{title}</h2>')
        else:
            self.body.append('=' * column_width(title))
            self.body.append(title)
            self.body.append('=' * column_width(title))
        self.nl()

    def subsection(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        if self._desectioned:
            self.html(f'<h3>{title}</h3>')
        else:
            self.body.append(title)
            self.body.append('-' * column_width(title))
        self.nl()


