#!/usr/bin/env python3

from docutils.utils import column_width
import textwrap

from .config import get_config, base_names, rel_from_root

class text_base:
    def __init__(self):
        self.body_stack = []

    def push(self):
        self.body_stack.append(self.body)
        self.body = []

    def pop(self, *, line=None, top=None, section=None):
        if self.body_stack:
            last_out = self.body
            self.body = self.body_stack.pop()
            if last_out:
                if section:
                    self.section(section)
                if top:
                    self.add(top)
                    self.nl()
                self._add_lines(last_out, prefix=line, wrap=False)

    def rst_only(self, with_rst, without_rst):
        return without_rst

    def _set_footer_text(self, labels):
        self.home_text = None
        self.journal_text = None
        self.products_text = None
        self.services_text = None
        self.users_text = None
        self.reviews_text = None
        self.keywords_text = None
        self.categories_text = None

        footer = set(get_config('sections').split())

        self.home_text = get_config(labels, 'home', 
                                        default=base_names['home'])
        if 'j' in footer:
            self.journal_text = get_config(labels, 'journal', 
                                        default=base_names['journal'])
        if 'p' in footer:
            self.products_text = get_config(labels, 'products', 
                                        default=base_names['products'])
        if 's' in footer:
            self.services_text = get_config(labels, 'services', 
                                        default=base_names['services'])
        if 'u' in footer:
            self.users_text = get_config(labels, 'users', 
                                        default=base_names['users'])
        if 'r' in footer:
            self.reviews_text = get_config(labels, 'reviews', 
                                        default=base_names['reviews'])
        if 'k' in footer:
            self.keywords_text = get_config(labels, 'keywords', 
                                        default=base_names['keywords'])
        if 'c' in footer:
            self.categories_text = get_config(labels, 'categories', 
                                        default=base_names['categories'])

    def inline_images(self):
        return False

    def title(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.body.append('#' * column_width(title))
        self.body.append(title)
        self.body.append('#' * column_width(title))
        self.body.append('')

    def subtitle(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.body.append(title)
        self.body.append('*' * column_width(title))
        self.body.append('')

    def section(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append('=' * column_width(title))
        self.body.append(title)
        self.body.append('=' * column_width(title))
        self.nl()

    def subsection(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append(title)
        self.body.append('-' * column_width(title))
        self.nl()

    def blockquote(self, what, *, asis=False):
        if not isinstance(what, str):
            raise ValueError(repr(what))

        if asis:
            whatsplit = what.split('\n')
        else:
            whatsplit = what.split('\n\n')

        for par in whatsplit:
            par = par.strip()
            if not par or not asis:
                self.nl()
            if par:
                self.body.extend(textwrap.wrap(par,
                    initial_indent=' '*4,
                    subsequent_indent=' '*4))

    def under(self, what, amount=None):
        if not isinstance(what, str):
            raise ValueError(repr(what))
        n = -1
        maxn = -len(self.body)
        while n > maxn:
            if self.body[n].strip():
                break
            n -= 1
        if n == maxn and amount is None:
            self.body.extend(what.splitlines())
        else:
            if amount is None:
                first_blank = True
                for x in range(len(self.body[n])):
                    if not self.body[n][x].isspace():
                        break
                if x == len(self.body[n]):
                    x = 2
                    word = ''
                else:
                    word = self.body[n].strip().split()[0]
                for w in word:
                    if w not in '0123456789-.+*':
                        word = None
                if word is not None:
                    x += len(word) + 1
            else:
                x = amount

            for par in what.split('\n\n'):
                self.nl()
                self.body.extend(textwrap.wrap(par, 
                        initial_indent=' '*x, subsequent_indent=' '*x))

    def playable_audio(self, shorts):
        pass

    def playable_video(self, shorts):
        pass

    def clean(self, text):
        return text

    def html(self, *text):
        return False

    def gmi(self, *txt):
        return False

    def rel_ref(self, lrel):
        if isinstance(lrel, str):
            relpath = lrel
            lrel = Path(lrel)
        else:
            relpath = str(lrel)
        if lrel.is_absolute():
            if not lrel.exists():
                raise ValueError('Absolute paths should exist: {}'.format(lrel))
            lrel = rel_from_root(lrel)
            relpath = str(lrel)
        return relpath

    def poetry(self, text):
        if not text:
            return
        if isinstance(text, str):
            text = text.splitlines()
        self.body.extend(text)

    def _add_lines(self, text, *, prefix=None, wrap=False):
        ''' split in to lines and add a prefix

            Wrapping is done paragraph-style before prefixing,
            if specified.
        '''
        if not text:
            return
        if wrap:
            if isinstance(text, str):
                pars = text.split('\n\n')
            else:
                pars = text
        else:
            if isinstance(text, str):
                pars = [text]
            else:
                pars = text
        i = 0
        if not prefix:
            prefix = ''
        for par in pars:
            i += 1
            if wrap:
                txt = textwrap.wrap(par,
                    initial_indent=prefix, subsequent_indent=prefix)
                self.body.extend(txt)
            else:
                if isinstance(par, str):
                    txt = par.splitlines()
                else:
                    txt = par
                if not txt:
                    self.body.append('')
                else:
                    for line in txt:
                        self.body.append('{}{}'.format(prefix, line))

    def web_link(self, href, ltitle, labstract, *, context=None, extra=None):
        relpath = None
        lrel = None

        ctitle = ltitle
        if ctitle is None:
            ctitle = url_label(href)

        if extra is None:
            extra = ''
        if extra and not extra[0].isspace():
            extra = ' ' + extra
        if context:
            # No "Italic" support for Gopher
            ctitle = context.format(ctitle)
        link = f'{ctitle} <{href}>{extra}'
        self.body.append(link)
        if labstract:
            self.add(textwrap.fill(labstract,
                    initial_indent=' '*3, subsequent_indent=' '*3))
            self.nl()
        return 

    def desectioned(self, state):
        pass

    def add(self, text, *, wrap=False):
        self._add_lines(text, wrap=wrap)

    def nl(self):
        if self.body and self.body[-1]:
            self.body.append('')
        return

