#!/usr/bin/env python3

import argparse
from pathlib import Path
import sys
from configparser import ConfigParser
import time
import yaml
import json
import shutil
import os

_verbose = False

def verbose(*args, **kwargs):
    global _verbose
    if _verbose:
        print(*args, **kwargs)

def create_redirect(from_path, to_url, title):
    if from_path.suffix != '.html':
        verbose("{} is not an HTML file".format(from_path))
        return
    title1 = title
    if not title:
        title1 = 'Requested document moved!'
    else:
        title1 = title1 + ' moved!'
    content = []
    content.append('<!doctype html>\n<html><head>\n<title>')
    content.append(title1)
    content.append('</title>\n')
    content.append('<meta http-equiv="Refresh" content="5; url={}">\n'.format(to_url))
    content.append('</head><body>\n<h1>')
    content.append(title1)
    content.append('</h1>\n<p>You will be redirected to\n')
    content.append('<a href="{}">{}</a>\n'.format(to_url, to_url))
    content.append('in five seconds.</a></p>\n</body></html>\n')
    from_path.write_text(''.join(content))
    return

def cmd_redirect(args):
    global verbose
    verbose = args.verbose
    create_redirect(Path(args.html).resolve(), args.url, args.title)


