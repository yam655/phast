#!/usr/bin/env python3

import argparse
import sys
import os
from pathlib import Path

from .cmd_render import cmd_render
from .cmd_redirect import cmd_redirect
from .cmd_standard import cmd_standard
from .cmd_update import cmd_update

parser = argparse.ArgumentParser(
            description='A reStructuredText-based media-focused static site generator that renders to HTML and Gopher.'
        )
parser.add_argument('--verbose', '-v', action='store_true', help='Be more verbose about things.')
parser.add_argument('--root', help='Specify root of document tree',
                    default=os.environ.get('PHAST_DOCROOT'))

subparsers = parser.add_subparsers(help='Subcommands', dest='cmd')

render_parser = subparsers.add_parser('render', help='Render documents')
render_parser.add_argument('--force', '-f', action="store_true",
                help='Force rendering of whole document tree.')
render_parser.set_defaults(func=cmd_render)

redirect_parser = subparsers.add_parser('redirect', help='generate a redirect stub')
redirect_parser = argparse.ArgumentParser(description="Create standard redirection file")
redirect_parser.add_argument("--verbose", "-v", action="store_true",
                help="Enable verbose mode")
redirect_parser.add_argument("html", help="Destination file")
redirect_parser.add_argument("url", help="URL to redirect to")
redirect_parser.add_argument("--title", help="Title of page being redirected")
redirect_parser.set_defaults(func=cmd_redirect)

update_parser = subparsers.add_parser('update', help='Update folder metadata')
update_parser.add_argument('--recurse', '-r', action="store_true",
                help='Recursively process files.')
update_parser.add_argument('--force', '-f', action="store_true",
                help='Force creation/update of folder.conf.')
update_parser.set_defaults(func=cmd_update)

def main(argv):
    args = parser.parse_args(argv)
    if args.root is None:
        p = Path('.').resolve()
        c = p / 'site.conf'
        while p != p.parent and not c.exists():
            p = p.parent
            c = p / 'site.conf'
        if c.exists():
            args.root = str(p)
    if not args.root or not Path(args.root).is_dir():
        print('ERROR: Please go to (sub)directory containing phast "site.conf".')
        sys.exit(1)

    docroot = str(Path(args.root).expanduser().resolve())
    os.environ['PHAST_DOCROOT'] = docroot
    here = str(Path('.').resolve())
    if here != docroot and not here.startswith(docroot):
        os.chdir(docroot)
    if args.cmd is None:
        cmd_standard(args)
    args.func(args)
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

