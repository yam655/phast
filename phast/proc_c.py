#!/usr/bin/env python3

import glob
from pathlib import Path

from .config import rel_from_root, unconf, get_config, find_config_path, get_base_name, get_category_name_list, get_category_name
from .deps import dep_add, dep_changed
from .out_rst import rst
from .out_gmi import gemini
from .out_gophermap import gophermap
from .out_m3u8 import m3u8
from .brief import get_brief
from .meta_utils import j_to_stamp
from .syn_atom import atom
from .syn_hateoas import hateoas
from .syndication import process_syndication
from .mock_proc import process_gen_categories, load_localcache, find_in_cache


def process_c(bucket, out, source, localcache, *, have_m3u8=0):
    title = get_category_name(bucket['title'])
    if title and title[0] == '!':
        title = title[1:]
    out.title(title)
    process_gen_categories(bucket, out, localcache, source, have_m3u8)
    return out

def process(source, bucket):
    localcache = {}

    localcache = load_localcache(bucket, source)
    rel = bucket.get('rel_path')
    if rel is None:
        raise ValueError('{} needs a rel_path'.format(repr(bucket)))
    if isinstance(rel, str):
        rel = Path(rel)
    rst_out = rst()
    m3u8_bits = m3u8(localcache)
    # process_syndication(atom, rel / (rel.stem + '.xml'), cnt, None, title=bucket['title'])
    # process_syndication(hateoas, rel / (rel.stem + '.hal'), cnt, None, title=bucket['title'])
    process_c(bucket, out=rst_out, source=source,
                        localcache=localcache, have_m3u8=len(m3u8_bits))
    html = rst_out.render(path="{}/folder.rst".format(rel))
    gmi = process_c(bucket, out=gemini(), source=source,
                        localcache=localcache, have_m3u8=len(m3u8_bits))
    gmap = process_c(bucket, out=gophermap(), source=source,
                        localcache=localcache, have_m3u8=len(m3u8_bits))
    m3u8_text = str(m3u8_bits)
    out = [ {
                'rel_path': rel / (rel.stem + '.m3u8'),
                'is_file': True,
                'create': m3u8_text
            },
            {
            'rel_path':rel, 
            'is_file': False,
            'html':html, 
            'source':str(rst_out), 
            'index.gmi':str(gmi),
            'gophermap':str(gmap)}]
    return out


