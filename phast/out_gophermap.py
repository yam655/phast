#!/usr/bin/env python3

import codecs
from pathlib import Path
import textwrap

from .config import get_config, base_names, rel_from_root
from .meta_utils import url_label
from .out_text import text_base


class gophermap(text_base):
    def __init__(self):
        super().__init__()
        self.body = []
        self.body.append('')
        self.journal_text = None
        self.products_text = None
        self.services_text = None
        self.users_text = None
        self._set_footer_text('labels:plain')

    def __str__(self):

        body = []
        self.nl()
        body.append('-' * 66)
        body.append('')
        for name, path in [(self.home_text,'/'), 
                            (self.journal_text,'/j/'),
                            (self.products_text,'/p/'),
                            (self.services_text,'/s/'),
                            (self.users_text,'/u/'),
                            (self.reviews_text,'/r/'),
                            (self.categories_text,'/c/'),
                            (self.keywords_text,'/k/')]:
            if name:
                body.append('1{}\t{}'.format(name, path))
        b = self.clean('\n'.join(body))
        return self.clean('\n'.join(self.body) + b)

    def rel_link(self, lrel, ltitle, labstract, *, add_suffix=False,
                only_title=False, italic=False,
                is_file=False, context=None, include_parent=False):
        if isinstance(lrel, str):
            relpath = lrel
            lrel = Path(lrel)
        else:
            relpath = str(lrel)
        if add_suffix and not lrel.suffix:
            relpath = relpath + '.txt'
            lrel = Path(relpath)
        if lrel.is_absolute():
            if not lrel.exists() and not Path(str(lrel.parent / lrel.stem) + '.rst').exists():
                raise ValueError('Absolute paths should exist: {}'.format(lrel))
            lrel = rel_from_root(lrel)
            relpath = str(lrel)
        lname = lrel.name
        if include_parent:
            lname = '{}/{}'.format(lrel.parent.name, lrel.name)
        ctitle1 = ltitle
        if only_title or not ltitle or ltitle == lname:
            pass
        elif lname.replace('_', ' ') != ltitle:
            ctitle1 = '({}) {}'.format(lname, ltitle)
        ctitle = ctitle1

        if context:
            # No "Italic" support for Gopher
            ctitle = context.format(ctitle)
        # TODO this needs to support gopher at non-root locations
        if is_file:
            gt = self.suffixToGopherType(lrel.suffix)
            link = '{}{}\t/{}'.format(gt, ctitle, lrel)
        else:
            link = '1{}\t/{}'.format(ctitle, lrel)
        self.body.append(link)
        if labstract:
            self.add(textwrap.fill(labstract,
                    initial_indent=' '*3, subsequent_indent=' '*3))
            self.nl()
        return 

    def web_link(self, href, ltitle, labstract, *, 
                context=None, extra=None):
        relpath = None
        lrel = None

        ctitle = ltitle
        if ctitle is None:
            ctitle = url_label(href)
        if extra is None:
            extra = ''
        if extra and not extra[0].isspace():
            extra = ' ' + extra

        if context:
            ctitle = context.format(ctitle)
        link = f'h{ctitle}{extra}\t/URL:{href}\t+\t+'
        self.add(link)
        if labstract:
            self.add(textwrap.fill(labstract,
                    initial_indent=' '*3, subsequent_indent=' '*3))
            self.nl()
        return 


    def list_or_not(self, humankey, meta, key=None, split_char=None,
                    split_context=None, context=None):
        values = None
        if key is None:
            values = meta
        else:
            values = meta.get(key)
        if isinstance(values, (list, tuple)):
            pass
        elif isinstance(values, str):
            if values.strip() == '':
                values = None
            else:
                values = values.strip().splitlines()
        elif values:
            if meta != values:
                raise ValueError('called with "{}" having invalid data: {}'.format(repr(meta), repr(values)))
            else:
                raise ValueError('called invalid data: {}'.format(repr(values)))
        if not values:
            return

        if len(values) == 1:
            splt = ''
            if split_char:
                s = values[0].split(split_char,1)
                v = s[0].strip()
                if split_context:
                    splt = split_context.format(s[1].strip())
                elif len(s) > 1:
                    splt = s[1].strip()
            else:
                v = values[0].strip()
            lne = '{}: {}{}'.format(humankey, v, splt)
            if not context:
                self.body.append(lne)
            else:
                self.body.append(context.format(lne))
        else:
            humankey = humankey.strip()
            lne = '{}:'.format(humankey)
            if not context:
                self.body.append(lne)
            else:
                lne = context.format(lne)
                self.body.append(lne)
            spaces = ' ' * len(lne)
            for value in values:
                splt = ''
                if split_char:
                    s = values[0].split(split_char,1)
                    v = s[0].strip()
                    splt = split_context.format(s[1].strip())
                else:
                    v = values[0].strip()
                self.body.append('{}- {}{}'.format(spaces, v, splt))
            self.nl()
        return

    def covers(self, *, graphic=None, text=None, text_body=None, alt=None, embed=False, caption=None):
        if caption:
            self.body.extend(textwrap.wrap(
                'Caption: ' + caption,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.nl()
        if graphic:
            fname = Path(graphic)
            self.rel_link(fname, fname, None, add_suffix=False, is_file=True,
                            context='- Cover Image (graphic): {}')
        if text:
            fname = Path(text)
            self.rel_link(fname, fname, None, add_suffix=False, is_file=True,
                            context='- Cover Image (text): {}')
        if alt:
            self.body.append(textwrap.fill(
                'Description of cover image: ' + alt,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.body.append('')

        if embed and text_body:
            self.body.append('')
            txt_image = text_body.expandtabs()
            self.body.append(textwrap.indent(txt_image, prefix='.    '))

    def suffixToGopherType(self, suffix):
        if suffix and suffix[0] != '.':
            suffix = '.' + suffix
        if not suffix:
            return '0'
        elif suffix in ('.txt','.ly','.song','.mma','.sh') or suffix.endswith('.txt'):
            return '0'
        elif suffix in ('.mp3','.flac','.ogg','.wav','.midi','.mid'):
            return 's'
        elif suffix == '.gif' or suffix.endswith('.gif'):
            return 'g'
        elif suffix in ('.png', '.jpg', '.jpeg'):
            return 'I'
        elif suffix.endswith('.png') or suffix.endswith('.jpg') or suffix.endswith('.jpeg'):
            return 'I'
        else:
            return '9'

    def add_links(self, meta, metakey, humankey, *, extra=None, as_par=False):
        if meta.get(metakey):
            items = meta[metakey]
            if isinstance(items, str):
                items = items.splitlines()
            if extra:
                extra = ' ({})'.format(extra)
            else:
                extra = ''

            if len(items) == 1:
                self.web_link(items[0], url_label(items[0]), None, 
                                context='{} {}{}'.format(humankey, '{}', extra))
            else:
                self.body.append(humankey + extra)
                spaces = ' ' * len(humankey + ' ')
                for urls in items:
                    self.web_link(urls, url_label(urls), None, 
                                context='{}{}{}'.format(spaces, '{}', extra))
                self.nl()
        return

    def clean(self, text):
        if not text:
            return text
        return str(codecs.encode(text, encoding='US-ASCII', errors='xmlcharrefreplace'), encoding='UTF-8')

    def add(self, text, *, wrap=False):
        self._add_lines(text, wrap=wrap)

    def force_end(self):
        pass

    def nl(self):
        if self.body and self.body[-1]:
            self.body.append('')
        return

