#!/usr/bin/env python3

import codecs
from pathlib import Path

from .config import get_config, base_names
from .meta_utils import url_label


class gophermap:
    def __init__(self):
        self.body = []
        self.body.append('')
        self.journal_text = None
        self.products_text = None
        self.services_text = None
        self.users_text = None
        footer = set(get_config('sections').split())

        self.home_text = get_config('labels:plain', 'home', 
                                        default=base_names['home'])
        if 'j' in footer:
            self.journal_text = get_config('labels:plain', 'journal', 
                                        default=base_names['journal'])
        if 'p' in footer:
            self.products_text = get_config('labels:plain', 'products', 
                                        default=base_names['products'])
        if 's' in footer:
            self.services_text = get_config('labels:plain', 'services', 
                                        default=base_names['services'])
        if 'u' in footer:
            self.users_text = get_config('labels:plain', 'users', 
                                        default=base_names['users'])

    def __str__(self):
        return self.clean('\n'.join(self.body))

    def title(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.body.append(title)
        self.body.append('#' * len(title))
        self.body.append('')

    def section(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append(title)
        self.body.append('=' * len(title))
        self.nl()

    def rel_link(self, lrel, ltitle, labstract, *, is_file=False, context=None):
        if isinstance(lrel, str):
            lrel = Path(lrel)
        lname = lrel.name
        if is_file:
            if not lrel.suffix:
                lrel = lrel.parent / (lrel.name + '.txt')
        relpath = str(lrel)
        ctitle1 = ltitle
        if not ltitle or ltitle == lname:
            ctitle1  = ltitle
        elif lname.replace('_', ' ') != ltitle:
            ctitle1 = '({}) {}'.format(lname, ltitle)
        ctitle = ctitle1
        if context:
            ctitle = context.format(ctitle)
        if is_file:
            gt = self.suffixToGopherType(lrel.suffix)
            link = '{}{}\t{}'.format(gt, ctitle, lrel)
        else:
            link = '1{}\t{}'.format(ctitle, lrel)
        self.body.append(link)
        return 

    def list_or_not(self, humankey, meta, key=None):
        values = None
        if key is None:
            if isinstance(meta, (list, tuple)):
                values = meta
            elif isinstance(meta, str):
                if meta.strip() == '':
                    values = None
                else:
                    values = [meta]
            elif meta is not None:
                raise ValueError('called with no key and invalid data: {}'.format(repr(meta)))
        else:
            values = meta.get(key)
        if not values:
            return

        if isinstance(values, str):
            values = values.strip().splitlines()
        if len(values) == 1:
            self.body.append('{}: {}'.format(humankey, values[0]))
        else:
            humankey = humankey.strip()
            spaces = ' ' * 3
            self.body.append(humankey)
            for value in values:
                self.body.append('{}- {}'.format(spaces, value))
            self.nl()
        return

    def under(self, what):
        if not isinstance(what, str):
            raise ValueError(repr(what))
        n = -1
        maxn = -len(what)
        while n > maxn:
            if self.body[n].strip():
                break
            n -= 1
        if n == maxn:
            self.body.append(what)
        else:
            first_blank = True
            for x in range(len(self.body[n])):
                if not self.body[n][x].isspace():
                    break
            if x == len(self.body[n]):
                x = 2
                word = ''
            else:
                word = self.body[n].strip().split()[0]
            for w in word:
                if w not in '0123456789-.+*':
                    word = None
            if word is not None:
                x += len(word) + 1
            self.body.append('{}{}'.format(' ' * x, what))

    def playable_audio(self, audio_shorts):
        pass

    def covers(self, *, graphic=None, text=None, text_body=None, alt=None, embed=False):
        embed = None
        if caption:
            self.body.append(textwrap.fill(
                'Caption: ' + caption,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.nl()
        if graphic:
            fname = graphic
            gopherType = suffixToGopherType('.' + fname.rsplit('.',1)[-1])
            self.body.append('{}- {}\t{}'.format(gopherType,
                        "Cover Image (graphic):", fname, fname))
        if text:
            fname = text
            embed = fname
            gopherType = suffixToGopherType('.' + fname.rsplit('.',1)[-1])
            self.body.append('{}- {}\t{}'.format(gopherType,
                        "Cover Image (text):", fname, fname))
        if alt:
            self.body.append(textwrap.fill(
                'Description of cover image: ' + alt,
                    initial_indent=' '*4, subsequent_indent=' '*4))
            self.body.append('')

        if embed and text_body:
            self.body.append('')
            txt_image = text_body.expandtabs()
            self.body.append(textwrap.indent(txt_image, prefix=' '*4))

    def suffixToGopherType(self, suffix):
        if suffix and suffix[0] != '.':
            suffix = '.' + suffix
        if not suffix:
            return '0'
        elif suffix == '.txt' or suffix.endswith('.txt'):
            return '0'
        elif suffix in ('.mp3','.flac','.ogg','.wav','.midi','.mid'):
            return 's'
        elif suffix == '.gif' or suffix.endswith('.gif'):
            return 'g'
        elif suffix in ('.png', '.jpg', '.jpeg'):
            return 'I'
        elif suffix.endswith('.png') or suffix.endswith('.jpg') or suffix.endswith('.jpeg'):
            return 'I'
        else:
            return '9'

    def add_links(self, meta, metakey, humankey, *, extra=None):
        if meta.get(metakey):
            items = meta[metakey]
            if isinstance(items, str):
                items = items.splitlines()
            if extra:
                extra = ' ({})'.format(price)
            else:
                extra = ''

            if len(items) == 1:
                self.body.append('{} `{}{} <{}>`__'.format(humankey,
                                url_label(items[0]), 
                                extra, items[0]))
            else:
                self.body.append(humankey + extra)
                spaces = ' ' * len(humankey + ' ')
                for urls in items:
                    self.body.append('{} `{} <{}>`__'.format(spaces,
                        url_label(urls), urls))
                self.lf()
        return

    def clean(self, text):
        if not text:
            return text
        return str(codecs.encode(text.expandtabs(), encoding='US-ASCII', errors='xmlcharrefreplace'), encoding='UTF-8')

    def poetry(self, text):
        if not text:
            return
        if isinstance(text, str):
            text = text.splitlines()
        self.body.extend(text)

    def add(self, text):
        if text:
            self.body.extend(text.splitlines())

    def nl(self):
        if self.body and self.body[-1]:
            self.body.append('')
        return

    def footer(self):
        self.body.append('-' * 76)
        self.body.append('')
        for name, path in [(self.home_text,'/'), 
                            (self.blog_text,'/j/'),
                            (self.products_text,'/p/'),
                            (self.services_text,'/s/'),
                            (self.users_text,'/u/')]:
            if name:
                self.body.append('1{}\t{}'.format(name, path))
