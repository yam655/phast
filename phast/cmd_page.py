#!/usr/bin/env python3

import sys
import re
from pathlib import Path
import time
import os
import subprocess

from .config import find_config_path

def cmd_page(args):
    title = args.title
    desc = args.abstract
    timen = time.time()
    times = time.localtime(timen)
    path = find_config_path().parent / 'j' / time.strftime('%Y-%m/%d_r%H%M.rst', times)
    if path.exists():
        print('ERROR: "{}" already exists.'.format(d), file=sys.stderr)
    path.parent.mkdir(parents=True, exist_ok=True)

    editor = None
    for var in ('PHAST_EDITOR', 'VISUAL', 'EDITOR'):
        if var in os.environ:
            editor = os.environ[var]
            break
    if editor is None:
        for d in os.environ['PATH'].split(':'):
            check = Path(d) / 'sensible-editor'
            if check.exists():
                editor = str(check)
                break
            check = Path(d) / 'nano'
            if check.exists():
                editor = str(check)
                break
            
    if not title:
        title = input('Title of page? ')

    if not desc:
        d = input('Abstract for page? ')
        desc = '|Phlog| {}'.format(d)

    if not editor:
        print('ERROR: no editor found.', file=os.stderr)

    template = make_template(title, desc)
    path.write_text(template)

    subprocess.run([editor, str(path)])
    p = path.read_text()
    if p == template:
        print('No change. File discarded.')
        path.unlink()
    return 0

def make_template(title, desc):
    out = []
    out.append('')
    out.append('.. meta::')
    out.append('   :description: {}'.format(desc))
    out.append('')
    out.append('#' * len(title))
    out.append(title)
    out.append('#' * len(title))
    out.append('')
    out.append('')
    return '\n'.join(out)


