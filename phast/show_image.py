#!/usr/bin/env python3

from pathlib import Path

from .config import root_path

def showcase_image(bucket, source, out, localcache):
    folder = bucket.get('folder', {})
    conf = bucket.get('conf', {})

    graphic = folder.get('image')
    text = folder.get('image.txt')
#   if 'First Potato (Our Holy Ruler)' == folder.get('title'):
#       print(f'{folder}')
#       raise ValueError('"source" should be defined. "{}" "{}"'.format(folder.get('image'), folder.get('image.txt')))
    if graphic or text:
        root = root_path()
        out.section('Images')
        if graphic:
            if isinstance(graphic, str) or not graphic.is_absolute():
                if not Path(graphic).exists():
                    raise ValueError('"{}" does not exist'.format(graphic))
        else:
            graphic = None
        if text:
            if isinstance(text, str) or not text.is_absolute():
                if not Path(text).exists():
                    print (repr(folder))
                    raise ValueError('"{}" does not exist'.format(text))
        else:
            text = None
        out.covers(graphic=graphic, text=text,
                    text_body=folder.get('image.txt/body'),
                    alt=folder.get('image.alt'), embed=True)


