#!/usr/bin/env python3

from pathlib import Path

from .config import find_config_path, get_cache

from .config import get_config, read_config
from .brief import get_brief
from .out_rst import rst
from .out_gmi import gemini
from .out_gophermap import gophermap
from .out_m3u8 import m3u8

def fill_local_cache(source, *, limit, reqtype=None):
    cache = get_cache()
    localcache = {}
    jdir = source / 'j'
    if isinstance(reqtype, str):
        reqtype = [reqtype]
    for d1 in sorted(jdir.glob('*'), reverse=True):
        for d2 in sorted(d1.glob('*'), reverse=True):
            k = str(d2)
            brief = get_brief(k)
            if not brief:
                continue
            if reqtype is not None:
                if 'bits' not in brief:
                    continue
                bits = brief['bits']
                found = False
                for req  in reqtype:
                    if req in bits:
                        found = True
                        break
                if not found:
                    continue
    
            localcache[k] = brief
            if len(localcache) >= limit:
                break
        if len(localcache) >= limit:
            break
    return localcache


def process_syndication(syn, out_file, length, enclosure, *, title=None, briefs=None):
    source = find_config_path()
    bucket = get_cache()[str(source)]
    if briefs is None:
        briefs = fill_local_cache(source.parent, limit=length, reqtype=enclosure)
    s = syn(bucket, briefs, title=title)
    dest_html = get_config('html', as_path=True)
    dest_gopher = get_config('gopher', as_path=True)
    dest_gemini = get_config('gemini', as_path=True)
    s1 = str(s)
    d1,d2,d3 = None,None,None
    if dest_html:
        d1 = dest_html / out_file
        d1.write_text(s1)
    if dest_gopher:
        d2 = dest_gopher / out_file
        if d1 != d2:
            d2.write_text(s1)
    if dest_gemini:
        d3 = dest_gemini / out_file
        if d3 != d2 and d3 != d1:
            d3.write_text(s1)
    return

