#!/usr/bin/env python3


def process(source, bucket):
    rel = bucket.get('rel_path')
    meta = bucket.get('copy')
    if not rel and not meta:
        raise ValueError('copy values not specified for {}'.format(source))
    if not meta:
        to_file = rel
        from_file = source
    else:
        from_file, to_file = meta
    if not from_file.is_file():
        raise ValueError('{} should be a file {}'.format(source, repr(meta)))
    return [{'copy': (from_file, to_file),
            'rel_path': to_file, 
            'is_file': True,
            }]
