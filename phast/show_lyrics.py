
def showcase_lyrics(bucket, out, localcache):
    lyrics = None
    if not lyrics and 'lyrics' in bucket.get('folder', {}):
        lyrics = bucket['folder']['lyrics']
    if not lyrics and 'audio' in bucket:
        lyrics = bucket['audio']['lyrics']
    if lyrics:
        out.section('Lyrics')
        out.poetry(lyrics)
