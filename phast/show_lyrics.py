
from .config import get_cache

def showcase_lyrics(bucket, source, out, localcache):
    lyrics = None
    if not lyrics and 'lyrics' in bucket.get('folder', {}):
        lyrics = bucket['folder']['lyrics']
    if not lyrics and 'conf' in bucket:
        lyrics = bucket['conf'].get('audio',{}).get('lyrics')
    if not lyrics and 'audio' in bucket:
        lyrics = bucket['audio']['lyrics']
    if not lyrics:
        for k in localcache:
            ks = str(k)
            if ks.endswith('.song'):
                cache = get_cache()
                x = cache.get(ks,{})
                lyrics = x.get('song',{}).get('lyrics')
                break
            if ks.endswith('.ly'):
                cache = get_cache()
                x = cache.get(ks,{})
                lyrics = x.get('lilypond',{}).get('lyrics')
                break

    conf = bucket.get('conf', {})
    cache = get_cache()
    ly_shorts = {}
    for f in conf:
        n = conf[f]
        if 'in-showcases' in n:
            if 'lyrics' in n.get('in-showcases').splitlines():
                ly_shorts[n.get('short', f)] = f

    if ly_shorts or lyrics:
        out.section('Lyrics')
        out.nl()

    if lyrics:
        out.poetry(lyrics)

    if ly_shorts:
        out.nl()
        out.add('Download:')
        out.nl()
        for lshort in ly_shorts:
            lfilename = ly_shorts[lshort]
            if isinstance(lfilename, str):
                link = source / lfilename
                brief = conf[lfilename]
            else:
                link = lfilename
                brief = localcache[lfilename]
                lfilename = link.name
            title = brief.get('title', lfilename)
            abstract = brief.get('abstract', '')
            cntx = '    - {}'
            if lshort not in title and lshort not in abstract:
                cntx = '    - ({}) {}'.format(lshort, '{}')
            out.rel_link(link, title, abstract, only_title=True, # absolute
                            is_file=True, add_suffix=False, context=cntx)
        out.nl()

