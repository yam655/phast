#!/usr/bin/env python3

from pathlib import Path

from .brief import save_brief
from .meta_utils import ignore_path
from .config import find_config_path, get_cache
from . import load_audio
from . import load_rst
from . import load_file
from . import load_ly
#from . import load_p
#from . import load_s
from . import load_site
from . import load_song
from . import load_folder
from . import load_directory

def load_brief(source):
    if not source:
        return None
    brief = None
    if isinstance(source, str):
        source = Path(source)

    for mod in (load_rst, load_audio, load_ly, load_song,
                #load_p, load_s, 
                load_site, load_folder, load_directory, load_file):
        loaded = mod.load(source, None)
        if not loaded:
            continue
        bucket, changed = loaded
        k = str(source)
        if bucket is None:
            continue
        brief = save_brief(k, bucket)
        if 'title' not in brief:
            raise ValueError("{} missing title".format(repr(mod)))
        if 'rel_path' not in brief:
            raise ValueError("{} missing rel_path".format(repr(mod)))
        break
    return brief

