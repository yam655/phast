#!/usr/bin/env python3

from pathlib import Path

from .config import get_cache
from .brief import save_brief
from .meta_utils import ignore_path
from . import load_media
from . import load_rst
from . import load_file
from . import load_ly
from . import load_site
from . import load_song
from . import load_folder
from . import load_directory

def load_brief(source, *, full=False):
    if not source:
        return None
    brief = None
    bucket = None
    if isinstance(source, str):
        source = Path(source)
    consider = True

    if not source.is_dir():
        ignore = source / "IGNORE"
        if ignore.exists():
            return None, None

    for mod in (load_rst, load_media, load_ly, load_song,
                load_site, load_folder, load_directory, load_file):
        loaded = mod.load(source, None)
        if not loaded:
            continue
        bucket, changed = loaded
        k = str(source)
        if bucket is None:
            continue
        get_cache()[k] = bucket
        if mod is load_folder:
            consider = False
        break
    if bucket is not None and consider:
        if 'rel_path' not in bucket:
            raise ValueError(repr(bucket))
        source_parent = source.parent
        parent_got = load_folder.load(source_parent, None)
        if parent_got is not None:
            parent_bucket = parent_got[0]
            override = parent_bucket.get('_override',{}).get(source.name, {})
            for k in override:
                v = override[k].strip()
                if not v:
                    continue
                if v == '-':
                    del bucket[k]
                else:
                    bucket[k] = override[k]
    if bucket is None:
        return None
    brief = save_brief(k, bucket)
    if 'title' not in brief:
        raise ValueError("{} missing title".format(repr(mod)))
    if 'rel_path' not in brief:
        raise ValueError("{} missing rel_path".format(repr(mod)))
    if full:
        return bucket
    return brief

