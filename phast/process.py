#!/usr/bin/env python3

from pathlib import Path
from .config import find_config_path, get_cache

from . import proc_copy
from . import proc_folder
from . import proc_p
from . import proc_page
from . import proc_redirect
from . import proc_s
from . import proc_site
from . import proc_mockd
from . import proc_k
from . import proc_c

def follow_link(bucket, cache):
    if 'rel_path' not in bucket:
        raise ValueError('Unable to follow "link" in {}'.format(repr(bucket)))
    bucket1 = cache.get(bucket['link']).copy()
    bucket1['is_file'] = bucket.get('is_file', False)
    bucket1['rel_path'] = bucket.get('rel_path')
    return bucket1

def process_list(container):
    cache = get_cache()
    
    for source in sorted(container):
        bucket = cache.get(str(source))
#        if str(source) == '/Users/yam655/src/yam655/j/2019-01/07_r2052':
#            raise ValueError(repr([list(bucket.keys()), list(bucket.get('folder',{}).keys())]))
        if isinstance(bucket, set):
            raise ValueError(repr([source, bucket]))

        out = []
        if bucket and 'link' in bucket:
            bucket = follow_link(bucket, cache)
        if source == 'product:':
            continue
        if not bucket or 'ignore' in bucket:
            # raise ValueError('No bucket for: "{}"'.format(source))
            continue
        if 'redirect' in bucket:
            out.extend(proc_redirect.process(source, bucket))
        if 'copy' in bucket:
            out.extend(proc_copy.process(source, bucket))
        if 'product' in bucket:
            out.extend(proc_p.process(source, bucket))
        if 'keyword' in bucket:
            out.extend(proc_k.process(source, bucket))
        if 'category' in bucket:
            out.extend(proc_c.process(source, bucket))
        if 'page' in bucket:
            out.extend(proc_page.process(source, bucket))
        if 'mock_dir' in bucket:
            out.extend(proc_mockd.process(source, bucket))
        if 'service' in bucket:
            out.extend(proc_s.process(source, bucket))
        if 'site' in bucket:
            out.extend(proc_site.process(source, bucket))
        if 'folder' in bucket:
            out.extend(proc_folder.process(source, bucket))
        if not out and bucket.get('is_file'):
            out.extend(proc_copy.process(source, bucket))
        if not out:
            raise ValueError('For "{}", uknown data mode: {}'
                                .format(source, list(bucket.keys())))
        for x in out:
            yield x

