#!/usr/bin/env python3

import sys
import re
from pathlib import Path
import time
from shutil import copyfile

from .config import find_config_path

def cmd_mkdir(args):
    d = args.dir
    if d == '-':
        d = stamp()
    elif d.startswith('@'):
        preload_dir(d[1:])
        return 0

    if not re.match(r'[0-9]{4}-[0-9]{2}/[0-9]{2}_r[0-9]{4}', d):
        print('ERROR: directory did not match required pattern'
              ' YYYY-MM/DD_rXXXX', file=sys.stderr) 
        sys.exit(1)
    path = find_config_path().parent / 'j' / d
    if path.exists():
        print('ERROR: "{}" already exists.'.format(d), file=sys.stderr)
    path.mkdir(parents=True, exist_ok=False)
    print(path)
    return 0

def preload_dir(thing):
    item = Path(thing).resolve()
    if not item.exists():
        print('ERROR: "{}" does not exist.'.format(item), file=sys.stderr)
        sys.exit(1)
    #print(f'DEBUG: checking {item.name}', file=sys.stderr)

    if re.match(r'[0-9]{4}-[0-9]{2}-[0-9]{2}[-_ ]r?[0-9]{4}[-_].*', item.name):
        piece = re.sub(r'^([0-9]{4}-[0-9]{2})-([0-9]{2})[-_ ]r?([0-9]{4})[-_](.*)$',
                       r'\1/\2_r\3/\4', item.name).replace(' ','_').replace("'", '')
        #print(f'DEBUG: MATCH {piece}', file=sys.stderr)
        fullpath = find_config_path().parent / 'j' / piece
        path = fullpath.parent

    # Meadow After Border_2020-10-01_1318.mp3
    elif re.match(r'.*[-_ ][0-9]{4}-[0-9]{2}-[0-9]{2}[-_ ]r?[0-9]{4}[.].*', item.name):
        piece = re.sub(r'^(.*)[-_ ]([0-9]{4}-[0-9]{2})-([0-9]{2})[-_ ]r?([0-9]{4})([.].*)$',
                       r'\2/\3_r\4/\1\5', item.name).replace(' ','_').replace("'", '')
        #print(f'DEBUG: MATCH {piece}', file=sys.stderr)
        fullpath = find_config_path().parent / 'j' / piece
        path = fullpath.parent

    else:
        d = stamp(item.stat().st_mtime)
        #print(f'DEBUG: NO-MATCH {d}', file=sys.stderr)
        path = find_config_path().parent / 'j' / d
        fullpath = path / item.name
    if path.exists():
        print(f'ERROR: "{str(path)}" already exists.', file=sys.stderr)
    path.mkdir(parents=True, exist_ok=False)
    for part in item.parent.glob(item.stem + '.*'):
        fullpart = fullpath.parent / (fullpath.stem + (''.join(part.suffixes)).lower())
        copyfile(part, fullpart)
    print(path)
    return 0

def stamp(timen = None):
    if timen is None:
        timen = time.time()
    times = time.localtime(timen)
    return time.strftime("%Y-%m/%d_r%H%M", times)

