#!/usr/bin/env python3

import glob
from pathlib import Path

from .config import get_category_name_list, find_config_path
from .brief import get_brief
from .meta_utils import j_to_stamp


def load_localcache(data, source):
    localcache = {}
    os1 = data.get('ordinal', {})
    os2 = data.get('__ordinal', {})
    os3 = data.get('--ordinal', {})
    # If we load __obsolete here, it will show in the m3u8 file. (Bad.)

    for o in sorted(os1):
        b = get_brief(os1[o])
        localcache[os1[o]] = b
    for o in sorted(os2):
        b = get_brief(os2[o])
        localcache[os2[o]] = b
    for o in sorted(os3):
        b = get_brief(os3[o])
        localcache[os3[o]] = b

    return localcache


def find_in_cache(path, localcache):
    if not path:
        return None
    if not isinstance(path, str):
        path = str(path)
    if path[0] == '/':
        return localcache.get(path)
    path = find_config_path().parent / path
    path = str(path)
    return localcache.get(path)


def process_gen_keywords(data, out, localcache, source, have_m3u8):
    return process_gen_something(data, out, localcache, source, have_m3u8, in_keywords=True)

def process_gen_categories(data, out, localcache, source, have_m3u8):
    return process_gen_something(data, out, localcache, source, have_m3u8, in_categories=True)

def process_gen_product(data, out, localcache, source, have_m3u8):
    return process_gen_something(data, out, localcache, source, have_m3u8, in_product=True)

def process_gen_service(data, out, localcache, source):
    return process_gen_something(data, out, localcache, source, 0, in_service=True)


def process_gen_something(data, out, localcache, source, have_m3u8, **kwargs):
    notes = data.get('notes', [])
    out.nl()
    if notes:
        out.section('Notes')
        for n in notes:
            stamp = j_to_stamp(n)
            brief = find_in_cache(n, localcache)
            title = ''
            if brief:
                title = ' "{}"'.format(brief['title'])
            out.rel_link(n, stamp, None, # absolute
                            is_file=False, add_suffix=False, only_title=True,
                            context='From {}{}:'.format('{}',title), italic=True)
            out.nl()
            out.blockquote(notes[n], asis=True)
        out.nl()

    announcements = data.get('announcement', [])
    if announcements:
        out.section('Announcements')
        for a in sorted(announcements, reverse=True, key=lambda x: x[1]):
            ann = announcements[a]
            brief = get_brief(a)
            if 'title' not in brief:
                raise ValueError(repr(announcements))
            abstract = 'from {}: {}'.format(j_to_stamp(brief.get('rel_path')),
                            brief['title'])
            out.rel_link(a, ann, abstract, only_title=True, # absolute
                            is_file=False, add_suffix=False, context='- {}')
        out.nl()

    os1 = data.get('ordinal', {})
    os2 = data.get('__ordinal', {})
    os3 = data.get('--ordinal', {})
    os4 = data.get('__obsolete', {})

    def show_entries(sorted_osX, osX, unordered=True, *, obsolete=False):
        for o in sorted_osX:
            keyB = osX[o]
            klist = keyB
            if isinstance(klist, str):
                klist = [keyB,]
            for key in klist:
                b = None
                if localcache is not None:
                    if key in localcache:
                        b = localcache[key]
                    else:
                        b = get_brief(key)
                if not b:
                    continue
                relb = Path(b['rel_path'])
                titleb = b.get('title', key.rsplit('/',1)[-1])
                cat = b.get('categories')
                if obsolete:
                    pass
                elif unordered:
                    o = '*'
                else:
                    o = f'{o}.'
                if obsolete:
                    if cat:
                        cattitle = get_category_name_list(cat)
                        context=f'* (former {o}) {"{}"} (in {cattitle})'
                    else:
                        context=f'* (former {o}) {"{}"}'
                else:
                    if cat:
                        cattitle = get_category_name_list(cat)
                        context='{} {} (in {})'.format(o, '{}', cattitle)
                    else:
                        context='{} {}'.format(o, '{}')
                out.rel_link(relb, titleb, b.get('abstract'),  # rel/root
                                    is_file=False, add_suffix=False, include_parent=True,
                                    context=context)
            out.nl()


    if os1 and len(out.body) > 5:
        out.section('Ordered entries')
    show_entries(sorted(os1), os1, False)

    if os2 and len(out.body) > 5:
        out.section('Unordered entries')
    show_entries(sorted(os2, reverse=True), os2)

    if os3 and len(out.body) > 5:
            out.section('Journal')
    show_entries(sorted(os3, reverse=True), os3)

    if os4:
        out.section('Obsolete entries')
    show_entries(sorted(os4), os4, obsolete=True)


    rel = Path(data.get('rel_path'))
    out.nl()
    if have_m3u8 > 0:
        out.nl()
        relb = rel.stem + '.m3u8'
        songs = ' ({} songs and videos)'.format(have_m3u8)
        out.rel_link(relb, 'Remote Playlist', None, only_title=True, # rel/root
                            is_file=True, add_suffix=False,
                            context='Listen: ({}) {}{}'
                                    .format(relb.rsplit('.',1)[-1],'{}', songs))




