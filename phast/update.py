#!/usr/bin/env python3

from pathlib import Path
import magic
import subprocess

from .brief import get_brief
from .brief_load import load_brief
from .config import rel_from_root, verbose, get_config
from .meta_utils import ignore_path

def _handle_code(f, state, folder_conf, folder, confirm, my_conf, brief):
    if '|Code|' not in confirm:
        confirm['|Code|'] = brief

def _handle_video(f, state, folder_conf, folder, confirm, my_conf, brief):
    if state.get('bestvideo') is None:
        state['bestvideo'] = f
    else:
        myPri = ['.webm', '.ogg', '.mp4'].index(state['bestvideo'].suffix)
        lastPri = ['.webm', '.ogg', '.mp4'].index(f.suffix)
        if myPri < lastPri:
            state['bestvideo'] = f

    summary = folder_conf[f.name]
    if 'video' not in folder_conf:
        bucket = load_brief(f, full=True)
        video = bucket['video']
    
        d2 = {}
        for ak in video:
            if ak == 'poem':
                continue
            if isinstance(video[ak], str):
                d2[ak] = video[ak]
            else:
                d2[ak] = '\n'.join(video[ak]).strip()
            if ak == 'TITLE' and 'title' not in folder:
                folder['title'] = d2[ak]
        folder_conf['video'] = d2
    video = folder_conf['video']
    
    default_block = {}
    for x in summary:
        if x in ('POET', 'COMPOSER'):
            del summary[x]
    default_block = {}
    if 'DEFAULT' in folder_conf:
        default_block = folder_conf['DEFAULT']
    default_album = default_block.get('album')
    if not default_album:
        default_album = default_block.get('_album')
    if my_conf:
        if default_album and 'released_on' in my_conf:
            if 'p/{}'.format(default_album).replace(' ','_') == my_conf['released_on']:
                del my_conf['released_on']
        del_me = []
        for k1 in my_conf:
            if k1 and k1[0] == '_':
                continue
            if not my_conf[k1].strip():
                del_me.append(k1)
            else:
                my_conf[k1] = str(my_conf[k1])
        for k1 in del_me:
            del my_conf[k1]
        for k1 in folder_conf['audio']:
            if k1 in my_conf and k1 not in default_block:
                del my_conf[k1]
        if 'abstract' in my_conf:
            abstract = my_conf['abstract']
            problem_text = ' contains:MPEG ADTS, layer III, v1, '
            if problem_text in abstract:
                abstract = abstract.replace(problem_text,' ')
                my_conf['abstract'] = abstract
    folder_conf[f.name]['type'] = 'video'
    folder_conf[f.name]['in-showcases'] = 'video'
    if '|Video|' not in confirm:
        confirm['|Video|'] = brief
    if not state.get('maybe_product') and 'title' in video:
        state['maybe_product'] = {'title': video['title']}
        if video.get('TRACKNUMBER') is not None:
            state['maybe_product']['ordinal'] = video.get('TRACKNUMBER')


def _handle_keywords(f, state, folder_conf, folder, confirm, my_conf, brief):
    keytype = f.suffix[1:]
    if keytype not in folder:
        folder[keytype] = f.read_text().strip()

def _handle_audio(f, state, folder_conf, folder, confirm, my_conf, brief):
    if state.get('bestaudio') is None:
        state['bestaudio'] = f
    else:
        myPri = ['.spx', '.ogg', '.mp3', '.flac'].index(state['bestaudio'].suffix)
        lastPri = ['.spx', '.ogg', '.mp3', '.flac'].index(f.suffix)
        if myPri < lastPri:
            state['bestaudio'] = f

    summary = folder_conf[f.name]
    if 'audio' not in folder_conf:
        bucket = load_brief(f, full=True)
        audio = bucket['audio']
    
        d2 = {}
        for ak in audio:
            if ak == 'poem':
                continue
            if isinstance(audio[ak], str):
                d2[ak] = audio[ak]
            else:
                d2[ak] = '\n'.join(audio[ak]).strip()
            if ak == 'TITLE' and 'title' not in folder:
                folder['title'] = d2[ak]
        folder_conf['audio'] = d2
    audio = folder_conf['audio']
    
    default_block = {}
    for x in summary:
        if x in ('POET', 'COMPOSER'):
            del summary[x]
    default_block = {}
    if 'DEFAULT' in folder_conf:
        default_block = folder_conf['DEFAULT']
    default_album = default_block.get('album')
    if not default_album:
        default_album = default_block.get('_album')
    if my_conf:
        if default_album and 'released_on' in my_conf:
            if 'p/{}'.format(default_album).replace(' ','_') == my_conf['released_on']:
                del my_conf['released_on']
        del_me = []
        for k1 in my_conf:
            if k1 and k1[0] == '_':
                continue
            if not my_conf[k1].strip():
                del_me.append(k1)
            else:
                my_conf[k1] = str(my_conf[k1])
        for k1 in del_me:
            del my_conf[k1]
        for k1 in folder_conf['audio']:
            if k1 in my_conf and k1 not in default_block:
                del my_conf[k1]
        if 'abstract' in my_conf:
            abstract = my_conf['abstract']
            problem_text = ' contains:MPEG ADTS, layer III, v1, '
            if problem_text in abstract:
                abstract = abstract.replace(problem_text,' ')
                my_conf['abstract'] = abstract
    folder_conf[f.name]['type'] = 'audio'
    folder_conf[f.name]['in-showcases'] = 'audio'
    if '|Audio|' not in confirm:
        confirm['|Audio|'] = brief
    if not state.get('maybe_product') and 'title' in audio:
        state['maybe_product'] = {'title': audio['title']}
        if audio.get('TRACKNUMBER') is not None:
            state['maybe_product']['ordinal'] = audio.get('TRACKNUMBER')

def _process_confirm_for_folder(folder, confirm, folder_conf):
    if len(confirm) == 1:
        k, some_brief = confirm.popitem()
        if 'abstract' not in folder:
            if some_brief and some_brief.get('abstract'):
                folder['abstract'] = some_brief['abstract']
        if 'title' not in folder:
            if some_brief and some_brief.get('title'):
                folder['title'] = some_brief['title']
    elif confirm:
        duration = None
        if 'video' in folder_conf:
            duration = folder_conf['video']['duration']
        if duration is None and 'audio' in folder_conf:
            duration = folder_conf['audio']['duration']
        dur = ''
        if duration:
            dur = ' ({})'.format(duration)
        if 'abstract' not in folder:
            markers = list(confirm.keys())
            markers.sort()
            if '|Code|' in confirm:
                folder['abstract'] = '{} assorted content{}'.format(' '.join(markers), dur)
            elif '|Video|' in confirm:
                folder['abstract'] = confirm['|Video|']['abstract']
            elif '|Audio|' in confirm:
                folder['abstract'] = confirm['|Audio|']['abstract']
            else:
                folder['abstract'] = '{} assorted content{}'.format(' '.join(markers), dur)
        if 'title' not in folder:
            for c in ('|Video|', '|Audio|', '|Sheetmusic|', '|Lyrics|'):
                if c in confirm and 'title' in confirm[c]:
                    folder['title'] = confirm[c]['title']
                    break
        if 'title' not in folder:
            for c in confirm:
                if 'title' in confirm[c]:
                    folder['title'] = confirm[c]['title']
                    break

def update_folder_conf(path, folder_conf, brief_cache=None, ignore_dirs=False):
    product_thumbs = {}
    mapping = {}
    confirm = {}
    state = {}
    prefix = None
    present = set()
    poem = None
    if 'poem' in folder_conf:
        poem = folder_conf['poem']['source']
    
    if 'folder' not in folder_conf:
        folder_conf['folder'] = {}
    folder = folder_conf['folder']
    some_brief = None
    for v in folder:
        if v and v[0] == '_':
            continue
        folder[v] = str(folder[v])
    if 'isostamp' in folder:
        del folder['isostamp']
    for f in path.parent.glob('*'):
        present.add(f.name)
        if ignore_path(f) or f.stem == 'folder':
            continue
        if f.is_dir():
            if ignore_dirs:
                continue
            ignore = f / "IGNORE"
            if ignore.exists():
                continue
        suffixes = ''
        if f.suffixes:
            suffixes = ''.join(f.suffixes)[1:]
        mapping.setdefault(suffixes, [])
        mapping[suffixes].append(f.name)

        brief = None
        if f.is_dir():
            brief = get_brief(f)
        if not brief:
            brief = load_brief(f)
        if not brief:
            continue
        if brief_cache is not None:
            brief_cache[f] = brief
        my_conf = None
        if f.name in folder_conf:
            my_conf = folder_conf[f.name]
            if 'isostamp' in my_conf:
                del my_conf['isostamp']
            if 'artifact_name' in my_conf and 'title' not in my_conf:
                my_conf['title'] = my_conf['artifact_name']
                del my_conf['artifact_name']
            if 'artifact_description' in my_conf and 'abstract' not in my_conf:
                my_conf['abstract'] = my_conf['artifact_description']
                del my_conf['artifact_description']
            if not my_conf.get('abstract') and brief.get('abstract'):
                my_conf['abstract'] = brief['abstract']
            if not my_conf.get('title') and brief.get('title'):
                my_conf['title'] = brief['title']
        else:
            if brief:
                folder_conf[f.name] = {}
                summary = folder_conf[f.name]
                summary['title'] = brief.get('title', f.name)
                if brief.get('abstract'):
                    summary['abstract'] = brief.get('abstract')
                my_conf = folder_conf[f.name]
            else:
                ## Probably purposefully ignored.
                print('WARNING: Unable to find brief for "{}"'.format(f))
                continue

        if f.suffix in ('.keywords', '.autokw'):
            if f.suffix == '.autokw':
                state['have_autokw'] = True
            _handle_keywords(f, state, folder_conf, folder, confirm, my_conf, brief)

        if f.suffix in ('.mp3', '.flac', '.ogg', '.spx'):
            _handle_audio(f, state, folder_conf, folder, confirm, my_conf, brief)

        elif f.suffix in ('.mp4', '.webm'):
            _handle_video(f, state, folder_conf, folder, confirm, my_conf, brief)

        elif f.suffix in ('.py', '.sh'):
            _handle_code(f, state, folder_conf, folder, confirm, my_conf, brief)

        elif f.suffix in ('.png', '.jpeg', '.jpg', '.gif', '.svg', '.webp'):
            is_thumb = False
            if f.stem.endswith('-thumb') or suffixes.startswith('thumb'):
                is_thumb = True
                product_thumbs[f.suffix] = str(f.name)
            elif state.get('bestimage') is None:
                state['bestimage'] = f
            elif len(f.suffixes) > 2:
                myPri = ['.front', '.cover', '.back'].index(state['bestimage'].suffixes[0])
                lastPri = ['.front', '.cover', '.back'].index(f.suffixes[0])
                if myPri < lastPri:
                    state['bestimage'] = f
            elif len(state['bestimage'].suffixes) == 1:
                myPri = ['.svg', '.jpeg', '.jpg', '.webp', '.png', '.gif'].index(state['bestimage'].suffix)
                lastPri = ['.svg', '.jpeg', '.jpg', '.webp', '.png', '.gif'].index(f.suffix)
                if state['bestimage'].stem == 'back':
                    state['bestimage'] = f
                elif f.stem == 'back':
                    pass
                elif myPri < lastPri:
                    state['bestimage'] = f
            folder_conf[f.name]['type'] = 'image'
            if not is_thumb:
                folder_conf[f.name]['in-showcases'] = 'image'
                if '|Graphic|' not in confirm:
                    confirm['|Graphic|'] = brief

        elif suffixes == 'txt':
            if f.name == 'folder.txt':
                prefix = True
            elif f.name.endswith('_lyrics.txt') or f.name == 'lyrics.txt':
                state['need_autokw'] = f
                summary = folder_conf[f.name]
                summary['in-showcases'] = 'lyrics'
                if 'showcase_lyrics' not in folder:
                    folder['showcase_lyrics'] = f.name
                if '|Lyrics|' not in confirm:
                    confirm['|Lyrics|'] = brief
            elif f.name.endswith('_transcript.txt') or f.name == 'transcript.txt':
                state['need_autokw'] = f
                summary = folder_conf[f.name]
                summary['in-showcases'] = 'transcript'
                if 'showcase_transcript' not in folder:
                    folder['showcase_transcript'] = f.name
                if '|Transcript|' not in confirm:
                    confirm['|Transcript|'] = brief
            elif f.name.endswith('_poem.txt') or f.name == 'poem.txt':
                if poem is None:
                    poem = f.name

        elif suffixes == 'lyrics.txt':
            state['need_autokw'] = f
            summary = folder_conf[f.name]
            summary['in-showcases'] = 'lyrics'
            if 'showcase_lyrics' not in folder:
                folder['showcase_lyrics'] = f.name
            if '|Lyrics|' not in confirm:
                confirm['|Lyrics|'] = brief

        elif suffixes == 'transcript.txt':
            state['need_autokw'] = f
            summary = folder_conf[f.name]
            summary['in-showcases'] = 'transcript'
            if 'showcase_transcript' not in folder:
                folder['showcase_transcript'] = f.name
            if '|Transcript|' not in confirm:
                confirm['|Transcript|'] = brief

        elif suffixes == 'ly':
            bucket = load_brief(f, full=True)
            if not state.get('maybe_product') and 'product-info' in bucket:
                state['maybe_product'] = bucket['product-info']

            summary = folder_conf[f.name]
            summary['in-showcases'] = 'lyrics'
            if 'showcase_lyrics' not in folder:
                folder['showcase_lyrics'] = f.name
            summary['in-showcases'] = 'lyrics\naudio'
            if '|Sheetmusic|' not in confirm:
                confirm['|Sheetmusic|'] = brief

        elif suffixes == 'poem.txt':
            if poem is None:
                poem = f.name

        elif suffixes == 'song':
            summary = folder_conf[f.name]
            summary['in-showcases'] = 'audio'
            bucket = load_brief(f, full=True)
            if not state.get('maybe_product') and 'product-info' in bucket:
                state['maybe_product'] = bucket['product-info']

        elif f.suffix in ('.mma', '.vtt', '.mid', '.midi', 
                          '.kar', '.mmpz', '.wav'):
            summary = folder_conf[f.name]
            summary['in-showcases'] = 'audio'

        elif f.suffix == '.pdf':
            lyfile = f.parent / (f.stem + '.ly')
            if lyfile.exists():
                summary = folder_conf[f.name]
                summary['in-showcases'] = 'audio'
                if not summary.get('abstract'):
                    summary['abstract'] = '|Sheetmusic| Ready-to-Print sheet music'

    _process_confirm_for_folder(folder, confirm, folder_conf)

    if 'need_autokw' in state and not 'have_autokw' in state:
        autokw = get_config('autokw')
        if autokw:
            kwsource = state['need_autokw']
            outfile = kwsource.parent / (kwsource.stem + '.autokw')
            autokw_out = subprocess.run([autokw, kwsource], capture_output=True, text=True).stdout
            if autokw_out:
                autokw_out = autokw_out.strip().replace('%','')
            if autokw_out:
                outfile.write_text(autokw_out)
                folder['autokw'] = autokw_out

    if 'product' in folder and 'product' not in folder_conf:
        folder_conf['product'] = {}
        folder_conf['product']['title'] = folder['product']
        del folder['product']
    if 'product' not in folder_conf and state.get('maybe_product'):
        if 'title' in state.get('maybe_product',{}):
            folder_conf['product'] = state['maybe_product']
    elif 'product' in folder_conf and state.get('maybe_product'):
        prod = folder_conf['product']
        if 'ordinal' not in prod and state['maybe_product'].get('ordinal') is not None:
            prod['ordinal'] = state['maybe_product']['ordinal']

    for m in mapping:
        if len(mapping[m]) == 1:
            #if mapping[m][0] in folder_conf:
                folder_conf[mapping[m][0]]['short'] = m
        else:
            for fm in mapping[m]:
                #if fm in folder_conf:
                    folder_conf[fm]['short'] = fm

    if state.get('bestvideo'):
        folder['showcase_video'] = state.get('bestvideo').name
    elif 'showcase_video' in folder:
        del folder['showcase_video']

    if state.get('bestaudio'):
        folder['showcase_audio'] = state.get('bestaudio').name
    elif 'showcase_audio' in folder:
        del folder['showcase_audio']

    if state.get('bestimage'):
        folder['showcase_picture'] = state['bestimage'].name
    elif 'showcase_picture' in folder:
        del folder['showcase_picture']
    present.add('folder')
    present.add('audio')
    present.add('video')
    present.add('poem')
    present.add('product')
    present.add('song')
    present.add('redirect')
    present.add('DEFAULT')
    if 'title' not in folder:
        folder['title'] = ''
    if 'abstract' not in folder:
        folder['abstract'] = ''
    if 'keywords' in folder:
        if 'keyword' not in folder:
            folder['keyword'] = folder['keywords']
            del folder['keywords']
    elif 'keyword' not in folder:
        folder['keyword'] = ''
    if 'category' not in folder:
        folder['category'] = 'uncategorized'
    if 'product' not in folder_conf:
        product = {}
        folder_conf['product'] = product
        product = folder_conf['product']
        product['title'] = ''
        product['abstract'] = ''
        product['ordinal'] = ''
        product['image'] = ''
        product['image.txt'] = ''
        product['notes'] = ''
        product['announcement'] = ''
    else:
        product = folder_conf['product']
    if 'product' in folder_conf and 'audio' in folder_conf:
        audio_block = folder_conf['audio']
        if not product.get('title','').strip() and 'ALBUM' in audio_block:
            product['title'] = audio_block['ALBUM']
        if not product.get('year','').strip() and 'DATE' in audio_block:
            product['year'] = audio_block['DATE']
        if not product.get('purchase_url','') and audio_block.get('COMMENT','').endswith('bandcamp.com'):
            product['purchase_url'] = ''
            product['purchase_price'] = ''
    if product_thumbs and 'product' in folder_conf:
        product = folder_conf['product']
        if '.gif' in product_thumbs:
            product['image.gif'] = product_thumbs['.gif']
        if '.webp' in product_thumbs:
            product['image.thumb'] = product_thumbs['.webp']

    for k in list(folder_conf.keys()):
        if k not in present:
            if '.poem.' in k and 'source' in folder_conf[k]:
                poem = {'source': folder_conf[k]['source']}
                folder_conf['poem'] = poem
                del folder_conf[k]
            elif not k.startswith('hide:'):
                print('WARNING:', k)


