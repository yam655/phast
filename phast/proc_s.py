#!/usr/bin/env python3

import glob
from pathlib import Path

from .config import rel_from_root, unconf, get_config
from .deps import dep_add, dep_changed
from .out_rst import rst
from .out_gmi import gemini
from .out_gophermap import gophermap
from .show_service import showcase_service, showcase_option
from .brief import get_brief
from .meta_utils import j_to_stamp, clean_gen_path
from .mock_proc import find_in_cache, load_localcache, process_gen_service


def compile_options(bucket):
    out = []
    for k in bucket:
        print(k)
        if k.startswith('option/'):
            group = k.get('group', '!')
            key = '{}\t{}'.format(group, k)
            print(key)
            out.append((k, key))
    out.sort(key=lambda x: x[-1])
    if not out:
        return []
    return zip(*out)[-1]

def process_option(bucket, name, out):
    thing = {}
    url = get_config('url')
    if not url.endswith('/'):
        url = url + '/'
    if not bucket:
        return
    prod = bucket
    for d in prod:
        if d in ('notes', 'announcement', '__TOUCHED__'):
            continue
        elif d in ('image', 'image.txt'):
            thing[d] = '{}{}/{}'.format(url, prod[d][1], prod[d][0])
            continue
        elif isinstance(prod[d], dict):
            continue
        else:
            thing[d] = prod[d][0]
    if thing:
        showcase_option(thing, name, out)

def process_options(bucket, out):
    out.nl()
    last_group = None
    for opt in sorted(bucket, key=lambda x: '{}\t{}'.format(bucket[x]['option'].get('group','-'), x)):
        b = bucket[opt]['option']
        if last_group != b.get('group',None):
            last_group = b.get('group')
            if last_group is not None:
                if '|' in last_group[0]:
                    out.subsection(last_group[0].split('|')[-1].strip().title())
                else:
                    out.subsection(last_group[0].title())
                out.nl()
        process_option(b, opt, out)
        out.nl()

def fill_needs(need_info):
    need = {}
    paypal = get_config('contact', 'paypal')
    email = get_config('contact', 'email')
    phone = get_config('contact', 'phone')
    bio = get_config('contact', 'bio')
    bio_bits = {}
    bio_bits['bio_text'] = get_config('contact', 'bio_text')
    if not bio_bits['bio_text']:
        bio_bits['bio_text'] = 'Who are we? Why can we help?'
    if bio and bio.strip():
        if '//' in bio:
            bio_bits['bio_is_url'] = True
            # Bio as remote URL
            raise NotImplemented('URLs not currently supported')
        elif ':' in bio:
            # Bio as product
            bio = clean_gen_path(bio.strip())
        elif '/' in bio:
            # Bio as local page
            # TODO: 'folder' vs 'page' (etc)
            bio_bits['bio_is_file'] = False
            bio_bits['bio_is_add_suffix'] = False
            raise NotImplemented('local URLs not currently supported')
    if not need_info:
        if paypal:
            need['paypal'] = paypal
        if email:
            need['email'] = email
        if paypal:
            need['paypal'] = paypal
        if bio:
            need['bio'] = bio
    else:
        for n in need_info.split():
            if n == 'paypal' and paypal:
                need['paypal'] = paypal
            if n == 'email' and email:
                need['email'] = email
            if n == 'phone' and phone:
                need['phone'] = phone
            if n == 'bio' and bio:
                need['bio'] = bio
                for b in bio_bits:
                    need[b] = bio_bits[b]
    for label in list(need.keys()):
        label_text = '{}_text'.format(label)
        t = get_config('contact', label_text)
        if t:
            need[label_text] = t
    return need

def add_contact_info(top_bottom, out):
    need_info = get_config('contact', '{}_info'.format(top_bottom))
    needs = fill_needs(need_info)
    phone = needs.get('phone')
    email = needs.get('email')
    paypal = needs.get('paypal')
    bio = needs.get('bio')

    if not needs:
        return

    out.nl()
    header = get_config('contact', '{}_header'.format(top_bottom))
    if not header:
        header = get_config('contact', 'header')
    if not header:
        header = 'Contact Information'
    out.add('.. admonition:: {}'.format(header))
    out.nl()
    text = get_config('contact', '{}_text'.format(top_bottom))
    if not text:
        text = get_config('contact', 'text')
    if text:
        out.add('   {}'.format(text.strip()))
        out.nl()

    if needs.get('bio_is_url'):
        raise NotImplemented('URLs not currently supported')
    if bio:
        out.rel_link(bio, needs['bio_text'], None, # rel/root
                    is_file=needs.get('bio_is_file', False),
                    add_suffix=needs.get('bio_add_suffix', False),
                    only_title=True, context='   * {}')

    if phone:
        if '+' in phone:
            phone_url = 'tel:{}'.format(phone.replace('-',''))
        else:
            phone_url = 'tel:+1{}'.format(phone.replace('-',''))
        out.add('   * Are you ready to get started? {}'.format(phone))
    if email:
        out.add('   * Can we answer questions about something? {}'.format(email))
    if paypal:
        out.add('   * Remember, payment is easy! {}'.format(paypal))

    out.nl()
    return

def process_s(bucket, out, source, localcache):
    out.title(bucket['title'])
    paypal = get_config('contact', 'paypal')
    email = get_config('contact', 'email')
    phone = get_config('contact', 'phone')
    out.nl()

    out.nl()
    thing = {}
    url = get_config('url')
    if not url.endswith('/'):
        url = url + '/'
    prod = bucket.get('service', {})
    for d in prod:
        if d in ('notes', 'announcement', '__TOUCHED__'):
            continue
        elif d in ('image', 'image.txt'):
            thing[d] = '{}{}/{}'.format(url, prod[d][1], prod[d][0])
            continue
        elif isinstance(prod[d], dict):
            continue
        else:
            thing[d] = prod[d][0]

    showcase_service(thing, out)

    add_contact_info('top', out)

    process_options(bucket.get('options',{}), out)
    process_gen_service(bucket, out, localcache, source)

    add_contact_info('bottom', out)
    return out


def process(source, bucket):
    localcache = {}

    localcache = load_localcache(bucket, source)
    rel = bucket.get('rel_path')
    if rel is None:
        raise ValueError('{} needs a rel_path'.format(repr(bucket)))
    if isinstance(rel, str):
        rel = Path(rel)
    rst_out = process_s(bucket, out=rst(), source=source,
                        localcache=localcache)
    html = rst_out.render(path="{}/folder.rst".format(rel))
    gmi = process_s(bucket, out=gemini(), source=source,
                        localcache=localcache)
    gmap = process_s(bucket, out=gophermap(), source=source,
                        localcache=localcache)
    out = [ {
            'rel_path':rel, 
            'is_file': False,
            'html':html, 
            'source':str(rst_out), 
            'index.gmi':str(gmi),
            'gophermap':str(gmap)}]
    return out


