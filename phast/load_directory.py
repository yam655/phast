#!/usr/bin/env python3

import glob

from .brief import get_brief
from .config import rel_from_root, remove_replacements
from .deps import dep_add, dep_changed
from .load_folder import gather_product, gather_service, gather_review, gather_keyword, gather_category, ref_or_inline
from .meta_utils import ignore_path
from .brief_load import load_brief
from .update import update_folder_conf

def find_file_something(meta, path, something, deps, dflt):
    name = path.name
    changed = False
    if path.is_dir():
        name = 'folder'
        tpath = path / ('{}.{}'.format(name, something))
    else:
        tpath = path.parent / ('{}.{}'.format(name, something))
    
    if dep_changed(tpath, deps) and tpath.exists():
        changed = True
        dep_add(tpath, deps)
        meta[something] = tpath.read_text().strip()

    if dflt and not meta.get(something):
        meta[something] = dflt
        changed = True
    return changed


def ref_or_first(source, meta, ref, deps, *wildcards):
    filename = meta.get(ref)
    changed = False
    if filename:
        filename = filename.strip()
    if filename:
        it_file = source / filename
        if it_file.exists():
            if dep_changed(it_file, deps):
                dep_add(it_file, deps)
                changed = True
        elif filename == '-':
            return False
        else:
            meta[ref] = None
            filename = None
    if not filename:
        for wc in wildcards:
            filenames = list(source.glob(wc))
            if len(filenames) > 0:
                it_file = filenames[0]
                if dep_changed(it_file, deps):
                    changed = True
                    dep_add(it_file, deps)
                    meta[ref] = it_file.name
                break
    return changed


def load_directory(data, source, deps, brief_cache):

    meta = {}
    data['folder'] = meta
    changed = 0

    for genre in source.glob('*.genre'):
        if dep_changed(genre, deps):
            dep_add(genre, deps)
            meta['genre/' + genre.stem] = genre.read_text().strip().splitlines()
            changed += 1

    if find_file_something(data, source, 'abstract', deps, None):
        changed += 1
    if find_file_something(data, source, 'title', deps, None):
        changed += 1
    if find_file_something(data, source, 'keywords', deps, None):
        changed += 1
    if find_file_something(data, source, 'category', deps, None):
        changed += 1

    data.setdefault('conf',{})
    conf = data['conf']
    newest_mt = 0
    for fname in source.glob('*'):
        mt = fname.stat().st_mtime
        if mt > newest_mt:
            newest_mt = mt
    if newest_mt > deps.get('__COMPOSITE__', 0):
        deps['__COMPOSITE__'] = newest_mt
        changed += 1
        update_folder_conf(source / 'folder.conf', conf, brief_cache=brief_cache)
        if not data.get('title'):
            data['title'] = conf.get('folder',{}).get('title')
        if not data.get('abstract'):
            data['abstract'] = conf.get('folder',{}).get('abstract')

    if ref_or_inline(source, meta, 'showcase_lyrics', 'lyrics', deps):
        changed += 1
    if ref_or_inline(source, meta, 'showcase_transcript', 'transcript', deps):
        changed += 1
    if ref_or_inline(source, meta, 'chapbook_poem', 'poem', deps):
        changed += 1
    if ref_or_first(source, meta, 'showcase_audio', deps, '*.spx', '*.ogg', '*.mp3'):
        changed += 1

    return changed > 0


def load(source, data):
    if not source or not source.is_dir():
        return None, None
    ignore = source / "IGNORE"
    if ignore.exists():
        return None, None
    oneItem = None
    ablist = []
    if data is None or data.get('loader') != 'directory':
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']

    rel = rel_from_root(source)
    data['rel_path'] = rel
    data['is_file'] = False
    data['loader'] = 'directory'

    brief_cache = {}
    changed = load_directory(data, source, deps, brief_cache)
    abstract = data.get('abstract')
    title = data.get('title')

    good_brief = None
    for s in source.glob('*'):
        if ignore_path(s) or s.stem == 'folder':
            continue
        if s.suffix in ('.mp3', '.flac', '.spx', '.ogg'):
            if brief_cache.get(s):
                good_brief = s
        elif s.suffix in ('.ly', '.song', '.rst'):
            if brief_cache.get(s) and not good_brief:
                good_brief = s
        if s.is_dir():
            ignore = s / "IGNORE"
            if ignore.exists():
                continue
            else:
                oneItem = False
        if s in brief_cache:
            ablist.append(brief_cache.get(s).get('title', s.name))
        else:
            ablist.append(s.name)
        if oneItem is None:
            oneItem = s
        else:
            oneItem = False
        if abstract and len(ablist) > 2:
            break

    if oneItem and not good_brief:
        if brief_cache.get(oneItem):
            good_brief = oneItem
    if good_brief:
        brief = brief_cache.get(good_brief)
        abstract = brief.get('abstract')
        if abstract and not data['abstract']:
            data['abstract'] = abstract
        title = brief.get('title')
        if title and not data['title']:
            data['title'] = title

    if not abstract and not data.get('abstract'):
        abstract = '({} files) {}'.format(len(ablist), '; '.join(ablist))
        if len(abstract) > 200:
            abstract = remove_replacements(abstract)
            if len(abstract) > 200:
                abstract = abstract[:197] + '...'
        data['abstract'] = abstract
    if not title and not data.get('title'):
        title = source.name.replace('_', ' ')
        data['title'] = title

#    if oneItem:
#        data['link'] = str(oneItem)
#        return data, True

    meta = data['folder']
    meta['title'] = data['title']
    meta['abstract'] = data['abstract']

    folder_txt = source / 'folder.txt'
    if dep_changed(folder_txt, deps):
        dep_add(folder_txt, deps)
        changed = True
        prefix = None
        if folder_txt.exists():
            prefix = folder_txt.read_text()
        meta['prefix'] = prefix

    if 'conf' in data:
        gather_product(data, source, deps)
        gather_service(data, source, deps)
        gather_review(data, source, deps)
        gather_keyword(data, source, deps)
        gather_category(data, source, deps)

    return data, changed

