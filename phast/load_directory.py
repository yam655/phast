#!/usr/bin/env python3

import glob

from .brief import get_brief
from .config import rel_from_root, get_base_name
from .load_folder import dep_add, dep_changed
from .meta_utils import ignore_path
from .brief_load import load_brief

def find_file_something(meta, path, something, deps, dflt):
    name = path.name
    changed = False
    if path.is_dir():
        name = 'folder'
        tpath = path / ('{}.{}'.format(name, something))
    else:
        tpath = path.parent / ('{}.{}'.format(name, something))
    
    if dep_changed(tpath, deps) and tpath.exists():
        changed = True
        dep_add(tpath, deps)
        meta[something] = tpath.read_text().strip()

    if dflt and not meta.get(something):
        meta[something] = dflt
        changed = True
    return changed


def ref_or_first(source, meta, ref, deps, *wildcards):
    filename = meta.get(ref)
    changed = False
    if filename:
        filename = filename.strip()
    if filename:
        it_file = source / filename
        if it_file.exists():
            if dep_changed(it_file, deps):
                dep_add(it_file, deps)
                changed = True
        elif filename == '-':
            return False
        else:
            meta[ref] = None
            filename = None
    if not filename:
        for wc in wildcards:
            filenames = list(source.glob(wc))
            if len(filenames) > 0:
                it_file = filenames[0]
                if dep_changed(it_file, deps):
                    changed = True
                    dep_add(it_file, deps)
                    meta[ref] = it_file.name
                break
    return changed

def ref_or_inline(source, meta, ref, inline, deps):
    filenames = list(source.glob('*.{}.txt'.format(inline)))
    changed = False
    if len(filenames) > 1:
        it_file = filenames[0]
        if dep_changed(it_file, deps):
            changed = True
            dep_add(it_file, deps)
            meta[inline] = it_file.read_text()
            meta[ref] = inline
    return changed


def load_directory(data, source, deps):

    meta = {}
    data['folder'] = meta
    changed = 0

    for genre in source.glob('*.genre'):
        if dep_changed(genre, deps):
            dep_add(genre, deps)
            meta['genre/' + genre.stem] = genre.read_text().strip().splitlines()
            changed += 1

    if find_file_something(data, source, 'abstract', deps, None):
        changed += 1
    if find_file_something(data, source, 'title', deps,
                    get_base_name(data['rel_path']).replace('_',' ')):
        changed += 1

    if ref_or_inline(source, meta, 'showcase_lyrics', 'lyrics', deps):
        changed += 1
    if ref_or_inline(source, meta, 'chapbook_poem', 'poem', deps):
        changed += 1
    if ref_or_first(source, meta, 'showcase_audio', deps, '*.spx', '*.ogg', '*.mp3'):
        changed += 1

    data.setdefault('conf',{})
    conf = data['conf']
    for fname in source.glob('*'):
        if not fname.suffix or fname.suffix in ('.title', '.abstract'):
            continue
        
        box = load_brief(fname)
        if not box:
            box = {}
            if find_file_something(box, fname, 'abstract', deps, None):
                changed += 1
            if find_file_something(box, fname, 'title', deps,
                            '{} ({})'.format(fname.stem.replace('_',' '), fname.suffix[1:])):
                changed += 1
            #(repr(box))
        if box:
            conf[fname.name] = box
        else:
            raise ValueError(repr(fname))
    return changed > 0


def load(source, data):
    if not source or not source.is_dir():
        return None, None
    oneItem = None
    ablist = []
    if data is None or data.get('loader') != 'directory':
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']

    rel = rel_from_root(source)
    data['rel_path'] = rel
    data['is_file'] = False
    data['loader'] = 'directory'

    changed = load_directory(data, source, deps)
    abstract = data.get('abstract')
    title = data.get('title')

    for s in source.glob('*'):
        if ignore_path(s):
            continue
        if s.is_dir():
            oneItem = False
        ablist.append(str(s.name))
        if oneItem is None:
            oneItem = s
        else:
            oneItem = False
        if abstract and len(ablist) > 2:
            break

    if not abstract:
        abstract = '({} files) {}'.format(len(ablist), '; '.join(ablist))
        if len(abstract) > 60:
            abstract = abstract[:57] + '...'
        data['abstract'] = abstract
    if not title:
        title = source.name.replace('_', ' ')
        data['title'] = title

#    if oneItem:
#        data['link'] = str(oneItem)
#        return data, True

    meta = data['folder']
    meta['title'] = data['title']
    meta['abstract'] = data['abstract']

    folder_txt = source / 'folder.txt'
    if dep_changed(folder_txt, deps):
        dep_add(folder_txt, deps)
        changed = True
        prefix = None
        if folder_txt.exists():
            prefix = folder_txt.read_text()
        data['prefix'] = prefix

    return data, changed

