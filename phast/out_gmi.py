#!/usr/bin/env python3

import codecs
from pathlib import Path
import re

from .config import get_config, base_names, rel_from_root, convert_replacements
from .meta_utils import url_label
from .out_gophermap import gophermap

def rst2gemini(text):
    text1 = re.sub(r'^[.][.]\W+_([^_`].*):\W+([^:].*)\W*$', '=> $2\t$1', text)
    text1 = re.sub(r'^[.][.]\W+_`([^_].*)`:\W+([^:].*)\W*$', '=> $2\t$1', text1)
    text1 = re.sub(r'^[.][.]\W+__:\W+([^:].*)\W*$', '=> $1', text1)
    return text1

def cmark2gemini(text):
    text1 = re.sub(r'^\[([^\]]*)\]:\W+(.*)\W*$', '=> $2\t$1', text)
    text1 = re.sub(r'''^\[([^\]]*)\]:\W+(.*)\W+('|"|\(\().*('|"|\)\))\W*$''', '=> $2\t$1', text1)
    return text1

class gemini(gophermap):
    def __init__(self):
        super().__init__()
        self._set_footer_text('labels:unicode')

    def suffixToGopherType(self, suffix):
        raise RuntimeException("GMI files should not use Gopher Types.")

    def rel_link(self, lrel, ltitle, labstract, *, add_suffix=False,
                only_title=False, italic=False,
                is_file=False, context=None, include_parent=False):
        if isinstance(lrel, str):
            relpath = lrel
            lrel = Path(lrel)
        else:
            relpath = str(lrel)
        if add_suffix and not lrel.suffix:
            relpath = relpath + '.txt'
            lrel = Path(relpath)
        if lrel.is_absolute():
            if not lrel.exists() and not Path(str(lrel.parent / lrel.stem) + '.rst').exists():
                raise ValueError('Absolute paths should exist: {}'.format(lrel))
            lrel = rel_from_root(lrel)
            relpath = str(lrel)
        lname = lrel.name
        if include_parent:
            lname = '{}/{}'.format(lrel.parent.name, lrel.name)
        ctitle1 = ltitle
        if only_title or not ltitle or ltitle == lname:
            pass
        elif lname.replace('_', ' ') != ltitle:
            ctitle1 = '({}) {}'.format(lname, ltitle)
        ctitle = ctitle1

        if context:
            # No "Italic" support for Gopher
            ctitle = context.format(ctitle)

        link = '=> /{}\t{}'.format(lrel, ctitle)
        self.body.append(link)
        if labstract:
            self.add(f'   {labstract}')
            self.nl()
        return 

    def web_link(self, href, ltitle, labstract, *, 
                context=None, extra=None):
        relpath = None
        lrel = None

        ctitle = ltitle
        if ctitle is None:
            ctitle = url_label(href)
        if extra is None:
            extra = ''
        if extra and not extra[0].isspace():
            extra = ' ' + extra

        if context:
            ctitle = context.format(ctitle)
        link = f'=> {href}\t{ctitle}{extra}'
        self.add(link)
        if labstract:
            self.add(f'   {labstract}')
            self.nl()
        return 

    def title(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append(f'# {title}')
        self.nl()

    def subtitle(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append(f'## {title}')
        self.nl()

    def section(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append(f'### {title}')
        self.nl()

    def subsection(self, title):
        if not isinstance(title, str):
            raise ValueError(repr(title))
        self.nl()
        self.body.append(f'#### {title}')
        self.nl()


    def __str__(self):

        body = []
        self.nl()
        body.append('-' * 66)
        body.append('')
        for name, path in [(self.home_text,'/'), 
                            (self.journal_text,'/j/'),
                            (self.products_text,'/p/'),
                            (self.services_text,'/s/'),
                            (self.users_text,'/u/'),
                            (self.reviews_text,'/r/'),
                            (self.categories_text,'/c/'),
                            (self.keywords_text,'/k/')]:
            if name:
                body.append('=> {}\t{}'.format(path, name))
        b = self.clean('\n'.join(body))
        return self.clean('\n'.join(self.body) + b)


    def clean(self, text):
        return convert_replacements(text)

    def blockquote(self, what, *, asis=False):
        if not isinstance(what, str):
            raise ValueError(repr(what))

        if asis:
            whatsplit = what.split('\n')
        else:
            whatsplit = what.split('\n\n')

        for par in whatsplit:
            par = par.strip()
            if not par or not asis:
                self.nl()
            if par:
                par = ' '.join(par.split()) # normalize spaces
                self.add(f'> {par}')

    def under(self, what, amount=None):
        if not isinstance(what, str):
            raise ValueError(repr(what))
        n = -1
        maxn = -len(self.body)
        while n > maxn:
            if self.body[n].strip():
                break
            n -= 1
        if n == maxn and amount is None:
            self.body.extend(what.splitlines())
        else:
            if amount is None:
                first_blank = True
                for x in range(len(self.body[n])):
                    if not self.body[n][x].isspace():
                        break
                if x == len(self.body[n]):
                    x = 2
                    word = ''
                else:
                    word = self.body[n].strip().split()[0]
                for w in word:
                    if w not in '0123456789-.+*':
                        word = None
                if word is not None:
                    x += len(word) + 1
            else:
                x = amount

            for par in what.split('\n\n'):
                self.nl()
                if par:
                    par = ' '.join(par.split()) # normalize spaces
                    self.add(f'{" " * x}{par}')

    def _add_lines(self, text, *, prefix=None, wrap=False):
        ''' split in to lines and add a prefix

            Wrapping is done paragraph-style before prefixing,
            if specified.
        '''
        if not text:
            return
        if not isinstance(text, str):
            text = "\n".join(text)
        pars = text.split('\n\n')
        i = 0
        if not prefix:
            prefix = ''
        for par in pars:
            i += 1
            if par:
                txt = ' '.join(par.split()) # normalize spaces
                self.body.append(f'{prefix}{txt}')
            else:
                self.nl()

    def gmi(self, *text):
        for txt in text:
            self.add(f'{txt}')
        return True

    def covers(self, *, graphic=None, text=None, text_body=None, caption=None, alt=None, embed=False):
        if caption and not (graphic and embed):
            self.body.append(f'    Caption: {caption}')
            self.nl()
            
        if graphic:
            graphic = Path(graphic)
            if not graphic.is_absolute() and not graphic.exists():
                raise ValueError('Graphics should be absolute paths and exist: {}'.format(graphic))
            fname = graphic
            self.rel_link(fname, None, None, add_suffix=False, is_file=True, # absolute
                            context='- Cover Image (graphic): {}')
        if text:
            text = Path(text)
            if not text.is_absolute() and not text.exists():
                raise ValueError('Graphics should be absolute paths and exist: {}'.format(text))
            fname = text
            self.rel_link(fname, None, None, add_suffix=False, is_file=True, # absolute
                            context='- Cover Image (text): {}')
        if alt:
            self.body.append(f'    Description of cover image: {alt}')
            self.body.append('')

    def list_or_not(self, humankey, meta, key=None, split_char=None,
                    split_context=None, context=None):
        values = None
        if key is None:
            values = meta
        else:
            values = meta.get(key)
        if isinstance(values, (list, tuple)):
            pass
        elif isinstance(values, str):
            if values.strip() == '':
                values = None
            else:
                values = values.strip().splitlines()
        elif values:
            if meta != values:
                raise ValueError('called with "{}" having invalid data: {}'.format(repr(meta), repr(values)))
            else:
                raise ValueError('called invalid data: {}'.format(repr(values)))
        if not values:
            return

        if len(values) == 1:
            splt = ''
            if split_char:
                s = values[0].split(split_char,1)
                v = s[0].strip()
                if split_context:
                    splt = split_context.format(s[1].strip())
                elif len(s) > 1:
                    splt = s[1].strip()
            else:
                v = values[0].strip()
            lne = '{}: {}{}'.format(humankey, v, splt)
            if not context:
                self.body.append(lne)
            else:
                self.body.append(context.format(lne))
        else:
            humankey = humankey.strip()
            lne = '{}:'.format(humankey)
            if not context:
                self.body.append(lne)
            else:
                lne = context.format(lne)
                self.body.append(lne)
            spaces = ' ' * len(lne)
            self.nl()
            for value in values:
                splt = ''
                if split_char:
                    s = values[0].split(split_char,1)
                    v = s[0].strip()
                    splt = split_context.format(s[1].strip())
                else:
                    v = values[0].strip()
                self.body.append('{}* {}{}'.format(spaces, v, splt))
                self.nl()
            self.nl()
        return


    def force_end(self):
        pass

