
from .config import get_config, get_cache
from .brief import brief_bits

class m3u8:
    def __init__(self, briefs):
        self.url = get_config('url')
        if not self.url:
            self.url = ''
        elif not self.url.endswith('/'):
            self.url = self.url + '/'
        self.briefs = briefs
        self.mode_id = None
        self.local = {}
        self.bit_bag = {}
        cache = get_cache()
        self.dcache = {}
        for f in briefs:
            x = cache.get(f)
            if x is None:
                continue
            categories = x.get('categories',['phlog'])
            if len(categories) == 1 and categories[0].startswith('!'):
                continue
            if not x:
                self.local[f] = briefs[f]
            else:
                self.local[f] = x
            if 'bits' not in self.local[f]:
                bits = brief_bits(self.local[f])
            else:
                bits = self.local[f]['bits']
            if not bits:
                bits = []
            self.bit_bag[f] = bits

    def mode(self, mode = None):
        self.mode_id = mode

    def duration(self, n):
        if n in self.dcache:
            return self.dcache[n]
        b = self.local.get(n)
        if not b:
            return 0
        duration = b.get('conf',{}).get('audio',{}).get('duration')
        if not duration:
            return 0
        if '.' in duration:
            duration = duration.split('.',1)[0]
        if ':' in duration:
            ds = duration.split(':')
            secs = int(ds[-1])
            mins = int(ds[-2])
            hrs = 0
            if len(ds) > 2:
                hrs = int(df[-3])
            mins = mins + hrs * 60
            secs = secs + mins * 60
        else:
            secs = int(duration)
        return secs

    def __len__(self):
        num = 0

        if self.mode_id is None:
            options = ('mp4', 'spx', 'ogg', 'mp3', 'flac')
        elif isinstance(self.mode_id, str):
            options = [self.mode_id]
        else:
            options = self.mode_id
        for b in sorted(self.bit_bag, reverse=True):
            bits = self.bit_bag[b]
            if not bits:
                continue
            for ext in options:
                if ext in bits:
                    num += 1
                    break
        return num


    def __str__(self):
        out = []
        if self.mode_id is None:
            options = ('spx', 'ogg', 'mp3', 'flac')
        elif isinstance(self.mode_id, str):
            options = [self.mode_id]
        else:
            options = self.mode_id

        out.append('#EXTM3U')
        for b in sorted(self.briefs, reverse=True):
            brief = self.briefs[b]
            bits = self.bit_bag.get(b)
            if not bits:
                continue
            fnames = None

            for ext in options:
                if ext in bits:
                    fnames = bits[ext]
                    break
            if not fnames:
                continue
            rel = brief['rel_path']

            t = '#EXTINF:{}, yam655.com - {}'.format(self.duration(b),
                    brief['title'])
            out.append(t)

            if len(fnames) > 1:
                # Expect extra files to have extra identification making length longer
                fnames.sort(key=lambda x: len(x))
            out.append('{}{}/{}'.format(self.url, rel, fnames[0]))
        
        return '\n'.join(out)


