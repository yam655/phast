#!/usr/bin/env python3

from pathlib import Path

from .config import rel_from_root, unconf, get_config, find_config_path, get_base_name, get_category_name, get_cache
from .brief import get_brief
from .out_rst import rst
from .out_gmi import gemini
from .out_gophermap import gophermap
from .meta_utils import ignore_path, j_to_stamp


def process_mockd(bucket, *, out, source, localcache, title_override=None):
    title = bucket['title']
    rel = Path(bucket['rel_path'])

    if title_override:
        title = title_override
    out.title(title)
    out.nl()

    out.section('Recent Changes')

    limit = 5
    for s in sorted(localcache.items(), key=lambda x: x[1]['__TOUCHED__'], reverse=True):
        if limit <= 0:
            break
        limit -= 1
        sop, bucket = s
        #bucket = localcache.get(sop)
        ltitle = bucket['title']
        labstract = bucket.get('abstract')
        lrel = Path(bucket['rel_path'])
        add_suffix = False
        is_file = False
        entries = len(bucket.get("__ordinal", []))
        ents = f'{entries} entries'
        if bucket['__TOUCHED__'].startswith('j/'):
            giblet = j_to_stamp(bucket['__TOUCHED__'])
        else:
            giblet = None
        ref = '{}'

        if lrel.parts[0] == 'c':
            ltitle = get_category_name(ltitle)
            if ltitle.startswith('!'):
                continue

        if giblet is not None:
            out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=f'- ({giblet}) {ref}: {ents}')
        else:
            out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=f'- {ref}: {ents}')
        out.nl()

    have_popular = False

    for s in sorted(localcache.items(), key=lambda x: len(x[1].get('__ordinal', [])) + len(x[1].get('ordinal', {})), reverse=True):
        sop, bucket = s
        #bucket = localcache.get(sop)
        ltitle = bucket['title']
        labstract = bucket.get('abstract')
        lrel = Path(bucket['rel_path'])
        add_suffix = False
        is_file = False
        entries = len(bucket.get("__ordinal", [])) + len(bucket.get("ordinal",{}))
        if entries <= 1:
            continue
        giblet = f'{entries} entries'
        ref = '{}'
        if not have_popular:
            out.section('Popular')
            have_popular = True

        if lrel.parts[0] == 'c':
            ltitle = get_category_name(ltitle)
            if ltitle.startswith('!'):
                continue

        out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=f'- ({giblet}) {ref}')
        out.nl()


    out.section('Alphabetical')

    for s in sorted(localcache.items(), key=lambda x: x[1]['title'].casefold()):
        sop, bucket = s
        #bucket = localcache.get(sop)
        ltitle = bucket['title']
        labstract = bucket.get('abstract')
        lrel = Path(bucket['rel_path'])
        add_suffix = False
        is_file = False
        entries = len(bucket.get("__ordinal", [])) + len(bucket.get("ordinal",{}))
        ents = f'{entries} entries'
        giblet = None
        if bucket['__TOUCHED__'].startswith('j/'):
            giblet = j_to_stamp(bucket['__TOUCHED__'])
        ref = '{}'

        if lrel.parts[0] == 'c':
            ltitle = get_category_name(ltitle)
            if ltitle.startswith('!'):
                continue

        if giblet is not None:
            out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=f'- {ref}: ({giblet}) {ents}')
        else:
            out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=f'- {ref}: {ents}')
        out.nl()

    out.section('Hidden')

    numhidden = 0
    for s in sorted(localcache.items(), key=lambda x: x[1]['title'].casefold()):
        sop, bucket = s
        #bucket = localcache.get(sop)
        ltitle = bucket['title']
        labstract = bucket.get('abstract')
        lrel = Path(bucket['rel_path'])
        add_suffix = False
        is_file = False
        entries = len(bucket.get("__ordinal", [])) + len(bucket.get("ordinal",{}))
        ents = f'{entries} entries'
        giblet = None
        if bucket['__TOUCHED__'].startswith('j/'):
            giblet = j_to_stamp(bucket['__TOUCHED__'])
        ref = '{}'

        if lrel.parts[0] == 'c':
            ltitle = get_category_name(ltitle)
            if not ltitle.startswith('!'):
                continue
            ltitle = ltitle[1:]
        elif 'hidden' not in bucket:
            continue

        numhidden += 1
        if giblet is not None:
            out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=f'- {ref}: ({giblet}) {ents}')
        else:
            out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=f'- {ref}: {ents}')
        out.nl()

    if numhidden == 0:
        out.add('(nothing hidden)')


    out.nl()
    return out

def fill_local_cache(bucket):
    cache = get_cache()
    localcache = {}
    for f in bucket.get('mock_dir', []):
        sf = str(f)
        if sf in cache:
            value = cache[sf]
            if value:
                localcache[f] = value
    return localcache

def process(source, bucket):
    localcache = fill_local_cache(bucket)

    rel = bucket.get('rel_path')
    rst_out = rst()
    title_override = get_base_name(rel, 'labels:unicode')
    contents = process_mockd(bucket, out=rst_out, source=rel,
                        localcache=localcache, title_override=title_override)
    html = rst_out.render(path="{}/folder.rst".format(rel))
    gmi = process_mockd(bucket, out=gemini(), source=rel,
                        localcache=localcache, title_override=title_override)
    title_override = get_base_name(rel, 'labels:plain')
    gmap = process_mockd(bucket, out=gophermap(), source=rel,
                        localcache=localcache, title_override=title_override)
    out = [{
            'rel_path':rel, 
            'is_file': False,
            'html':html, 
            'source':str(contents), 
            'index.gmi':str(gmi),
            'gophermap':str(gmap)}]
    return out

