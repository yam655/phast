#!/usr/bin/env python3

from .config import find_config_path, get_cache
from .config import rel_from_root, unconf
from .load_file import find_something, sensible_path_title
from .deps import dep_add, dep_changed

def scan_rst_file(contents, what, *, under=None):
    if under is None:
        under = what
        what = None
    have_under = False
    have_what = False
    for line in contents.splitlines():
        linestrip = line.strip()
        if have_what:
            return linestrip
        elif have_under:
            if what is None:
                return linestrip
            elif not linestrip:
                have_under = False
            elif linestrip.startswith(what):
                if linestrip == what:
                    have_what = True
                else:
                    return linestrip[len(what):].strip()
        elif line.startswith(under):
            if linestrip == under:
                have_under = True
            elif what is None:
                return linestrip[len(under):].strip()
            else:
                have_under = True
    return ''

def find_title_rst(text):
    title = None
    for t in text.splitlines():
        if title:
            if not t or t != t[0] * len(t):
                title = None
            break
        if not t:
            continue
        if not t[0].isalnum():
            continue
        title = t
    return title

def load(source, data):
    if not source or not source.is_file():
        return None, None
    if not source.suffix == '.rst':
        return None, None

    if data is None:
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']
    changed = False
    rel = rel_from_root(source.parent / source.stem)
    data['is_file'] = True
    data['add_suffix'] = True
    data['rel_path'] = rel

    if dep_changed(source, deps):
        changed = True
        dep_add(source, deps)
        data['page'] = True

        contents = source.read_text()
        data['contents'] = contents

        abstract = scan_rst_file(contents, ':description:', under='.. meta::')
        title = find_title_rst(contents)
        if not title:
            title = source.stem.replace('_',' ')

        data['abstract'] = abstract
        data['title'] = title

    return data, changed


