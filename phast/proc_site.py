#!/usr/bin/env python3

from pathlib import Path

from .config import get_cache
from .brief import get_brief
from .out_rst import rst
from .out_gophermap import gophermap


def process_site(bucket, *, out, source, localcache):
    product = bucket['site']
    title = bucket['title']
    prefix = product.get('prefix','')
    rel = Path(bucket['rel_path'])

    out.title(title)
    out.add(prefix)
    out.nl()

    for sop in localcache:
        bucket = localcache.get(sop)
        if not bucket:
            continue
        ltitle = bucket['title']
        labstract = bucket['abstract']
        lrel = bucket['rel_path']
        is_file = bucket.get('is_file')

        out.rel_link(lrel.relative_to(rel), ltitle, labstract, is_file=is_file, context='- {}')

        if labstract:
            out.under(labstract)
            out.nl()

    out.nl()
    out.add('..')
    out.nl()
    return out

def fill_local_cache(source, limit=20):
    cache = get_cache()
    localcache = {}
    jdir = source / 'j'
    for d1 in sorted(jdir.glob('*'), reverse=True):
        for d2 in sorted(d1.glob('*'), reverse=True):
            k = str(d2)
            brief = get_brief(k)
            if brief:
                localcache[k] = brief
            if len(localcache) > limit:
                break
        if len(localcache) > limit:
            break
    return localcache

def process(source, bucket):
    localcache = fill_local_cache(source)
    rst_out = rst()
    contents = process_site(bucket, out=rst_out, source=source, localcache=localcache)
    html = rst_out.render(path="{}/folder.rst".format(source))
    gmap = process_site(bucket, out=gophermap(), source=source, localcache=localcache)
    out = [{
            'rel_path':bucket.get('rel_path'), 
            'is_file': False,
            'html':html, 
            'source':str(contents), 
            'gophermap':str(gmap)}]
    if 'support' in bucket:
        for d in bucket['support'].split(';'):
            d = d.strip()
            s = source / d
            for f in s.glob('*'):
                rf = rel_from_root(f)
                out.append({'copy': (f, rf)})
    return out

