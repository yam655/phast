#!/usr/bin/env python3

from pathlib import Path
from collections import OrderedDict

from .config import get_cache, get_config, find_config_path, read_config, get_category_list
from .config import rel_from_root, unconf, get_base_name, get_category_name_list, get_category_name
from .brief import get_brief
from .out_rst import rst
from .out_gmi import gemini
from .out_gophermap import gophermap
from .out_m3u8 import m3u8
from .meta_utils import ignore_path, j_to_stamp



def process_cats(*, out, localcache, max_hot):
    out.nl()
    other_categories = []
    limit = 3
    cache = get_cache()

    out.html('<div class="row">')
    # out.subtitle(subtitle)
    out.subtitle('Recent Categories')
    out.nl()
    max_hot2 = max_hot
    max_hot = (limit + 2) * max_hot2
    # max_hot2 = 1

    for s in sorted(localcache.items(), key=lambda x: x[1]['__TOUCHED__'], reverse=True):
        if limit <= 0:
            other_categories.append(s)
            continue
        sop, bucket = s
        subtitle = bucket['title']
        labstract = bucket.get('abstract')
        lrel = Path(bucket['rel_path'])
        add_suffix = False
        is_file = False
        entries = len(bucket.get("__ordinal", [])) + len(bucket.get("ordinal", {}))
        ents = f'{entries} entries'
        ref = '{}'
        # data = cache[f'category:{subtitle}']
        data = bucket

        subtitle = get_category_name(subtitle)
        if not subtitle or subtitle[0] == '!':
            other_categories.append(s)
            continue
        limit -= 1

        out.nl()

        if entries - max_hot2 > 0:
            more_entries = entries - max_hot2
            entries_text = 'entries'
            if more_entries == 1:
                entries_text = 'entry'
            out.rel_link(lrel,
                    f'{subtitle} ({entries - max_hot2} more)', None,
                    include_parent=True, add_suffix=False,
                    is_file=False, only_title=True)
        else:
            out.rel_link(lrel, f'{subtitle}', None,
                    include_parent=True, add_suffix=False,
                    is_file=False, only_title=True)

        out.nl()

        os1 = data.get('ordinal', [])
        os2 = data.get('__ordinal', {})
        out.nl()
        hot = 0

        for o in sorted(os1, reverse=True):
            if hot >= max_hot2:
                break
            if localcache is not None:
                b = localcache[os1[o]]
            if not b:
                continue
            relb = Path(b['rel_path'])
            stampb = j_to_stamp(relb)
            titleb = b.get('title', os1[o].rsplit('/',1)[-1])
            cat = b.get('categories', ['uncategories'])
            cattitle = get_category_name_list(cat, drop_hidden=True)
            if not cattitle:
                continue
            hot += 1
            context = out.rst_only(f'{o}. {"{}"} :small:`({stampb})`', f'{o}. {"{}"} ({stampb})')
            out.rel_link(relb, titleb, None,  # rel/root
                                is_file=False, add_suffix=False, include_parent=True,
                                context=context, only_title=True)

        out.nl()

        for o in sorted(os2, reverse=True):
            if hot >= max_hot2:
                break
            b = get_brief(os2[o])
            if not b:
                continue
            relb = Path(b['rel_path'])
            stampb = j_to_stamp(relb)
            titleb = b.get('title', os2[o].rsplit('/',1)[-1])
            cat = b.get('categories', ['uncategories'])
            cattitle = get_category_name_list(cat, drop_hidden=True)
            if not cattitle:
                continue
            hot += 1
            context = out.rst_only(f'- {"{}"} :small:`({stampb})`', f'- {"{}"} ({stampb})')
            out.rel_link(relb, titleb, None, # rel/root
                            is_file=False, add_suffix=False, include_parent=True,
                            context=context, only_title=True)

    out.nl()
    out.html('</div>')

    out.html('<div class="row">')

    out.nl()
    out.subtitle("Other categories")
    out.nl()

    hot = 0
    more_others = len(other_categories)
    for s in other_categories:
        sop, bucket = s
        ltitle = bucket['title']
        labstract = bucket.get('abstract')
        lrel = Path(bucket['rel_path'])
        add_suffix = False
        is_file = False
        entries = len(bucket.get("__ordinal", [])) + len(bucket.get("ordinal", {}))
        ents = f'{entries} entries'
        ref = '{}'
        try:
            giblet = j_to_stamp(bucket['__TOUCHED__'])
        except:
            giblet = None

        ltitle = get_category_name(ltitle)
        if ltitle.startswith('!'):
            continue
        if hot >= max_hot:
            break
        hot += 1

        if giblet is None:
            context=f'- {ref}: {ents}'
        else:
            context=f'- {ref}: {ents} [{giblet}]'
        out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context=context)
        out.nl()

    entries = len(other_categories)
    if hot >= max_hot and entries - hot > 0:
        subtitle = 'Categories'
        more_entries = entries - hot
        entries_text = 'entries'
        if more_entries == 1:
            entries_text = 'entry'
        out.rel_link(Path('c'),
                    f'view {entries - hot} more {entries_text} in "{subtitle}"', None,
                    include_parent=True, add_suffix=False,
                    is_file=False, context='- ({})', only_title=True)

    out.html('</div>')

    return out


def sniff_products(localcache, product_list, other_products):
    min_tracks = 10
    for s in sorted(localcache.items(), key=lambda x: x[1]['__TOUCHED__'], reverse=True):
        sop, bucket = s
        product = bucket.get('product', {})
        graphic = product.get('image')
        graphic_gif = product.get('image.gif')
        graphic_thumb = product.get('image.thumb', graphic_gif)
        if graphic and not graphic_gif:
            print(f'WARNING: image without image.gif in {sop}')
        graphic = False
        if graphic_gif:
            graphic = True
        if not product or 'hidden' in product:
            continue
        os2 = bucket.get('__ordinal', {})
        os1 = bucket.get('ordinal', [])
        if not graphic and (len(os1) + len(os2)) < min_tracks:
            if len(os2) > 0:
                other_products.append(s)
            continue
#       if len(os1) == 0 and len(os2) == 0:
#           other_products.append(s)
#           continue

        ctitle = product.get('category')
        if ctitle:
            ctitle = ctitle[0]
        if ctitle:
            ctitle = get_category_name_list(ctitle, drop_hidden=True)
            if not ctitle:
                other_products.append(s)
                continue

        product_list.append((sop, bucket))
    if len(product_list) % 2 == 0:
        other_products.insert(0, product_list[-1])
        del product_list[-1]
    return product_list


def process_prod(*, out, localcache, max_hot):
    out.nl()
    product_list = []
    other_products = []
#   limit = 7
    cache = get_cache()

    out.nl()
    out.add(out.rst_only('.. role:: small', ''))
    out.nl()

    sniff_products(localcache, product_list, other_products)

    for sop, bucket in product_list:
#       if limit <= 0:
#           other_products.append(s)
#           continue
        product = bucket.get('product', {})
        graphic_gif = product.get('image.gif')
        graphic_thumb = product.get('image.thumb', graphic_gif)
        graphic_alt = product.get('image.alt', [''])[0].replace('&', '&amp;').replace('"', '&quot;')
        graphic = False
        if graphic_gif:
            graphic = True
        subtitle = bucket['title']
        # data = cache.get(sop)
        data = bucket
        if 'hidden' in product:
            continue
        os2 = data.get('__ordinal', {})
        os1 = data.get('ordinal', {})
#       if not graphic and (len(os1) + len(os2)) < 10:
#           # print('NOT ENOUGH PAGES.')
#           if len(os2) > 0:
#               other_products.append(s)
#           continue
        labstract = bucket.get('abstract')
        lrel = Path(bucket['rel_path'])
        add_suffix = False
        is_file = False
        entries = len(bucket.get("__ordinal", {})) + len(bucket.get("ordinal", {}))
        ents = ''
        if entries == 1:
            ents = f'{entries} entry'
        else:
            ents = f'{entries} entries'
        journal_entries = len(bucket.get('--ordinal',{}))
        if journal_entries:
            journal_text = 'journals'
            if journal_entries == 1:
                journal_text = 'journal'
            blog_text = f'{journal_entries} {journal_text}'
            if entries:
                ents = f'{ents} / {blog_text}'
            else:
                ents = blog_text
        ref = '{}'
        try:
            giblet = j_to_stamp(bucket['__TOUCHED__'])
        except:
            giblet = None

#       limit -= 1

        out.html('<div class="row">')

        out.subtitle(subtitle)
        out.nl()

        os1 = data.get('ordinal', {})
        os2 = data.get('__ordinal', {})
        os3 = data.get('--ordinal', {})
        out.nl()
        hot = 0

        if graphic:
            graphic_thumb = rel_from_root(graphic_thumb[0])
            graphic_gif = rel_from_root(graphic_gif[0])
            out.html(f'<figure><picture style="border:1px"><source srcset="{graphic_thumb}" /> <img src="{graphic_gif}" width="300" height="300" alt="{graphic_alt}"/> <figcaption>')

        def get_and_add(product, what, addto):
            value = product.get(what)
            if value:
                value = value[0]
                if isinstance(value, str):
                    value = value.splitlines()
                for v in value:
                    addto.add(v.strip())
            else:
                value = None
            return value

        def get_and_descend(product, what):
            value = product.get(what)
            if value:
                value = value[0]
            else:
                value = None
            return value

        genres = set()
        mgenre = get_and_add(product, 'genre/music', genres)
        agenre = get_and_add(product, 'genre/audio', genres)
        fgenre = get_and_add(product, 'genre/fiction', genres)
        ngenre = get_and_add(product, 'genre/nonfiction', genres)
        genre = get_and_add(product, 'genre', genres)
        glist = "; ".join(list(genres))

        out.nl()
        if giblet is None:
            context = out.rst_only(f'{ref}: :small:`{ents}`', f'{ref}: {ents}')
        else:
            context = out.rst_only(f'{ref}: :small:`{ents} [{giblet}]`',
                                   f'{ref}: {ents} [{giblet}]')
#       if graphic and glist:
#           labstract = None
        out.rel_link(lrel, subtitle, labstract, only_title=True, # rel/root
                            add_suffix=add_suffix, is_file=is_file, context=context)
        out.nl()

        surl = get_and_descend(product, 'stream_url')
        durl = get_and_descend(product, 'download_urls')
        purl = get_and_descend(product, 'purchase_urls')
        price = get_and_descend(product, 'purchase_price')
        if labstract is None and glist:
            if len(genres) == 1:
                out.add(f'- Genre: {glist}')
            else:
                out.add(f'- Genres: {glist}')
        if durl:
            out.web_link(durl, 'Download', None, context='- {}')
        if purl:
            if price:
                price = out.rst_only(f' :small:`({price})`', f' ({price})')
            out.web_link(purl, 'Purchase', None, extra=price, context='- {}')
        if surl and not durl and not purl:
            out.web_link(surl, 'Stream', None, context='- {}')

        platforms = product.get('platforms')
        if platforms:
            out.add(f'- Platforms: {platforms[0]}')
        tracks = get_and_descend(product, 'tracks')
        dur = get_and_descend(product, 'audio/duration')
        if not dur:
            dur = get_and_descend(product, 'duration')
        if tracks and dur:
            out.add(f'- Duration: {tracks} tracks / {dur}')
        elif dur and not tracks:
            out.add(f'- Duration: {dur}')
        elif tracks and not dur:
            out.add(f'- Duration: {tracks} tracks')
        out.nl()

        if graphic and out.inline_images():
            entry = None
            blog = None
            #print ("GRAPHIC")
            if os1 and os2:
                #print ("os1 AND os2")
                os1k = list(sorted(os1))[0]
                os1v = cache.get(os1[os1k])
                os2k = list(sorted(os2))[0]
                os2v = cache.get(os2[os2k])
                entry = os2v
                if os1v['rel_path'] > os2v['rel_path']:
                    entry = os1v
            elif os1:
                #print ("os1 AND NOT os2")
                os1k = list(sorted(os1))[0]
                entry = cache.get(os1[os1k])
            elif os2:
                #print ("NOT os1 AND os2")
                os2k = list(sorted(os2))[0]
                entry = cache.get(os2[os2k])

            if os3:
                #print ("os3 the JOURNAL")
                blog = list(sorted(os3))[0]
                blog = cache.get(os3[blog])
                if entry:
                    #print ("os3 AND entry")
                    if entry['rel_path'] > blog['rel_path']:
                        blog = None
                        #print ("os3 AND entry: entry SELECTED")
                    else:
                        entry = None
                        #print ("os3 AND entry: blog SELECTED")

            b = blog
            if blog and not isinstance(blog, dict):
                raise ValueError(repr(blog))
            if not b:
                b = entry
                if entry and not isinstance(entry, dict):
                    raise ValueError(repr(entry))
            if not b:
                continue

            else:
                if not isinstance(b, dict):
                    raise ValueError(repr(b))
                relb = Path(b['rel_path'])
                stampb = j_to_stamp(relb)
                titleb = b.get('title', str(relb).rsplit('/',1)[-1])
                summary = b.get('abstract')
                cat = b.get('categories', ['uncategories'])
                cattitle = get_category_name_list(cat, drop_hidden=True)
                if cattitle:
                    if glist:
                        cattitle = ''
                    else:
                        cattitle = f'[{cattitle}]'
                    updateblog = 'entry'
                    if blog:
                        updateblog = 'journal'
                    if cattitle:
                        context = out.rst_only(f'*Last {updateblog}:* {stampb} :small:`{cattitle}` {ref}',
                                               f'*Last {updateblog}:* {stampb} {cattitle} {ref}')
                    else:
                        context = f'*Last {updateblog}:* {stampb} {ref}'
                    out.rel_link(relb, titleb, None, # rel/root
                                is_file=False, add_suffix=False, include_parent=True,
                                context=context, only_title=True)
            out.html('</figcaption>')

        else:
            if os1 or os2:
                if not out.gmi('### Recent entries'):
                    out.add('*Recent entries:*')
                out.nl()

            for o in sorted(os1, reverse=True):
                b = get_brief(os1[o])
                if not b:
                    continue
                if hot >= max_hot:
                    break
                relb = Path(b['rel_path'])
                stampb = j_to_stamp(relb)
                titleb = b.get('title', os1[o].rsplit('/',1)[-1])
                summary = b.get('abstract')
                cat = b.get('categories', ['uncategories'])
                cattitle = get_category_name_list(cat, drop_hidden=True)
                if not cattitle:
                    continue
                hot += 1
                if glist:
                    cattitle = ''
                else:
                    cattitle = f'[{cattitle}]'
                cattitle2 = ""
                if cattitle:
                    cattitle2 = " " + cattitle
                context = out.rst_only(f'- {ref} :small:`({stampb}){cattitle2}`',
                                       f'- {ref} ({stampb}){cattitle2}')
                out.rel_link(relb, titleb, None, # rel/root
                                is_file=False, add_suffix=False, include_parent=True,
                                context=context, only_title=True)

            for o in sorted(os2, reverse=True):
                b = get_brief(os2[o])
                if not b:
                    continue
                if hot >= max_hot:
                    break
                relb = Path(b['rel_path'])
                stampb = j_to_stamp(relb)
                titleb = b.get('title', os2[o].rsplit('/',1)[-1])
                summary = b.get('abstract')
                cat = b.get('categories', ['uncategories'])
                cattitle = get_category_name_list(cat, drop_hidden=True)
                if not cattitle:
                    continue
                hot += 1
                if glist:
                    cattitle = ''
                else:
                    cattitle = f'[{cattitle}]'
                cattitle2 = ""
                if cattitle:
                    cattitle2 = " " + cattitle
                context = out.rst_only(f'- {ref} :small:`({stampb}){cattitle2}`',
                                       f'- {ref} ({stampb}){cattitle2}')
                out.rel_link(relb, titleb, None, # rel/root
                                is_file=False, add_suffix=False, include_parent=True,
                                context=context, only_title=True)

            if hot >= max_hot and entries - hot > 0:
                more_entries = entries - hot
                entries_text = 'entries'
                if more_entries == 1:
                    entries_text = 'entry'
                out.rel_link(lrel,
                        f'view {entries - hot} more {entries_text} in "{subtitle}"', None,
                        include_parent=True, add_suffix=False,
                        is_file=False, context='- ({})', only_title=True)

        out.html('</div>')


#   if limit % 2 == 0:
    if True:

        out.html('<div class="row">')
        out.nl()
        out.subtitle("Other products")
        out.nl()

        hot = 0
        more_others = len(other_products)
        for s in other_products:
            sop, bucket = s
            ltitle = bucket['title']
            labstract = bucket.get('abstract')
            lrel = Path(bucket['rel_path'])
            add_suffix = False
            is_file = False
            entries = len(bucket.get("__ordinal", [])) + len(bucket.get("ordinal", {}))
            ents = f'{entries} entries'
            ref = '{}'
            try:
                giblet = j_to_stamp(bucket['__TOUCHED__'])
            except:
                giblet = None

            if 'hidden' in bucket.get('product',{}):
                continue
            if 'incomplete' in bucket.get('product',{}):
                continue
            if hot >= max_hot:
                break
            hot += 1

            if giblet is None:
                context=f'- {ref}: {ents}'
            else:
                context=f'- {ref}: {ents} [{giblet}]'
            out.rel_link(lrel, ltitle, labstract, only_title=True, # rel/root
                            add_suffix=add_suffix, is_file=is_file, context=context)
            out.nl()

        entries = len(other_products)
        if hot >= max_hot and entries - hot > 0:
            subtitle = 'Products'
            more_entries = entries - hot
            entries_text = 'entries'
            if more_entries == 1:
                entries_text = 'entry'
            out.rel_link(Path('p'),
                        f'view {entries - hot} more {entries_text} in "{subtitle}"', None,
                        include_parent=True, add_suffix=False,
                        is_file=False, context='- ({})', only_title=True)

        out.html('</div>')
    return out



def process_site(bucket, *, out, source, localcache_cats, localcache_prod, have_m3u8, max_hot, show_underground):
    product = bucket['site']
    title = bucket['title']
    prefix = product.get('prefix','')
    rel = Path(bucket['rel_path'])

    out.title(title)
    out.add(prefix)
    out.nl()

    process_prod(out=out, localcache=localcache_prod, max_hot=14)
    process_cats(out=out, localcache=localcache_cats, max_hot=2)

    if show_underground:
        out.nl()
        underground = get_config('underground')
        if underground:
            underground = underground.splitlines()
        else:
            underground = []
        for underline in underground:
            underparts = underline.split('|')
            out.rel_link(underparts[0].strip(), underparts[1].strip(), None, only_title=True, # rel/root
                            is_file=False, add_suffix=False,
                            context='Not available on the web: {"{}"}')
            out.nl()

    rel = Path(bucket.get('rel_path'))
    out.nl()
    if have_m3u8 > 0:
        out.nl()
        relb = 'recent.m3u8'
        songs = ' ({} songs and videos)'.format(have_m3u8)
        out.rel_link(relb, 'Remote Playlist', None, only_title=True, # rel/root
                            is_file=True, add_suffix=False,
                            context='Listen: ({}) {}{}'
                                    .format(relb.rsplit('.',1)[-1],'{}', songs))
    note_feeds(out)
    out.nl()
    return out

def note_feeds(out):
    config = read_config()
    feeds = config.get('feeds', {})
    for filen in feeds:
        line = feeds[filen]
        l = line.split('|')
        title = l[0].strip()
        if len(l) < 2:
            continue
        cnt = int(l[1])
        tpes = None
        if len(l) > 2:
            tpes = l[-1].strip().split()
            if not tpes:
                tpes = None
        if title.startswith("!"):
            continue
        else:
            out.rel_link(filen, title, None, only_title=True, 
                            is_file=True, add_suffix=False,
                            context='Feed: (xml) {} ({} posts)'.format('{}', cnt))

def fill_local_cache_mock_category():
    cache = get_cache()
    bucket = cache['category:__ALL__']
    localcache = {}
    for f in bucket.get('mock_dir', []):
        sf = str(f)
        if sf in cache:
            value = cache[sf]
            if value:
                localcache[f] = value
    return localcache

def fill_local_cache_mock_product():
    cache = get_cache()
    bucket = cache['product:__ALL__']
    localcache = {}
    for f in bucket.get('mock_dir', []):
        sf = str(f)
        if sf in cache:
            value = cache[sf]
            if value:
                localcache[f] = value
    return localcache

def fill_local_cache(source, *, limit, reqtype=None):
    cache = get_cache()
    localcache = {}
    jdir = source / 'j'
    if isinstance(reqtype, str):
        reqtype = [reqtype]
    for d1 in sorted(jdir.glob('*'), reverse=True):
        for d2 in sorted(d1.glob('*'), reverse=True):
            if ignore_path(d2):
                continue
            k = str(d2)
            brief = get_brief(k)
            if not brief:
                continue
            if reqtype is not None:
                if 'bits' not in brief:
                    continue
                bits = brief['bits']
                found = False
                for req  in reqtype:
                    if req in bits:
                        found = True
                        break
                if not found:
                    continue
    
            localcache[k] = brief
            if len(localcache) >= limit:
                break
        if len(localcache) >= limit:
            break
    return localcache


def process(source, bucket):
    recent_pages = get_config('recent_pages')
    recent_media = get_config('recent_media')
    if not recent_pages:
        recent_pages = 10
    else:
        recent_pages = int(recent_pages)
    if not recent_media:
        recent_media = recent_pages
    else:
        recent_media = int(recent_media)
    max_hot= get_config('max_hot')
    if max_hot is None:
        max_hot = 5

    localcache_cats = fill_local_cache_mock_category()
    localcache_prod = fill_local_cache_mock_product()

    rst_out = rst()
    m3u8_bits = m3u8(fill_local_cache(source, limit=recent_media,
                    reqtype=('ogg', 'mp3', 'spx', 'mp4', 'flac')))
    m3u8_text = str(m3u8_bits)
    contents = process_site(bucket, out=rst_out, source=source,
                    localcache_cats=localcache_cats,
                    localcache_prod=localcache_prod,
                    have_m3u8 = len(m3u8_bits), max_hot=max_hot,
                    show_underground = False)
    html = rst_out.render(path="{}/folder.rst".format(source))
    gmi = process_site(bucket, out=gemini(), source=source,
                    localcache_cats=localcache_cats,
                    localcache_prod=localcache_prod,
                    have_m3u8 = len(m3u8_bits), max_hot=max_hot,
                    show_underground = True)
    gmap = process_site(bucket, out=gophermap(), source=source,
                    localcache_cats=localcache_cats,
                    localcache_prod=localcache_prod,
                    have_m3u8 = len(m3u8_bits), max_hot=max_hot,
                    show_underground = True)
    out = [ {
                'rel_path': bucket.get('rel_path') / 'recent.m3u8',
                'is_file': True,
                'create': m3u8_text
            },
            {
                'rel_path':bucket.get('rel_path'), 
                'is_file': False,
                'html':html, 
                'source':str(contents), 
                'index.gmi':str(gmi),
                'gophermap':str(gmap)
            }]
    if 'support' in bucket:
        for d in bucket['support'].split(';'):
            d = d.strip()
            s = source / d
            for f in s.glob('*'):
                rf = rel_from_root(f)
                out.append({'copy': (f, rf)})
    return out




