#!/usr/bin/env python3

import taglib

from .config import rel_from_root
from .load_folder import dep_add, dep_changed


def get_duration(song):
    secs = song.length
    mins = None
    if secs >= 60:
        mins = secs // 60
        secs %= 60
    hours = None
    if mins is not None and mins >= 60:
        hours = mins // 60
        hours %= 60
    out = []
    if hours is not None:
        out.append(str(hours))
        out.append(":")
    if mins is None:
        mins = 0
    out.append("%02u:%02u" % (mins, secs))
    return "".join(out)


def load_audio(data, path):

    meta = {}
    data['audio'] = meta

    song = taglib.File(str(path))
    duration = meta['duration'] = get_duration(song)
    for tag in song.tags:
        meta[tag] = song.tags[tag]

    genres = []
    if 'GENRE' in meta:
        genres = meta['GENRE']
        if isinstance(genres,str):
            genres = genres.strip().splitlines()
        if 'spoken' in genres or 'noise' in genres or 'sound' in genres:
            meta['genre/audio'] = genres
        else:
            meta['genre/music'] = genres
        del meta['GENRE']

    title = meta.get('TITLE')
    if title is not None and not isinstance(title, str):
        title = title[0].strip()
    if not title:
        rel = data['rel_path']
        title = rel.stem.replace('_',' ')
    data['title'] = title

    artist = None
    if 'ARTIST' in meta:
        artist = '; '.join(meta.get('ARTIST'))
    album = None
    if 'ALBUM' in meta:
        album = meta.get('ALBUM')
    if album and not isinstance(album, str):
        album = album[0]

    if 'TRACKNUMBER' in meta:
        tn = meta['TRACKNUMBER']
        if '/' in tn:
            meta['TRACKNUMBER'] = tn.split('/', 1)[0]

    if 'LYRICS:NONE' in meta:
        meta['lyrics'] = meta['LYRICS:NONE'][0].strip()
        if meta['lyrics']:
            meta['poem'] = meta['lyrics']
        del meta['LYRICS:NONE']
    if 'LYRICS' in meta:
        if ['', ''] == meta['LYRICS']:
            del meta['LYRICS']
        else:
            raise(repr((source, meta['LYRICS'])))

    abstract = []
    abstract.append('|Audio|')
    if 'genre/music' in meta:
        if genres:
            abstract.append(genres[0])
        abstract.append('music')
    else:
        if genres:
            abstract.append(genres[0])
            if genres[0] == 'spoken':
                abstract.append('audio')
        else:
            abstract.append('audio')
    
    if artist:
        abstract.append('by')
        abstract.append(artist)
    if album:
        abstract.append('from')
        abstract.append(album)
    if duration:
        abstract.append('({})'.format(duration))

    data['abstract'] = ' '.join(abstract)
    return meta

audioFolderMapping = {
    'ALBUMARTIST': 'AlbumArtist',
    'ALBUM': 'Album',
    'ARTIST': 'Artist',
    'DATE': 'Year',
    'TITLE': 'Title',
    'LYRICIST': 'Lyricist',
    'POET': 'Lyricist',
    'COMPOSER': 'Composer',
    'duration': 'Duration',
    'lyrics': 'lyrics',
    'poem': 'poem',
    'genre/music': 'genre/music',
    'TRACKNUMBER': 'TrackNumber',
    'BAND': 'Band',
    'ENCODED-BY': '',
    'ENCODED_BY': '',
    'CODING_HISTORY': '',
    'ENCODER': '',
    'TIME_REFERENCE': '',
}
def audio_to_folder(audio, source):
    fldr = {}
    for k in audio.keys():
        v = audio[k]
        if not v:
            continue
        if isinstance(v, str):
            v = [v.strip()]
        if k not in audioFolderMapping:
            raise ValueError('"{}" : <{}> did not have a Folder mapping in {}'.format(k, v, source))
        k1 = audioFolderMapping[k]
        if k1:
            if k1 not in fldr:
                fldr[k1] = v
            else:
                fldr[k1].extend(v)
    return fldr

def load(source, data):
    if source.is_dir():
        return None, None
    if not source.suffix in ['.mp3', '.ogg', '.flac', '.wav', '.spx']:
        return None, None

    if data is None:
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']
    changed = False
    rel = rel_from_root(source)
    data['rel_path'] = rel

    if dep_changed(source, deps):
        dep_add(source, deps)
        changed = True
        data['copy'] = (source, rel)
        audio = load_audio(data, source)
        folder = audio_to_folder(audio, source)
        if folder:
            data['showcase'] = folder

    return data, changed

