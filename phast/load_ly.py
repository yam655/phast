#!/usr/bin/env python3

import re

from .config import rel_from_root
from .deps import dep_add, dep_changed

LY_QUOTES = re.compile(r'^([a-zA-Z0-9]+)\s*=\s*"([^"]+)"\s*$', re.MULTILINE)
LY_MARKUP = re.compile(r'^([a-zA-Z0-9]+)\s*=\s*\\markup\s*{\s*(.*)\s*}\s*$', re.MULTILINE)

def clean_ly_line(inl):
    out = []
    inl = inl.replace('~', ' ')
    # TODO does not properly join words
    last_broken = False
    for word in inl.split():
        if not word:
            continue
        if word in ('_', '__', r'\markup\italic'):
            continue
        if word == '--':
            if not last_broken:
                last_broken = True
            else:
                continue
        else:
            last_broken = False
        if len(word) > 1:
            if word.startswith('__'):
                word = word[2:]
            if word.endswith('__'):
                word = word[:-2]
            if word[0] == '_':
                word = word[1:]
            if word[-1] == '_':
                word = word[:-1]
            if word[0] == '"' == word[-1]:
                word = word[1:-1]
        out.append(word)
    return ' '.join(out).replace(' -- ','')

def ly2lyrics(contents):
    inblock = None
    blocks = []
    blockmap = {}
    for inl in contents.splitlines():
        inl = inl.strip()
        if not inl or inl[0] == '%':
            continue
        if 'set stanza' in inl:
            continue
        if inblock:
            if inl[0] == '}':
                inblock = False
            elif inblock in blockmap:
                clean = clean_ly_line(inl)
                blockmap[inblock].append(clean)
        elif 'lyricmode' in inl:
            words = inl.split()
            inblock = words[0]
            if not inblock.startswith('orig'):
                blocks.append(inblock)
                blockmap[inblock] = []
            continue
    out = []
    for b in blocks:
        if out:
            out.append('')
        out.extend(blockmap[b])
    return '\n'.join(out)


def lilyTags(ly_contents, tags):
    got = {}
    matches = LY_QUOTES.findall(ly_contents)
    for m in matches:
        got[m[0]] = m[1]
    matches = LY_MARKUP.findall(ly_contents)
    for m in matches:
        got[m[0]] = re.subn(r'(\\[a-zA-Z]+|[{}])', "", m[1])[0]

    for g in got:
        if g == "tuneArranger":
            if got["tuneArranger"].startswith("tune of"):
                tags["tuneOf"] = got["tuneArranger"][7:].strip()
            else:
                tags["tuneArranger"] = [got["tuneArranger"]]
        else:
            value = got[g]
            if "; " in value:
                value = value.split("; ")
            tags[g] = value
    return tags

lyFolderMapping = {
    'albumTitle': 'Album',
    'albumProduct': 'Album',
    # only-audio: AlbumArtist
    # only-audio: Artist
    'songDedication': 'Dedication',
    'inspiredBy': 'Dedication',
    'songTitle': 'Title',
    'songPoet': 'Lyricist',
    'songArranger': 'Arranger',
    'tuneArranger': 'Arranger',
    'tuneIs': 'TuneOf',
    'tuneOf': 'TuneOf',
    'tuneComposer': 'Composer',
    'tuneSource': 'TuneSource',
    'songCopyright': 'Year',
    'albumCopyright': 'Copyright',
    'albumLicense': 'Copyright',
    'albumTagline': '',
    'formatSource': '',
    'formatSubtitle': '',
    'formatSubsubtitle': '',
    'formatTagline': '',
    'formatCopyright': '',
    'inspiredByMarkup': '',
    'comment': 'Comment',
}

def lilypond_to_folder(ly):
    fldr = {}
    for k in ly.keys():
        v = ly[k]
        if not v:
            continue
        if isinstance(v, str):
            v = [v.strip()]
        if k not in lyFolderMapping:
            raise ValueError('{} did not have a Folder mapping'.format(k))
        k1 = lyFolderMapping[k]
        if k1:
            if k1 not in fldr:
                fldr[k1] = v
            else:
                fldr[k1].extend(v)
    return fldr

def load_ly(data, path):
    rel = rel_from_root(path)
    data['rel_path'] = rel

    meta = {}
    contents = path.read_text()
    lilyTags(contents, meta)
    data['lilypond'] = meta
    data['showcase'] = lilypond_to_folder(meta)

    title = meta.get('songTitle')
    if title is not None and not isinstance(title, str):
        title = title[0].strip()
    if not title:
        title = path.stem.replace('_',' ')
    data['title'] = title

    artist = None
    if 'songPoet' in meta:
        artist = meta.get('songPoet')
        if not isinstance(artist, str):
            artist = '; '.join()

    album = None
    if 'bookTitle' in meta:
        album = meta.get('bookTitle')
    elif 'albumTitle' in meta:
        album = meta.get('albumTitle')
    elif 'albumProduct' in meta:
        album = meta.get('albumProduct')
    if album and not isinstance(album, str):
        album = album[0]

    lyrics = ly2lyrics(contents).strip()
    if lyrics:
        meta['lyrics'] = lyrics
        meta['poem'] = lyrics
    else:
        meta['lyrics'] = ''
        meta['poem'] = '-'

    abstract = []
    abstract.append('|Sheetmusic| sheet music')
    if artist:
        abstract.append('by')
        abstract.append(artist)
    if album:
        abstract.append('from')
        abstract.append(album)

    data['abstract'] = ' '.join(abstract)
    if album is not None:
        data['product-info'] = {'title': album}

def load(source, data):
    if source.is_dir():
        return None
    if not source.suffix in ['.ly']:
        return None

    if data is None:
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']
    changed = False
    data['is_file'] = True

    if dep_changed(source, deps):
        changed = True
        dep_add(source, deps)
        load_ly(data, source)

    return data, changed


