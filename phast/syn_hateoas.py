#!/usr/bin/env python3

from pathlib import Path
import mimetypes
import re

from .config import get_config, get_cache, get_render_cache, read_config
from .brief import brief_bits
from .meta_utils import j_to_isodatetime

mimetypes.init()

class hateoas:
    def __init__(self, bucket, briefs, title):
        self.title = title
        self.bucket = bucket
        self.briefs = briefs
        self.url = get_config('url')
        if not self.url:
            self.url = ''
        elif not self.url.endswith('/'):
            self.url = self.url + '/'
        self.briefs = briefs
        self.mode_id = None
        self.local = {}
        self.bit_bag = {}
        cache = get_cache()
        self.dcache = {}
        for f in briefs:
            x = cache[f]
            if not x:
                self.local[f] = briefs[f]
            else:
                self.local[f] = x
            if 'bits' not in self.local[f]:
                bits = brief_bits(self.local[f])
            else:
                bits = self.local[f]['bits']
            if not bits:
                bits = []
            self.bit_bag[f] = bits

    def mode(self, mode = None):
        self.mode_id = mode

    def duration(self, n):
        if n in self.dcache:
            return self.dcache[n]
        b = self.local.get(n)
        if not b:
            return 0
        duration = b.get('conf',{}).get('audio',{}).get('duration')
        if not duration:
            return 0
        if '.' in duration:
            duration = duration.split('.',1)[0]
        if ':' in duration:
            ds = duration.split(':')
            secs = int(ds[-1])
            mins = int(ds[-2])
            hrs = 0
            if len(ds) > 2:
                hrs = int(df[-3])
            mins = mins + hrs * 60
            secs = secs + mins * 60
        else:
            secs = int(duration)
        return secs

    def __len__(self):
        num = 0

        if self.mode_id is None:
            options = ('spx', 'ogg', 'mp3', 'flac')
        elif isinstance(self.mode_id, str):
            options = [self.mode_id]
        else:
            options = self.mode_id
        for b in sorted(self.bit_bag, reverse=True):
            bits = self.bit_bag[b]
            if not bits:
                continue
            for ext in options:
                if ext in bits:
                    num += 1
                    break
        return num



    def __str__(self):
        out = []
        title = self.bucket['title']
        html_self = Path(self.bucket['rel_path'])
        xml_self = html_self.parent / (html_self.stem + '.xml')
        html_self = self.url + str(html_self)
        xml_self = self.url + str(xml_self)
        out.append('<?xml version="1.0" encoding="utf-8"?>')
        out.append('<feed xmlns="http://www.w3.org/2005/Atom">')
        out.append('<title>{}</title>'.format(self.title))
        out.append('<link href="{}"/>'.format(html_self))
        out.append('<link href="{}" rel="self" type="application/atom+xml"/>'.format(xml_self))

        author = get_config('author', self.url)
        out.append('<author><name>{}</name></author>'.format(author))

        copyright = None
        copyright_name = get_config('copyright')
        if copyright_name:
            copyright_name = root_path() / copyright_name
            if not copyright_name.exists():
                print('ERROR: Copyright file "{}" does not exist'.format(copyright_name))
                sys.exit(-1)
            copyright = copyright.read_text()

        if copyright:
            if r'</html>' in copyright:
                out.append('<rights type="html"')
            else:
                out.append('<rights')
            lang = get_config('lang')
            if lang:
                out.append(' xml:lang="{}">'.format(lang))
            else:
                out.append('>')
            out.append(copyright.replace('&', '&amp;').replace('<','&lt;').replace('>','&gt;'))
            out.append('</rights>')

        out.append('<id>{}</id>'.format(xml_self))

        abstract = get_config('abstract')
        if abstract:
            out.append('<subtitle>{}</subtitle>'.format(abstract))

        out.append('<generator uri="https://gitlab.com/yam655/phast">phast</generator>')

        # <updated>{}</updated>

        for b in sorted(self.briefs, reverse=True):
            brief = self.briefs[b]
            bits = self.bit_bag[b]

            rel = brief['rel_path']

            out.extend(self._entry(b, brief))

        out.append('</feed>')
        return '\n'.join(out)

    def _entry(self, b, brief):
        out = []
        bucket = self.local[b]
        out.append('<entry>')
        out.append('<title>{}</title>'.format(brief['title']))
        rel_path = brief['rel_path']
        self_url = Path(brief['rel_path'])
        self_url = self.url + str(self_url)
        out.append('<id>{}</id>'.format(self_url))

        out.append('<link rel="alternate" href="{}"/>'.format(self_url))
        summary = brief.get('abstract')
        if summary:
            config = read_config()
            replacements = config['replacements']
            sumry = re.split(r'(\|[A-Za-z0-9]+\|)', summary)
            sm = []
            for i in range(len(sumry)):
                s = sumry[i]
                if sumry[i] and sumry[i][0] == sumry[i][-1] == '|':
                    v = sumry[i][1:-1]
                    if v in replacements:
                        s = replacements[v]
                sm.append(s)
            if sm:
                out.append('<summary>{}</summary>'.format(''.join(sm)))


        author = get_config('author')
        a = bucket.get('audio',{}).get('ALBUMARTIST')
        if a == 'None':
            a = None
        authors = set()
        if isinstance(a, str):
            a = a.strip().splitlines()
        if a is not None:
            for a1 in a:
                if a1 is None:
                    raise ValueError(repr(a))
                authors.add(a1)
                out.append('<author>')
                out.append('<name>{}</name>'.format(a1))
                out.append('</author>'.format(author))
            author = None
        if author is not None:
            out.append('<author><name>{}</name></author>'.format(author))

        a = bucket.get('audio',{}).get('ARTIST', None)
        if isinstance(a, str):
            a = a.splitlines()
        if a is not None:
            for a1 in a:
                if a1 in authors:
                    continue
                authors.add(a1)
                out.append('<contributor>')
                out.append('<name>{}</name>'.format(a1))
                out.append('</contributor>'.format(author))

        copyright = bucket.get('license')
        if copyright:
            if r'</html>' in copyright:
                out.append('<rights type="html"')
            else:
                out.append('<rights')
            lang = get_config('lang')
            if lang:
                out.append(' xml:lang="{}">'.format(lang))
            else:
                out.append('>')
            out.append(copyright.replace('&', '&amp;').replace('<','&lt;').replace('>','&gt;'))
            out.append('</rights>')

        categories = bucket.get('category')
        if isinstance(categories, str):
            categories = categories.splitlines()
        if categories:
            for category in categories:
                out.append('<category term="{}" />'.format(category))
        publish_date = j_to_isodatetime(rel_path)
        out.append('<published>{}</published>'.format(publish_date))
        out.append('<updated>{}</updated>'.format(publish_date))

        out.append(self.enclosure(b, bucket))

        #<content type="html" xml:lang="en">
        out.append('<content type="html" xml:lang="en">')

        html = get_render_cache()[str(rel_path)]
        out.append(html.replace('&', '&amp;').replace('<','&lt;').replace('>','&gt;'))
        out.append('</content>')
        out.append('</entry>')
        return out

    def enclosure(self, b, bucket):
        out = []
        if self.mode_id is None:
            options = ('spx', 'ogg', 'mp3', 'flac')
        elif isinstance(self.mode_id, str):
            options = [self.mode_id]
        else:
            options = self.mode_id

        bits = self.bit_bag[b]
        fnames = None
        if not bits:
            return ''

        for ext in options:
            if ext in bits:
                fnames = bits[ext]
                break
        if not fnames:
            return ''
        if len(fnames) > 1:
            fnames.sort(key=lambda x: len(x))
        rel = bucket['rel_path']
        b1 = Path(b) / fnames[0]
        size = b1.stat().st_size
        if b1.suffix:
            mime = mimetypes.types_map[b1.suffix]
        else:
            raise ValueError('{} has no suffix.'.format(b1))
        url = '{}{}/{}'.format(self.url, rel, fnames[0])

        out = ['<link rel="enclosure" type="',
                mime,
                '" href="',
                url,
                '" length="',
                str(size),
                '" />']

        return ''.join(out)



