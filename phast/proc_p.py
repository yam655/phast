#!/usr/bin/env python3

import glob
from pathlib import Path

from .config import rel_from_root, unconf, get_config, find_config_path
from .deps import dep_add, dep_changed
from .out_rst import rst
from .out_gmi import gemini
from .out_gophermap import gophermap
from .show_product import showcase_product
from .out_m3u8 import m3u8
from .brief import get_brief
from .meta_utils import j_to_stamp
from .mock_proc import load_localcache, find_in_cache, process_gen_product


def process_p(bucket, out, source, localcache, *, have_m3u8=0):
    out.title(bucket['title'])
    thing = {}
    prod = bucket.get('product', {})
    for d in prod:
        if d in ('notes', 'announcement', '__TOUCHED__'):
            continue
#       elif d in ('image', 'image.txt'):
#           thing[d] = Path('{}/{}'.format(prod[d][1], prod[d][0]))
#           continue
        elif isinstance(prod[d], dict):
            continue
        else:
            thing[d] = prod[d][0]
    showcase_product(thing, None, out, localcache=localcache)
    process_gen_product(bucket, out, localcache, source, have_m3u8)
    return out

def process(source, bucket):
    localcache = {}

    localcache = load_localcache(bucket, source)
    rel = bucket.get('rel_path')
    if rel is None:
        raise ValueError('{} needs a rel_path'.format(repr(bucket)))
    if isinstance(rel, str):
        rel = Path(rel)
    rst_out = rst()
    m3u8_bits = m3u8(localcache)
    process_p(bucket, out=rst_out, source=source,
                        localcache=localcache, have_m3u8=len(m3u8_bits))
    html = rst_out.render(path="{}/folder.rst".format(rel))
    gmi = process_p(bucket, out=gemini(), source=source,
                        localcache=localcache, have_m3u8=len(m3u8_bits))
    gmap = process_p(bucket, out=gophermap(), source=source,
                        localcache=localcache, have_m3u8=len(m3u8_bits))
    m3u8_text = str(m3u8_bits)
    out = [ {
                'rel_path': rel / (rel.stem + '.m3u8'),
                'is_file': True,
                'create': m3u8_text
            },
            {
            'rel_path':rel, 
            'is_file': False,
            'html':html, 
            'source':str(rst_out), 
            'index.gmi':str(gmi),
            'gophermap':str(gmap)}]
    return out


