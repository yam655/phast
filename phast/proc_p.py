#!/usr/bin/env python3

import glob

from .config import rel_from_root, get_base_name, unconf
from .load_folder import dep_add, dep_changed
from .out_rst import rst
from .out_gophermap import gophermap

def determine_album_artist(product):
    album_artist = product.get('album_artist')
    if not album_artist:
        tracks_per_artist = {}
        for track in product.get('tracks',[]):
            artist = track.get('artists')
            if isinstance(artist, list):
                artist = "\n".join(artist)
            tracks_per_artist.setdefault(artist, 0)
            tracks_per_artist[artist] += 1
        most_tracks = 0
        for artist in tracks_per_artist:
            if tracks_per_artist[artist] > most_tracks:
                album_artist = artist
                most_tracks = tracks_per_artist[artist]
            elif tracks_per_artist[artist] == most_tracks:
                album_artist = None
    return album_artist


def product_image(meta, product_image, product_image_txt, product_image_alt, directory, addendum, *, embed_img=False, embed_txt=False, plain=False):

    if product_image in meta or product_image_txt in meta:
        addendum.append('')
        t = 'Album Images'
        if not plain:
            addendum.append('*' * len(t))
        addendum.append(t)
        addendum.append('*' * len(t))
        addendum.append('')
    embed = None
    path = directory / "something"
    if product_image in meta:
        fname = Path(meta.get(product_image))
        embed = fname
        embedpath = find_rel_path(path, fname)
        humankey = ":Cover Image (graphic):"
        addendum.append('{} `{} <{}>`__'.format(humankey, fname.name, embedpath))
    if product_image_txt in meta:
        fname = Path(meta.get(product_image_txt))
        humankey = ":Cover Image (text):"
        fnamepath = find_rel_path(path, fname)
        addendum.append('{} `{} <{}>`__'.format(humankey, fname.name, fnamepath))
    if embed:
        addendum.append('')
        embedpath = find_rel_path(path, embed)

        if embedpath is None:
            print("WARNING: could not find:", embed)
            if product_image_alt in meta:
                alt = meta[product_image_alt]
                addendum.append(textwrap.fill(
                    'Description of (missing) cover image: ' + alt,
                        initial_indent=' '*4, subsequent_indent=' '*4))
        else:
            addendum.append('.. image:: {}'.format(embedpath))
            if product_image_alt in meta:
                alt = meta[product_image_alt]
                addendum.append('   :alt: {}'.format(alt.expandtabs()))
                addendum.append('   :height: 24em')
    else:
        if product_image_alt in meta:
            print("WARNING: lost cover image for :", path)
            alt = meta[product_image_alt]
            addendum.append(textwrap.fill(
                'Description of (lost) cover image: ' + alt,
                initial_indent=' '*4, subsequent_indent=' '*4))



def process_shipped_project(product, urlout, *, directory=None, prefix=None):
    contents = process_shipped_project_source(product, directory=directory, prefix=prefix)
    source_path = "{}/product.txt".format(directory)
    html = render_html(contents=contents, path=source_path,
                                    urlout=urlout)
    gophermap = process_music_project_gophermap(product, directory=directory, prefix=prefix)
    obj = {'path':source_path, 'html':html, 'source':contents, 'gophermap':gophermap}
    return obj

def process_shipped_project_source(product, *, directory, prefix=None):
    if 'name' in product:
        title = product['name']
    elif 'title' in product:
        title = product['title']
    elif 'artifact_name' in meta:
        title = meta['artifact_name']
    if title:
        product['title'] = title
    body = []
    if prefix:
        body.append(prefix)
        body.append('')

    shipped = product['shipped']

    album_artist = shipped.get('released_as')
    d = directory
    if d.is_absolute():
        d1 = d
        r = ['..']
        while d1.name is not 'p' and d1.parent != d1:
            d1 = d1.parent
            r.append('..')
        if d1.parent == d1:
            reltoj = Path(".")
        else:
            reltoj = Path("/".join(r))

    elif d.parents[len(d.parents)-2] == 'p':
        reltoj = Path("../" * (len(d.parents) - 1))
    else:
        raise ValueError('Unknown path: {}'.format(directory))

    t = title
    body.append('#' * len(t))
    body.append(t)
    body.append('#' * len(t))
    body.append('')

    list_or_not(':Music Genre: ', shipped, 'genre/music', body)
    list_or_not(':Released By: ', shipped, 'music/released_by', body)
    list_or_not(':Released As: ', shipped, 'music/released_as', body)
    list_or_not(':Released On: ', shipped, 'music/released_on', body)
    add_links('download_urls', ':Download Album:', shipped, body)
    add_links('purchase_urls', ':Purchase Album:', shipped, body,
                price=shipped.get('purchase_price'))

    if 'announced' in shipped:
        urls = []
        for announce in shipped.get('announced', []):
            urls.append('`{} <{}>`__'.format(announce, reltoj / 'j' / announce))
        fake = {'u':urls}
        list_or_not(':Announcements:', fake, 'u', body)
    list_or_not(':Duration: ', shipped, 'duration', body)

    body.append('')
    product_image(product, 'image', 'image.txt', 'image.alt', directory, body, embed_img=True)

    if 'tracks' in shipped:
        body.append('')
        t = 'Track Listing'
        body.append('*' * len(t))
        body.append(t)
        body.append('*' * len(t))
        body.append('')
        for track in shipped.get('tracks', []):
            if not track:
                continue
            body.append(track)

    body.append('')
    status_transcription = None
    if 'music/transcription' in shipped:
        body.append('')
        status_transcription = shipped['music/transcription']
        body.append(textwrap.fill(
            'Transcription status: ' + status_transcription))

    body.append('')
    contents = '\n'.join(body)
    return contents


def process_music_project_source(product, *, directory, prefix=None):
    if 'name' in product:
        title = product['name']
    elif 'title' in product:
        title = product['title']
    elif '_title' in meta:
        title = meta['_title']
    elif 'artifact_name' in meta:
        title = meta['artifact_name']
    if title:
        product['title'] = title
    body = []
    if prefix:
        body.append(prefix)
        body.append('')

    album_artist = determine_album_artist(product)
    albumpath = product.get('path')
    dirs = []
    d = directory
    while d.name != 'p':
        dirs.append(d.name)
        d = d.parent
    if d.name == 'p':
        dirs.append(d.name)
        dirs.sort(reverse=True)
        if not albumpath:
            albumpath = "/".join(dirs)
        reltoj = Path("../" * len(dirs))
    else:
        reltoj = Path(".")
        if not albumpath:
            albumpath = '.'

    if 'genre/music' in product:
        body.append(':Music Genre: {}'.format(product['genre/music']))
    if album_artist:
        body.append(':Album Artist: {}'.format(album_artist))
    add_links('download_urls', ':Download Album:', product, body)
    add_links('purchase_urls', ':Purchase Album:', product, body)
    body.append('')

    last_ordinal = 0
    for track in product.get('tracks', []):
        trackpath = track.get('path')
        if not trackpath:
            continue
        reltrack = reltoj / 'j' / trackpath

        ordinal = track.get('ordinal', last_ordinal + 1)
        last_ordinal = ordinal
        byline = ''
        artists = track.get('artists')
        if isinstance(artists, list) and len(artists) == 1:
            artists = artists[0]
        if artists and not isinstance(artists, str) and artists != album_artist:
            if len(artists) <= 2:
                byline = ' ({})'.format(' with '.format(artists))
            else:
                byline = ' ({} with {})'.format(artists[0], ' and '.format(artists[1:]))
        tracktitle = track.get('title', 'Track {}'.format(ordinal))
        if reltrack:
            tracktitle = '`{} <{}>`_'.format(tracktitle, reltrack)
        ordstr = "{}. ".format(ordinal)
        body.append('{}{}{}'.format(ordstr, tracktitle, byline))

        ordspace = ' ' * len(ordstr)
        #if track.get('formats',[]):
            #body.append('')

        for fmt in track.get('formats',[]):
            formatFiles = list(reltrack.glob('*.' + fmt))
            formatFiles.sort()
            for dirpath in formatFiles:
                fname = dirpath.name
                key = fmt
                if len(formatFiles) > 1:
                    key = fname
                body.append('{}(`{} <{}>`__)'.format(ordspace, key, dirpath))
        body.append('')
    body.append('')
    body.append('..')
    body.append('')
    contents = '\n'.join(body)
    return contents

def process_music_project(product, *, directory=None, prefix=None):
    out.title(product['title'])
    out.add(product.get('prefix'))
    out.nl()
    
    album_artist = determine_album_artist(product)

    out.list_or_not('Music Genre', meta, 'genre/music')
    out.list_or_not('Album Artist', album_artist)
    out.add_links('Download Album', meta, 'product/download_urls')
    out.add_links('Purchase Album', meta, 'product/purchase_urls',
                    price = meta.get('product/purchase_price'))
    out.nl()

    last_ordinal = 0
    for track in product.get('tracks', []):
        trackpath = track.get('path')
        if not trackpath:
            continue
        reltrack = reltoj / 'j' / trackpath

        ordinal = track.get('ordinal', last_ordinal + 1)
        last_ordinal = ordinal
        byline = ''
        artists = track.get('artists')
        if isinstance(artists, list) and len(artists) == 1:
            artists = artists[0]
        if artists and not isinstance(artists, str) and artists != album_artist:
            if len(artists) <= 2:
                byline = ' ({})'.format(' with '.format(artists))
            else:
                byline = ' ({} with {})'.format(artists[0], ' and '.format(artists[1:]))
        tracktitle = track.get('title', 'Track {}'.format(ordinal))
        ordstr = "{}. ".format(ordinal)
        body.append('1{}{}{}\t{}'.format(ordstr, tracktitle, byline, reltrack))
        ordspace = ' ' * len(ordstr)

        for fmt in track.get('formats',[]):
            gopherType = suffixToGopherType(fmt)
            formatFiles = list(reltrack.glob('*.' + fmt))
            formatFiles.sort()
            for dirpath in formatFiles:
                fname = dirpath.name
                key = fmt
                if len(formatFiles) > 1:
                    key = fname
                body.append('{}{}{}\t{}'.format(gopherType,ordspace, key, dirpath))
        body.append('')
    body.append('')
    gophermap_footer(body)
    contents = '\n'.join(body)
    return contents



def process_music_project(product, urlout, *, directory=None, prefix=None):
    contents = process_music_project_source(product, directory=directory, prefix=prefix)
    source_path = "{}/product.txt".format(directory)
    html = render_html(contents=contents, path=source_path,
                                    urlout=urlout)
    gophermap = process_music_project_gophermap(product, directory=directory, prefix=prefix)
    obj = {'path':source_path, 'html':html, 'source':contents, 'gophermap':gophermap}
    return obj


def process(data, json_path, deps):
    if 'music' in prodbox:
        return process_music_project(prodbox['music'], urlout,
                    directory=source.parent, prefix=prefix)
    elif 'shipped' in prodbox:
        return process_shipped_project(prodbox, urlout,
                    directory=source.parent, prefix=prefix)
    raise ValueError(repr(prodbox))

