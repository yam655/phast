#!/usr/bin/env python3

import os
import sys
from pathlib import Path
from configparser import ConfigParser, ExtendedInterpolation
import diskcache

_conf_file = None
_conf_path = None
_cache = None


def find_config_path():
    global _conf_path
    if _conf_path:
        return _conf_path
    _conf_path = Path(os.environ['PHAST_DOCROOT']) / 'site.conf'
    while not _conf_path.exists():
        if _conf_path.parent == conf_path.parent.parent:
            _conf_path = None
            break
        _conf_path = _conf_path.parent.parent / _conf_path.name
    if not _conf_path:
        print('ERROR: Unable to find "site.conf" in folder ancestry.')
        sys.exit(1)
    return _conf_path

def rel_from_root(path):
    return path.relative_to(find_config_path().parent)

def up_to_root(folder):
    relpath = rel_from_root(folder)
    return Path('../' * len(relpath.parents))

def get_cache():
    global _cache
    if _cache is not None:
        return _cache
    root = find_config_path().parent
    cachedir = root / '_cache'
    _cache = diskcache.Index(str(cachedir))
    return _cache

def unconf(conf):
    ret = {}
    for label in conf:
        box = {}
        section = conf[label]
        for label1 in section:
            box[label1] = section[label1]
        if box:
            ret[label] = box
    return ret

def read_config():
    global _conf_file
    if _conf_file:
        return _conf_file
    conf_path = find_config_path()
    if not _conf_file:
        _conf_file = ConfigParser(interpolation=ExtendedInterpolation())
        _conf_file.optionxform = lambda option: option
        _conf_file.read(conf_path)
    return _conf_file

def get_config(section, what=None, *, as_path=False, default=None):
    config = read_config()
    if what is None:
        what = section
        section = 'phast'
    if not section in config:
        return default
    if what not in config[section]:
        return default
    got = config[section][what].strip()
    if not got:
        return default
    if as_path:
        got = Path(got).expanduser()
    return got

def simple_config():
    global _conf_path
    conf_path = find_config_path()
    conf = {}
    conf['source'] = conf_path.parent
    conf['dest_html'] = get_config('phast', 'html', as_path=True)
    conf['dest_gopher'] = get_config('phast', 'gopher', as_path=True)
    conf['url'] = get_config('phast', 'url')
    return conf

def verbose(*args, **kwargs):
    print(*args, **kwargs)

base_names = {
    'home': 'Home',
    'products': 'Products',
    'services': 'Services',
    'users': 'Users',
    'journal': 'Journal',
}

base_names_conf = {
    '': 'home',
    'p': 'products',
    's': 'services',
    'u': 'users',
    'j': 'journal',
}

def get_base_name(relname):
    what = base_names_conf.get(str(relname))
    answer = None
    if what:
        answer = get_config('labels:plain', str(what))
        if not answer:
            answer = base_names.get(what)
    if not answer:
        answer = Path(relname).name
    if not isinstance(answer, str):
        raise ValueError(repr(answer))
    return answer

def save_project(path, bucket):
    if not isinstance(path, str):
        path = str(path)
    cache = get_cache()
    all_projs = cache.get('project:__all__', set())
    if bname not in all_projects:
        all_projs.add(bname)
        cache['project:__all__'] = all_projs
    bname = 'project:' + path
    cache[bname] = bucket
    return brief

def get_all_projects():
    cache = get_cache()
    all_projs = cache.get('project:__all__', set())
    return all_projs
    
def get_project(path):
    if not isinstance(path, str):
        path = str(path)
    cache = get_cache()
    bname = 'project:' + path
    return cache.get(bname, {})

