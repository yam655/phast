#!/usr/bin/env python3

import os
import sys
from pathlib import Path
from configparser import ConfigParser
import diskcache
import re

_conf_file = None
_conf_path = None
_cache = None
_index = None
_briefs = None
_render_cache = None

def root_path():
    return find_config_path().parent

def find_config_path():
    global _conf_path
    if _conf_path:
        return _conf_path
    _conf_path = Path(os.environ['PHAST_DOCROOT']) / 'site.conf'
    if not _conf_path.exists():
        print('ERROR: Unable to find "site.conf".')
        sys.exit(1)
    return _conf_path

def rel_from_root(path):
    path = Path(path)
    if path.is_absolute():
        return path.relative_to(find_config_path().parent)
    elif isinstance(path, str):
        if len(path.split('/',1)) > 1:
            raise ValueError(path)
    elif len(str(path.parents[len(path.parents)-2])) > 1:
        raise ValueError(path)
    return path

def get_cache():
    global _cache
    if _cache is not None:
        return _cache
    if not _conf_file:
        raise NameError('read_config should be called before get_cache')
    active = _conf_file['__active']
    root = find_config_path().parent
    cachedir = root / '_cache' / active / 'general'
    _cache = diskcache.Index(str(cachedir))
    return _cache

def get_briefs():
    global _briefs
    if _briefs is not None:
        return _briefs
    if not _conf_file:
        raise NameError('read_config should be called before get_briefs')
    active = _conf_file['__active']
    root = find_config_path().parent
    indexdir = root / '_cache' / active / 'briefs'
    _briefs = diskcache.Index(str(indexdir))
    return _briefs

def get_index():
    global _index
    if _index is not None:
        return _index
    if not _conf_file:
        raise NameError('read_config should be called before get_index')
    active = _conf_file['__active']
    root = find_config_path().parent
    indexdir = root / '_cache' / active / 'index'
    _index = diskcache.Index(str(indexdir))
    return _index

def get_render_cache():
    global _render_cache
    if _render_cache is not None:
        return _render_cache
    if not _conf_file:
        raise NameError('read_config should be called before get_render_cache')
    active = _conf_file['__active']
    root = find_config_path().parent
    cachedir = root / '_cache' / active / 'render'
    _render_cache = diskcache.Index(str(cachedir))
    return _render_cache

def unconf(conf):
    ret = {}
    for label in conf:
        box = {}
        section = conf[label]
        for label1 in section:
            box[label1] = section[label1]
        if box:
            ret[label] = box
    return ret

def _read_config():
    global _conf_file
    conf_path = find_config_path()
    if not _conf_file:
        conf_file = ConfigParser()
        conf_file.optionxform = lambda option: option
        conf_file.read(conf_path)
        _conf_file = unconf(conf_file)
    return _conf_file

def read_config(mode=None):
    global _conf_file
    if not _conf_file:
        _read_config()
    if mode is None:
        if '__active' not in _conf_file:
            mode = 'local'
        else:
            return _conf_file
    if mode != _conf_file.get('__active', mode):
        raise ValueError('Can not change mode.')
    if mode not in _conf_file:
        return _conf_file
    for f in _conf_file[mode]:
        _conf_file['phast'][f] = _conf_file[mode][f]
    _conf_file['__active'] = mode
    return _conf_file

def get_config(section, what=None, *, as_path=False, default=None):
    config = read_config()
    if what is None:
        what = section
        section = 'phast'
    if isinstance(what, list):
        raise ValueError(repr(what))
    if not section in config:
        return default
    if what not in config[section]:
        return default
    got = config[section][what].strip()
    if not got:
        return default
    if as_path:
        got = Path(got).expanduser()
    return got

def simple_config():
    global _conf_path
    conf_path = find_config_path()
    conf = {}
    conf['source'] = conf_path.parent
    conf['dest_html'] = get_config('phast', 'html', as_path=True)
    conf['dest_gopher'] = get_config('phast', 'gopher', as_path=True)
    conf['dest_gemini'] = get_config('phast', 'gemini', as_path=True)
    conf['url'] = get_config('phast', 'url')
    return conf

def verbose(*args, **kwargs):
    print(*args, **kwargs)

base_names = {
    'home': 'Home',
    'products': 'Products',
    'services': 'Services',
    'users': 'Users',
    'journal': 'Journal',
    'reviews': 'Reviews',
    'keywords': 'Keywords',
    'categories': 'Categories',
}

base_names_conf = {
    '': 'home',
    '.': 'home',
    'p': 'products',
    's': 'services',
    'u': 'users',
    'j': 'journal',
    'r': 'reviews',
    'k': 'keywords',
    'c': 'categories',
}

def get_base_name(relname, how, *, default=None):
    what = base_names_conf.get(str(relname))
    if how is None:
        how = 'labels:plain'
    answer = None
    if what:
        answer = get_config(how, what)
        if not answer:
            answer = base_names.get(what)
    if not answer:
        answer = default
    return answer

def get_category_list():
    config = read_config()
    if 'categories' not in config:
        return {}
    cats = config['categories']
    return cats

def get_category_name_list(categories, *, drop_hidden=False):
    answers = []
    if isinstance(categories, str):
        categories = [categories, ]
    for cat in categories:
        catname = get_category_name(cat)
        if catname and catname[0] == '!':
            if drop_hidden:
                continue
            catname = catname[1:]
        answers.append(catname)
    return '; '.join(answers)

def get_category_names(categories, *, drop_hidden=False):
    answers = []
    if isinstance(categories, str):
        categories = [categories,]
    for category in categories:
        value = get_category_name(category)
        if value:
            if value[0] == '!':
                if drop_hidden:
                    continue
                value = value[1:]
            answers.append((category, value))
    return answers

def get_category_name(category, *, default=None):
    if not isinstance(category, str):
        raise ValueError(f'this only accepts strings: {category!r}')
    answer = get_config('categories', category)
    if not answer:
        answer = category.title()
    return answer

def _save_subset(path, bucket, prefix):
    if not isinstance(path, str):
        path = str(path)
    if not path:
        raise ValueError('No name for this subset.')
    cache = get_cache()
    bname = prefix + path
    aname = prefix + '__ALL__'
    all_of_it = cache.get(aname)
    if not all_of_it:
        cache[aname] = {
                'title': prefix[0],
                'is_dir': True,
                'rel_path': Path(prefix[0]),
                'mock_dir': set()
        }
        all_of_it = cache[aname]
    mock_dir = all_of_it['mock_dir']
    if bname not in mock_dir:
        all_of_it['mock_dir'].add(bname)
        cache[aname] = all_of_it
    cache[bname] = bucket
    return bucket

def save_product(path, bucket):
    path = path.lower()
    return _save_subset(path, bucket, 'product:')

def get_all_products():
    cache = get_cache()
    all_products = cache.get('product:__ALL__', {})
    return all_products

def get_product(path):
    if not isinstance(path, str):
        path = str(path)
    path = path.lower()
    cache = get_cache()
    bname = 'product:' + path
    return cache.get(bname, {})

def save_service(path, bucket):
    path = path.lower()
    return _save_subset(path, bucket, 'service:')

def get_all_services():
    cache = get_cache()
    all_services = cache.get('service:__ALL__', {})
    return all_services

def get_service(path):
    if not isinstance(path, str):
        path = str(path)
    path = path.lower()
    cache = get_cache()
    bname = 'service:' + path
    return cache.get(bname, {})

def save_review(path, source, bucket):
    path = path.lower()
    return _save_subset(path, bucket, 'review:')

def get_all_reviews():
    cache = get_cache()
    all_reviews = cache.get('review:__ALL__', {})
    return all_reviews

def get_review(path):
    if not isinstance(path, str):
        path = str(path)
    path = path.lower()
    cache = get_cache()
    bname = 'review:' + path
    return cache.get(bname, {})

def add_tag(keyw, filep):
    res = get_tag(keyw)
    if not isinstance(filep, str):
        filep = str(filep)
    if filep not in res:
        res[filep] = {}
        save_tag(keyw, res)
    return

def save_tag(path, bucket):
    if not isinstance(path, str):
        path = str(path)
    path = path.lower()
    cache = get_index()
    bname = path
    cache[bname] = bucket
    return

def get_all_tags():
    return get_index

def get_tag(path):
    if not isinstance(path, str):
        path = str(path)
    path = path.lower()
    cache = get_index()
    bname = path
    return cache.get(bname, {})

def save_keyword(path, bucket):
    if not bucket:
        bucket['title'] = path
    path = path.lower()
    return _save_subset(path, bucket, 'keyword:')

def add_keyword(keyw, filep):
    if not isinstance(filep, str):
        filep = str(filep)
    if not isinstance(keyw, str):
        for kw in keyw:
            add_keyword(kw, filep)
    else:
        res = get_keyword(keyw)
        if filep not in res:
            if 'title' not in res:
                bucket['title'] = keyw
            res[filep] = True
            save_keyword(keyw, res)
    return

def get_all_keywords():
    cache = get_cache()
    all_keywords = cache.get('keyword:__ALL__', {})
    return all_keywords

def get_keyword(path):
    if not isinstance(path, str):
        path = str(path)
    path = path.lower()
    cache = get_cache()
    bname = 'keyword:' + path
    return cache.get(bname, {})

def save_category(path, bucket):
    if not bucket:
        bucket['title'] = path
    path = path.lower()
    return _save_subset(path, bucket, 'category:')

def add_category(keyw, filep):
    if not isinstance(filep, str):
        filep = str(filep)
    if not isinstance(keyw, str):
        for kw in keyw:
            add_category(kw, filep)
    else:
        res = get_category(keyw)
        if filep not in res:
            if 'title' not in res:
                bucket['title'] = keyw
            res[filep] = True
            save_category(keyw, res)
    return

def get_all_categories():
    cache = get_cache()
    all_categories = cache.get('category:__ALL__', {})
    return all_categories

def get_category(path):
    if not isinstance(path, str):
        path = str(path)
    path = path.lower()
    cache = get_cache()
    bname = 'category:' + path
    return cache.get(bname, {})



def convert_replacements(txt):
    if not isinstance(txt, str):
        raise ValueError(repr(txt))
    config = read_config()
    replacements = config['replacements']
    texts = re.split(r'(\|[A-Za-z0-9]+\|)', txt)
    sm = []
    for i in range(len(texts)):
        s = texts[i]
        if texts[i] and texts[i][0] == texts[i][-1] == '|':
            v = texts[i][1:-1]
            if v in replacements:
                s = replacements[v]
        sm.append(s)
    return ''.join(sm)

def remove_replacements(txt):
    if not isinstance(txt, str):
        raise ValueError(repr(txt))
    config = read_config()
    replacements = config['replacements']
    texts = re.split(r'(\|[A-Za-z0-9]+\|)', txt)
    sm = []
    for i in range(len(texts)):
        s = texts[i]
        if texts[i] and texts[i][0] == texts[i][-1] == '|':
            v = texts[i][1:-1]
            if v in replacements:
                s = ""
        sm.append(s)
    return ''.join(sm)

