#!/usr/bin/env python3

from pathlib import Path

from .config import get_cache, get_base_name, get_category_names, get_config
from .brief import get_brief
from .out_rst import rst
from .out_gophermap import gophermap
from .out_gmi import gemini
from .show_product import showcase_product
from .show_service import showcase_service, showcase_option
from .show_lyrics import showcase_lyrics
from .show_transcript import showcase_transcript
from .show_audio import showcase_audio
from .show_video import showcase_video
from .show_image import showcase_image
from .meta_utils import ignore_path, clean_gen_path

SEE_SERVICE_PAGE = '(See service page for current price.)'

def option_summary(conf, out, localcache):
    options = []
    for c in conf:
        con = conf[c]
        if c.startswith('option/'):
            options.append((c, con))

    out.nl()
    last_group = None
    for opt in sorted(options, key=lambda x: '{}\t{}'.format(x[1].get('group','-'), x[0])):
        b = opt[1]
        name = opt[0]
        if last_group != b.get('group',None):
            last_group = b.get('group')
            if last_group is not None:
                if '|' in last_group:
                    out.subsection(last_group.split('|')[-1].strip().title())
                else:
                    out.subsection(last_group.title())
                out.nl()
        showcase_option(b, name, out, hide_price=SEE_SERVICE_PAGE)
        out.nl()

def process_folder(bucket, *, out, source, localcache, title_override=None):
    product = bucket.get('folder',{})
    title = bucket['title']
    prefix = bucket.get('prefix')
    prefix_is_rst = bucket.get('prefix/rst')
    rel = Path(bucket['rel_path'])
    used = set()

    if title_override:
        title = title_override
    if not prefix_is_rst:
        out.title(title)
    if prefix is not None:
        out.add(prefix)
        if prefix_is_rst:
            out.nl()
            out.add('----')
            out.desectioned(True)
    out.nl()

    showcase_video(bucket, source, out, localcache)
    showcase_image(bucket, source, out, localcache)
    showcase_audio(bucket, source, out, localcache)
    if 'transcript' in bucket:
        showcase_transcript(bucket, source, out, localcache)
    else:
        showcase_lyrics(bucket, source, out, localcache)

    for pre, context in [
                ('product', 'Found on: {}'),
                ('service', 'Please see service: {}'),
                ('review', 'Related reviews: {}'),
            ]:
        out.nl()
        prod = bucket.get('conf',{}).get(pre,{})
        if 'title' in prod:
            prodname = prod['title']
            lrel = clean_gen_path('{}:{}'.format(pre,prodname))
            if lrel is not None:
                out.rel_link(lrel, prodname, None, only_title=True, # rel/root
                         add_suffix=False, is_file=False, context=context)

    showcase_service(bucket.get('conf',{}).get('service',{}), out, localcache,
                    hide_price=SEE_SERVICE_PAGE)
    option_summary(bucket.get('conf',{}), out, localcache)
    showcase_product(bucket.get('conf',{}).get('product',{}), source, out,
                    localcache=localcache)

    prod = bucket.get('conf',{}).get('service',{})
    for marker in ('image', 'image.txt', 'image.thumb', 'image.gif', 'bash',
                   'sh', 'linux', 'macos', 'python', 'windows'):
        fns = prod.get(marker)
        if not fns:
            continue
        fn = source / fns
        fns1 = str(fn)
        if fns1 in localcache:
            continue
        if fn not in localcache:
            continue
        localcache[fn]['hide'] = True

    prod = bucket.get('conf',{}).get('product',{})
    for marker in ('image', 'image.txt', 'image.thumb', 'image.gif', 'bash',
                   'sh', 'linux', 'macos', 'python', 'windows'):
        fns = prod.get(marker)
        if not fns:
            continue
        fn = source / fns
        fns1 = str(fn)
        if fns1 in localcache:
            continue
        if fn not in localcache:
            continue
        localcache[fn]['hide'] = True

    for u in bucket.get('conf', {}):
        d = bucket['conf'][u]
        if 'in-showcases' in d:
            f = source / u
            used.add(f)

    out.nl()
    
    box = []
    for sop in localcache:
        if sop in used or sop.stem == 'folder':
            continue
        box.append(sop)

    first = True
    for sop in sorted(box, reverse=True):
#       if sop.name == 'folder.conf':
#           continue
        bucket2 = localcache.get(sop)
        if not bucket2:
            continue
        ltitle = bucket2['title']
        labstract = bucket2.get('abstract')
        lrel = bucket2['rel_path']
        add_suffix = bucket2.get('add_suffix', False)
        is_file = bucket2.get('is_file')
        hide = bucket2.get('hide')

        if not hide:
            if first:
                if len(out.body) > 5:
                    out.section('Other files')
                first = False
            out.rel_link(lrel, ltitle, labstract, # rel/root
                         add_suffix=add_suffix, is_file=is_file, context='- {}')
            out.nl()

    kws = bucket.get('keywords',[])
    if len(kws) == 1:
        out.nl()
        kw = kws[0]
        lrel = clean_gen_path(f'keyword:{kw}')
        context = 'Keyword: {}'
        out.rel_link(lrel, kw, None, only_title=True, # rel/root
                        add_suffix=False, is_file=False, context=context)
        out.nl()
    elif len(kws) > 1:
        out.nl()
        out.add('Keywords:')
        context = '   - {}'
        for kw in kws:
            lrel = clean_gen_path(f'keyword:{kw}')
            out.rel_link(lrel, kw, None, only_title=True, # rel/root
                        add_suffix=False, is_file=False, context=context)
        out.force_end()

    kws = bucket.get('categories',[])
#   if kws is None:
#       kws = ['uncategorized']
    if kws is not None:
        cattitles = get_category_names(kws, drop_hidden=False)
        if len(cattitles) == 1:
            out.nl()
            kw = cattitles[0]
            title = kw[1]
            lrel = clean_gen_path(f'category:{kw[0]}')
            context = 'Category: {}'
            out.rel_link(lrel, title, None, only_title=True, # rel/root
                            add_suffix=False, is_file=False, context=context)
            out.nl()
        elif len(kws) > 1:
            out.nl()
            out.add('Categories:')
            context = '   - {}'
            for cat, title in cattitles:
                lrel = clean_gen_path(f'category:{cat}')
                out.rel_link(lrel, title, None, only_title=True, # rel/root
                            add_suffix=False, is_file=False, context=context)
            out.force_end()

    out.nl()
    out.force_end()
    out.nl()
    return out

def fill_local_cache(source, conf):
    cache = get_cache()
    localcache = {}
    for f in source.glob('*'):
        if ignore_path(f):
            continue
        sf = str(f)
        if sf in cache:
            brief = get_brief(sf)
            if brief:
                localcache[f] = brief
    return localcache

def process(source, bucket):
    localcache = fill_local_cache(source, bucket.get('conf',{}))

    in_underground = False
    underground = get_config('underground')
    if underground:
        underground = underground.splitlines()
    else:
        underground = []
    rel = bucket.get('rel_path')
    for underline in underground:
        underparts = underline.split('|')
        upper = f'{underparts[0].strip()}/'
        relstr = str(rel)
        if relstr == underparts[0].strip() or relstr.startswith(upper):
            in_underground = True
            break

    rst_out = rst()
    title_override = get_base_name(rel, 'labels:unicode')
    contents = process_folder(bucket, out=rst_out, source=source,
                        localcache=localcache, title_override=title_override)

    if in_underground:
        html = ""
    else:
        html = rst_out.render(path="{}/folder.rst".format(source))
    gmi = process_folder(bucket, out=gemini(), source=source,
                        localcache=localcache, title_override=title_override)
    title_override = get_base_name(rel, 'labels:plain')
    gmap = process_folder(bucket, out=gophermap(), source=source,
                        localcache=localcache, title_override=title_override)
    out = [{
            'rel_path':rel, 
            'is_file': False,
            'html':html, 
            'source':str(contents), 
            'index.gmi':str(gmi),
            'gophermap':str(gmap)}]
    return out

