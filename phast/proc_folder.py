#!/usr/bin/env python3

from pathlib import Path

from .config import get_cache
from .brief import get_brief
from .out_rst import rst
from .out_gophermap import gophermap
from .show_product import showcase_product
from .show_lyrics import showcase_lyrics
from .show_audio import showcase_audio
from .meta_utils import ignore_path


def process_folder(bucket, *, out, source, localcache):
    product = bucket.get('folder',{})
    title = bucket['title']
    prefix = product.get('prefix')
    rel = Path(bucket['rel_path'])
    used = set()

    out.title(title)
    out.add(prefix)
    out.nl()

    showcase_product(bucket, out, localcache)
    showcase_audio(bucket, out, localcache)
    showcase_lyrics(bucket, out, localcache)

    for u in bucket.get('conf', {}):
        d = bucket['conf'][u]
        if 'in-showcases' in d:
            f = source / u
            used.add(f)

    out.nl()
    
    box = []
    for sop in localcache:
        if sop in used:
            continue
        box.append(sop)
    if box:
        out.section('Other files')

    for sop in box:
        if sop.name == 'folder.conf':
            continue
        bucket = localcache.get(sop)
        if not bucket:
            continue
        ltitle = bucket['title']
        labstract = bucket['abstract']
        lrel = bucket['rel_path']
        is_file = bucket.get('is_file')
        hide = bucket.get('hide')

        if not hide:
            out.rel_link(lrel.relative_to(rel), ltitle, labstract, 
                         is_file=is_file, context='- {}')

            if labstract:
                out.under(labstract)
                out.nl()

    out.nl()
    out.add('..')
    out.nl()
    return out

def fill_local_cache(source):
    cache = get_cache()
    localcache = {}
    for f in source.glob('*'):
        if ignore_path(f):
            continue
        sf = str(f)
        if sf in cache:
            brief = get_brief(sf)
            if brief:
                localcache[f] = brief
    return localcache

def process(source, bucket):
    localcache = fill_local_cache(source)
#    if bucket['loader'] == 'directory' and source.name == '2019-03':
#        raise ValueError(repr(list(localcache.items())))

    rst_out = rst()
    contents = process_folder(bucket, out=rst_out, source=source, localcache=localcache)
    html = rst_out.render(path="{}/folder.rst".format(source))
    gmap = process_folder(bucket, out=gophermap(), source=source, localcache=localcache)
    out = [{
            'rel_path':bucket.get('rel_path'), 
            'is_file': False,
            'html':html, 
            'source':str(contents), 
            'gophermap':str(gmap)}]
    return out

