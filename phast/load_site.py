#!/usr/bin/env python3

from configparser import ConfigParser

from .config import rel_from_root, unconf
from .deps import dep_add, dep_changed


def is_valid_site(source):
    if not source.is_dir():
        return False
    my_site_conf = source / 'site.conf'
    if not my_site_conf.exists():
        return False
    return True


def site_conf(source):
    if not source.is_file():
        return None
    site_conf = ConfigParser()
    site_conf.optionxform = lambda option: option
    site_conf.read(source)
    return unconf(site_conf)


def load_site_conf(data, my_site_conf, deps):
    changed = 0
    source = my_site_conf.parent
    if dep_changed(my_site_conf, deps):
        changed += 1
        dep_add(my_site_conf, deps)
        rel = rel_from_root(source)
        data['rel_path'] = rel
        conf = site_conf(my_site_conf)
        conf['conf'] = conf

        meta = conf.get('phast',{})
        data['site'] = meta

        # data['abstract'] = meta.get('abstract')
        data['title'] = meta.get('abstract', 'Home')

    return changed > 0


def load(source, data):
    if not is_valid_site(source):
        return None
    if data is None:
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']

    changed = 0
    if load_site_conf(data, source / 'site.conf', deps):
        changed += 1

    folder_txt = source / 'folder.txt'
    if dep_changed(folder_txt, deps):
        dep_add(folder_txt, deps)
        changed += 1
        prefix = None
        if folder_txt.exists():
            prefix = folder_txt.read_text()
        data['prefix'] = prefix

    return data, changed > 0


