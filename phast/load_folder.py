#!/usr/bin/env python3

import glob
from pathlib import Path
from configparser import ConfigParser, ExtendedInterpolation

from .config import rel_from_root, get_base_name, unconf
from .meta_utils import ignore_path


def reduce_configs(config, meta):
    dconfig = config.get('DEFAULT', {})
    fconfig = {}
    if meta is None:
        meta = {}

    for x in dconfig:
        if x[0] == '_':
            meta[x[1:]] = dconfig[x]
    for x in fconfig:
        meta[x] = fconfig[x]

    if 'folder' in config:
        fconfig = config['folder']
    if 'artifact_name' in fconfig:
        meta['title'] = fconfig['artifact_name']
    elif 'title' in fconfig:
        meta['title'] = fconfig['title']
    elif '_title' in dconfig:
        meta['title'] = dconfig['_title']
    if 'artifact_description' in fconfig:
        meta['abstract'] = fconfig['artifact_description']
    elif 'artifact_abstract' in fconfig:
        meta['abstract'] = fconfig['artifact_abstract']
    elif 'abstract' in fconfig:
        meta['abstract'] = fconfig['abstract']
    elif '_abstract' in dconfig:
        meta['abstract'] = dconfig['_abstract']
    elif '_description' in dconfig:
        meta['abstract'] = dconfig['_description']
    return meta


def ref_or_inline(source, meta, ref, inline, deps):
    filename = meta.get(ref)
    if filename:
        filename = filename.strip()
    changed = 0
    if filename:
        it_file = source / filename
        if dep_changed(it_file, deps):
            changed += 1
            dep_add(it_file, deps)
            meta[inline] = (source / filename).read_text().strip()
        elif filename in (inline, '-'):
            if filename == '-':
                meta[inline] = None
            elif inline not in meta:
                meta[inline] = None
                meta[ref] = '-'
            elif not meta.get(inline).strip():
                meta[inline] = None
                meta[ref] = '-'
        else:
            meta[ref] = None
            filename = None
    if not filename:
        filenames = list(source.glob('*.{}.txt'.format(inline)))
        if len(filenames) > 0:
            it_file = filenames[0]
            if dep_changed(it_file, deps):
                dep_add(it_file, deps)
                meta[inline] = it_file.read_text()
                meta[ref] = inline
    return changed > 0


def ref_or_first(source, meta, ref, deps, *wildcards):
    filename = meta.get(ref)
    if filename:
        filename = filename.strip()
    if filename:
        it_file = source / filename
        if it_file.exists():
            if dep_changed(it_file, deps):
                dep_add(it_file, deps)
        elif filename == '-':
            return
        else:
            meta[ref] = None
            filename = None
    if not filename:
        for wc in wildcards:
            filenames = list(source.glob(wc))
            if len(filenames) > 0:
                it_file = filenames[0]
                if dep_changed(it_file, deps):
                    dep_add(it_file, deps)
                    meta[ref] = it_file.name
                break
    return


def find_something(path, something, folder_conf, dflt):
    name = path.name
    if path.is_dir():
        name = 'folder'
    
    if folder_conf is not None:
        found = None
        if name in folder_conf:
            s = folder_conf[name]
            found = s.get(something)
        if found is not None:
            found = found.strip()
    if found and not isinstance(found, str):
        raise ValueError(found)
    if found:
        return found
    return dflt


def folder_conf(source):
    if not source.is_file():
        return None
    folder_conf = ConfigParser(interpolation=ExtendedInterpolation())
    folder_conf.optionxform = lambda option: option
    folder_conf.read(source)
    return unconf(folder_conf)


def dep_add(path, where, *, secondhand=None):
    p = str(path)
    if secondhand is None:
        if path.exists():
            where[p] = path.stat().st_mtime
        elif p in where:
            del where[p]
    else:
        if p not in where:
            where[p] = 0
        for k in secondhand:
            if secondhand[k] > where[p]:
                where[p] = secondhand[k]
    return path


def dep_changed(path, where, *, secondhand=None):
    p = str(path)
    if secondhand is None:
        if not path.exists():
            if p in where:
                return True
            else:
                return False
        elif p not in where:
            return True
        elif path.stat().st_mtime > where[p]:
            return True
        return False
    else:
        changed = False
        if p not in where:
            where[p] = 0
        for k in secondhand:
            if secondhand[k] > where[p]:
                changed = True
                break
        return changed

def is_valid_folder(source):
    if not source.is_dir():
        return False
    my_folder_conf = source / 'folder.conf'
    if not my_folder_conf.exists():
        return False
    return True

def gather_folder(data, my_folder_conf, deps):
    changed = 0
    source = my_folder_conf.parent
    found_bits = set()
    if my_folder_conf.stat().st_size == 0:
        raise ValueError('Invalid "folder.conf" size in {}'.format(my_folder_conf))
    if dep_changed(my_folder_conf, deps):
        changed += 1
        dep_add(my_folder_conf, deps)
        conf = folder_conf(my_folder_conf)
        for fname in source.glob('*'):
            if ignore_path(fname.resolve()):
                continue
            if fname.name not in conf:
                conf[fname.name] = {'title': fname.stem}
        data['conf'] = conf

        meta = reduce_configs(conf, None)
        data['folder'] = meta

        rel = data['rel_path']
        data['abstract'] = find_something(source, 'abstract', conf, None)
        title = find_something(source, 'title', conf, None)
        if title and not isinstance(title, str):
            raise ValueError(repr(title))
        if not title:
            title = get_base_name(rel).replace('_',' ')
            if title and not isinstance(title, str):
                raise ValueError(repr(get_base_name(rel)))
        data['title'] = title
    else:
        conf = data['conf']
        meta = data['folder']
        
    for genre in source.glob('*.genre'):
        field_name = 'genre/' + genre.stem
        found_bits.add(field_name)
        if dep_changed(genre, deps):
            dep_add(genre, deps)
            meta[field_name] = genre.read_text().strip().splitlines()


        purge_bits = set()
        for k in meta:
            if k.startswith('genre/'):
                if k not in found_bits:
                    purge_bits.add(k)
    else:
        meta = data['folder']

    if not isinstance(data['title'], str):
        raise ValueError(repr(data))

    if ref_or_inline(source, meta, 'showcase_lyrics', 'lyrics', deps):
        changed += 1
    if ref_or_inline(source, meta, 'chapbook_poem', 'poem', deps):
        changed += 1
    if ref_or_first(source, meta, 'showcase_audio', deps, '*.spx', '*.ogg', '*.mp3'):
        changed += 1

    return changed > 0


def find_decent_basename(conf, source, meta):
    ''' Find an acceptible basename (for HTML file)
        
        This assumes old file structure, which should be a given when
        `redirect_from_dir` is used in `folder.conf`.
        
        Modern use should favor explicit names.
    '''
    basename = None
    for mcheck in ('showcase_audio', 'showcase_lyrics', 'showcase_image'):
        if mcheck in meta:
            basename_bits = meta[mcheck].rsplit('.', 1)
            if len(basename_bits) < 2:
                continue
            basename = basename_bits[0]
            break
    if basename:
        return basename
    for k in conf:
        for ext in ('.lyrics.txt', '.mp3', '.spx', '.ogg', '.flac', '.mid', '.ly', '.song', '.midi', '.vtt'):
            if k.endswith(ext):
                basename = k[:-len(ext)]
                break
        if basename:
            break
    if basename:
        return basename
    parent = source.parent
    for f in parent.glob('*'):
        if ignore_path(k) or not f.is_file():
            continue
        if len(f.suffixes) == 1:
            basename = f.stem
        else:
            basename = f.name[:-len(''.join(f.suffixes))]
        break
    return basename
    

def clean_redirect(source, data, conf, meta, deps):
    if 'redirect' not in conf:
        return
    redirect = conf['redirect']
    title = data.get('title', meta.get('title', source.name))
    rel = Path(data['rel_path'])
    redirect_list = []
    for k in redirect:
        if k and k[0] == '_':
            continue
        if k == 'redirect_from_dir' or k.startswith('redirect_from_dir/'):
            from_dir = Path(redirect[k])
            basename = find_decent_basename(conf, source, meta)
            redirect_list.append(from_dir / basename)
        elif k == 'redirect_from' or k.startswith('redirect_from/'):
            redirect_list.append(redirect[k])
        else:
            raise ValueError('In {} found unknown redirect value {}'.format(
                                source, k))
    if redirect_list:
        data['redirect'] = redirect_list
    return


def load(source, data):
    if not is_valid_folder(source):
        return None
    if data is None or data.get('loader') != 'folder':
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']
    data['loader'] = 'folder'

    rel = rel_from_root(source)
    data['rel_path'] = rel
    data['is_file'] = False

    changed = 0
    if gather_folder(data, source / 'folder.conf', deps):
        changed += 1
    clean_redirect(source, data, data['conf'], data['folder'], deps)

    folder_txt = source / 'folder.txt'
    if dep_changed(folder_txt, deps):
        dep_add(folder_txt, deps)
        changed += 1
        prefix = None
        if folder_txt.exists():
            prefix = folder_txt.read_text()
        data['prefix'] = prefix

#    if str(source) == '/Users/yam655/src/yam655/j/2019-01/07_r2052':
#        raise ValueError(repr(data))
    if not data or not data.get('folder'):
        raise ValueError(repr(source))
    return data, changed > 0


