#!/usr/bin/env python3

import glob
from pathlib import Path
from configparser import ConfigParser
import re

from .config import rel_from_root, unconf, get_product, save_product, get_service, save_service, get_cache
from .config import get_keyword, save_keyword, get_category, save_category
from .meta_utils import ignore_path, clean_gen_path
from .load_rst import find_title_rst
from .deps import *

IN_JOURNAL = re.compile(r'j/....-../.._r....')

def reduce_configs(config, meta):
    fconfig = {}
    if meta is None:
        meta = {}

    for x in fconfig:
        meta[x] = fconfig[x]

    if 'folder' in config:
        fconfig = config['folder']
    if 'artifact_name' in fconfig:
        meta['title'] = fconfig['artifact_name']
    elif 'title' in fconfig:
        meta['title'] = fconfig['title']
    if 'artifact_description' in fconfig:
        meta['abstract'] = fconfig['artifact_description']
    elif 'artifact_abstract' in fconfig:
        meta['abstract'] = fconfig['artifact_abstract']
    elif 'abstract' in fconfig:
        meta['abstract'] = fconfig['abstract']
    if 'keywords' in fconfig and 'keyword' not in fconfig:
        meta['keyword'] = fconfig['keywords']
    return meta


def ref_or_inline(source, meta, ref, inline, deps):
    filename = meta.get(ref)
    if filename:
        filename = filename.strip()
    changed = 0
    if filename:
        it_file = source / filename
        if dep_changed(it_file, deps):
            changed += 1
            dep_add(it_file, deps)
            meta[inline] = (source / filename).read_text().strip()
        elif filename in (inline, '-'):
            if filename == '-':
                meta[inline] = None
            elif inline not in meta:
                meta[inline] = None
                meta[ref] = '-'
            elif not meta.get(inline).strip():
                meta[inline] = None
                meta[ref] = '-'
        else:
            meta[ref] = None
            filename = None
    if not filename:
        filenames = list(source.glob('*.{}.txt'.format(inline)))
        if len(filenames) > 0:
            it_file = filenames[0]
            if dep_changed(it_file, deps):
                dep_add(it_file, deps)
                meta[inline] = it_file.read_text()
                meta[ref] = inline
        filenames = list(source.glob('*_{}.txt'.format(inline)))
        if len(filenames) > 0:
            it_file = filenames[0]
            if dep_changed(it_file, deps):
                dep_add(it_file, deps)
                meta[inline] = it_file.read_text()
                meta[ref] = inline
    return changed > 0


def ref_or_first(source, meta, ref, deps, *wildcards):
    filename = meta.get(ref)
    if filename:
        filename = filename.strip()
    if filename:
        it_file = source / filename
        if it_file.exists():
            if dep_changed(it_file, deps):
                dep_add(it_file, deps)
        elif filename == '-':
            return
        else:
            meta[ref] = None
            filename = None
    if not filename:
        for wc in wildcards:
            filenames = list(source.glob(wc))
            if len(filenames) > 0:
                it_file = filenames[0]
                if dep_changed(it_file, deps):
                    dep_add(it_file, deps)
                    meta[ref] = it_file.name
                break
    return


def find_something(path, something, folder_conf, dflt):
    name = path.name
    if path.is_dir():
        name = 'folder'
    
    if folder_conf is not None:
        found = None
        if name in folder_conf:
            s = folder_conf[name]
            found = s.get(something)
        if found is not None:
            found = found.strip()
    if found and not isinstance(found, str):
        raise ValueError(found)
    if found:
        return found
    return dflt


def folder_conf(source):
    if not source.is_file():
        return None
    folder_conf = ConfigParser()
    folder_conf.optionxform = lambda option: option
    folder_conf.read(source)
    return unconf(folder_conf)


def is_valid_folder(source):
    if not source.is_dir():
        return False
    my_folder_conf = source / 'folder.conf'
    if not my_folder_conf.exists():
        return False
    ignore = source / "IGNORE"
    if ignore.exists():
        return False
    return True

def gather_folder(data, my_folder_conf, deps):
    changed = 0
    source = my_folder_conf.parent
    found_bits = set()
    if my_folder_conf.stat().st_size == 0:
        # raise ValueError('Invalid "folder.conf" size in {}'.format(my_folder_conf))
        return changed

    if dep_changed(my_folder_conf, deps):
        changed += 1
        dep_add(my_folder_conf, deps)
        conf = folder_conf(my_folder_conf)
        for fname in source.glob('*'):
            if ignore_path(fname.resolve()):
                continue
            if fname.name not in conf:
                conf[fname.name] = {'title': fname.stem}
        data['conf'] = conf

        meta = reduce_configs(conf, None)
        data['folder'] = meta

        rel = data['rel_path']
        data['abstract'] = find_something(source, 'abstract', conf, None)
        title = find_something(source, 'title', conf, None)
        if title and not isinstance(title, str):
            raise ValueError(repr(title))
        if not title:
            title = source.name.replace('_',' ')
        data['title'] = title

        data['_override'] = {}
        for baby in my_folder_conf.parent.glob('*'):
            if baby.name in conf:
                data['_override'][baby.name] = conf[baby.name]
    else:
        conf = data['conf']
        meta = data['folder']

    for prod in meta, conf.get('product', {}):
        for prodref in ('see_also', ):
            f = prod.get(prodref)
            if f:
                prod[prodref] = f.splitlines()

    for prod in meta, conf.get('product', {}):
        for prodref in ('image', 'image.txt', 'image.gif', 'image.thumb',
                        'windows', 'macos', 'linux', 'python', 'bash', 'sh'):
            f = prod.get(prodref)
            if not f:
                continue
            fref = source / f
            prod[prodref] = str(fref)
            if dep_changed(fref, deps):
                dep_add(fref, deps)
                changed += 1
                if fref.exists() and prodref.endswith('.txt'):
                    fbody = prodref + '/body'
                    prod[fbody] = fref.read_text()
        #if 'windows' in prod:
            #raise ValueError(repr(prod))

    for genre in source.glob('*.genre'):
        field_name = 'genre/' + genre.stem
        found_bits.add(field_name)
        if dep_changed(genre, deps):
            dep_add(genre, deps)
            changed += 1
            meta[field_name] = genre.read_text().strip().splitlines()

        purge_bits = set()
        for k in meta:
            if k.startswith('genre/'):
                if k not in found_bits:
                    purge_bits.add(k)
    else:
        meta = data['folder']

    if not isinstance(data['title'], str):
        raise ValueError(repr(data))

    if ref_or_inline(source, meta, 'showcase_transcript', 'transcript', deps):
        changed += 1
    if ref_or_inline(source, meta, 'showcase_lyrics', 'lyrics', deps):
        changed += 1
    if ref_or_inline(source, meta, 'chapbook_poem', 'poem', deps):
        changed += 1
    if ref_or_first(source, meta, 'showcase_audio', deps, '*.spx', '*.ogg', '*.mp3'):
        changed += 1

    return changed > 0


def find_decent_basename(conf, source, meta):
    ''' Find an acceptible basename (for HTML file)
        
        This assumes old file structure, which should be a given when
        `redirect_from_dir` is used in `folder.conf`.
        
        Modern use should favor explicit names.
    '''
    basename = None
    for mcheck in ('showcase_audio', 'showcase_lyrics', 'showcase_image', 'chapbook_poem', 'showcase_transcript'):
        if mcheck in meta:
            basename_bits = meta[mcheck].rsplit('.', 1)
            if len(basename_bits) < 2:
                continue
            basename = basename_bits[0]
            break
    if basename:
        return basename
    for k in conf:
        for ext in ('_lyrics.txt', '.lyrics.txt', '_transcript.txt', '.transcript.txt', '.mp3', '.spx', '.ogg', '.flac', '.mid', '.ly', '.song', '.midi', '.vtt'):
            if k.endswith(ext):
                basename = k[:-len(ext)]
                break
        if basename:
            break
    if basename:
        return basename
    parent = source.parent
    for f in parent.glob('*'):
        if ignore_path(k) or not f.is_file():
            continue
        if len(f.suffixes) == 1:
            basename = f.stem
        else:
            basename = f.name[:-len(''.join(f.suffixes))]
        break
    return basename
    

def clean_redirect(source, data, conf, meta, deps):
    if 'redirect' not in conf:
        return
    redirect = conf['redirect']
    title = data.get('title', meta.get('title', source.name))
    rel = Path(data['rel_path'])
    redirect_list = []
    for k in redirect:
        if k and k[0] == '_':
            continue
        if k == 'redirect_from_dir' or k.startswith('redirect_from_dir/'):
            from_dir = Path(redirect[k])
            basename = find_decent_basename(conf, source, meta)
            redirect_list.append(from_dir / basename)
        elif k == 'redirect_from' or k.startswith('redirect_from/'):
            redirect_list.append(redirect[k])
        else:
            raise ValueError('In {} found unknown redirect value {}'.format(
                                source, k))
    if redirect_list:
        data['redirect'] = redirect_list
    return

def _prop_categories(main_data, my_data, out_data):
    if 'categories' not in my_data and 'categories' in main_data:
        out_data['categories'] = main_data['categories']

def _gather_ordinal(my_data, out_data, rel_path, source):
    out_data.setdefault('--ordinal', {})
    out_data.setdefault('__ordinal', {})
    if my_data.get('ordinal') == '-':
        out_data['--ordinal'][rel_path]= str(source)
    elif not my_data.get('ordinal', '').strip():
        out_data['__ordinal'][rel_path]= str(source)

def _gather_defaults(what, my_data, source, out_data, rel_path):
    out_data.setdefault(what, {})
    out_what_data = out_data[what]
    for p in my_data:
        if p is None or my_data[p] is None:
            raise ValueError(repr(my_data))
        gather_gen_bits(p, my_data[p], source, out_what_data, out_data, rel_path)
    gather_gen_bits('__TOUCHED__', '1', source, out_what_data, out_data, rel_path)
    if 'abstract' in out_what_data:
        # print (f'GATHER_DEFAULTS {what} {source} {out_what_data["abstract"]}')
        out_data['abstract'] = out_what_data['abstract'][0]

def gather_product(data, source, deps):
    what = 'product'
    my_data = data.get('conf',{}).get(what,{})
    prodname = my_data.get('title','').strip()
    if not my_data or not prodname:
        return
    out_data = get_product(prodname)
    if not out_data:
        out_data = {'title': prodname}
    if 'rel_path' not in out_data:
        out_data['rel_path'] = clean_gen_path(f'{what}:{prodname}')
    rel_path = str(data['rel_path'])
    _prop_categories(data, my_data, out_data)
    prod = _gather_defaults(what, my_data, source, out_data, rel_path)
    _gather_ordinal(my_data, out_data, rel_path, source)
    save_product(prodname, out_data)

def gather_service(data, source, deps):
    conf = data['conf']
    if 'service' not in conf:
        return
    service = conf['service']
    if 'title' not in service:
        raise ValueError('No title in "service" for {}'.format(source))
    prodname = conf['service']['title']
    prod1 = get_service(prodname)
    if not prod1:
        prod1 = {'title': prodname}
    if 'rel_path' not in prod1:
        prod1['rel_path'] = clean_gen_path('service:{}'.format(prodname))
    if 'options' not in prod1:
        prod1['options'] = {}
    rels = str(data['rel_path'])
    prod1.setdefault('service', {})
    prod = prod1['service']
    prod1.setdefault('__ordinal', {})
    prod1.setdefault('--ordinal', {})
    for p in service:
        gather_gen_bits(p, service[p], source, prod, prod1, rels)
    gather_gen_bits('__TOUCHED__', '1', source, prod, prod1, rels)
    for c in conf:
        if c.startswith('option/'):
            c0 = conf[c]
            c0_id = c.split('/',1)[-1]
            prod1['options'].setdefault(c0_id,{})
            prod2 = prod1['options'][c0_id]
            prod2.setdefault('option',{})
            prod3 = prod2['option']
            for c1 in c0:
                gather_gen_bits(c1, c0[c1], source, prod3, prod2, rels)
    if 'abstract' in prod:
        prod1['abstract'] = prod['abstract'][0]
    if service.get('ordinal', '-'):
        prod1['--ordinal'][rels]= str(source)
    elif not service.get('ordinal', '').strip():
        prod1['__ordinal'][rels]: str(source)
    save_service(prodname, prod1)

def gather_gen_bits(p, new_data, source, prod, prod1, rels):
    if new_data is None:
        raise ValueError('new_data should never be None')
    if isinstance(new_data,str):
        new_data = new_data.strip()
    if not new_data:
        return

    if p == 'see_also':
        x = prod.get(p)
        if not x or rels > x[1]:
            new_data = set(new_data)
            prod[p] = (new_data, rels, str(source))
        else:
            for x1 in new_data:
                prod[p][0].add(x1)

    elif p == 'ordinal':
        if new_data != '-':
            idx = int(new_data)
            if p not in prod1:
                prod1[p] = {}
            obsolete = prod1[p].get(idx)
            new_value = str(source)
            if new_value and obsolete and new_value < obsolete:
                obsolete, new_value = new_value, obsolete
            elif obsolete != new_value:
                prod1[p][idx] = new_value
            else:
                obsolete = None
#           if obsolete:
#               if '__obsolete' not in prod1:
#                   prod1['__obsolete'] = {}
#               prod1['__obsolete'][rels] = obsolete
            if obsolete:
                if '__obsolete' not in prod1:
                    prod1['__obsolete'] = {}
                if idx not in prod1['__obsolete']:
                    prod1['__obsolete'][idx] = set()
                prod1['__obsolete'][idx].add(obsolete)
    elif p == 'image.txt':
        pbody = p + '/body'
        prod.setdefault(p, {})
        prod.setdefault(pbody, {})
        x = prod.get(pbody)
        nfile = None
        if '\n' not in new_data:
            nfile = source / new_data
            if nfile.exists():
                new_data = nfile.read_text()
            else:
                nfile = None
        if not x or rels > x[1]:
            prod[p] = (nfile, rels, str(source))
            prod[pbody] = (new_data, rels, str(source))
    elif p in ('notes', 'announcement'):
        prod1.setdefault(p, {})
        x = prod1.get(p)
        nfile = None
        if '\n' not in new_data:
            nfile = source / new_data
            if nfile.exists():
                new_data = nfile.read_text()
            else:
                nfile = None
        if new_data:
            prod1[p][rels] = new_data
    elif p == '__TOUCHED__':
        x = prod1.get(p)
        if not x or rels > x:
            prod1[p] = rels
    else:
        x = prod.get(p)
        #if p == 'image':
            #raise ValueError(f'\n\nUpdating "{p} for "{str(source)}"\n')
        if not x or rels > x[1]:
            if new_data:
                if new_data == '""' or new_data == "''":
                    prod[p] = ("", rels, str(source))
                else:
                    prod[p] = (new_data, rels, str(source))


def gather_review(data, source, deps):
    conf = data['conf']
    if 'review' not in conf:
        return
    review = conf['review']
    if 'title' not in review:
        raise ValueError('No title in "review" for {}'.format(source))
    prodname = conf['review']['title']
    prod1 = get_review(prodname)
    if not prod1:
        prod1 = {'title': prodname}
    if 'rel_path' not in prod1:
        prod1['rel_path'] = clean_gen_path('review:{}'.format(prodname))
    rels = str(data['rel_path'])
    prod1.setdefault('review', {})
    prod = prod1['review']
    prod1.setdefault('__ordinal', {})
    prod1.setdefault('--ordinal', {})
    for p in review:
        gather_gen_bits(p, review[p], source, prod, prod1, rels)
    gather_gen_bits('__TOUCHED__', '1', source, prod, prod1, rels)
    if 'abstract' in prod:
        prod1['abstract'] = prod['abstract'][0]
    if review.get('ordinal', '-'):
        prod1['--ordinal'][rels]= str(source)
    elif not review.get('ordinal', '').strip():
        prod1['__ordinal'][rels]: str(source)
    save_review(prodname, prod1)

def gather_keyword(data, source, deps):
    folder = data.get('conf',{}).get('folder',{})
    keywords = folder.get('keyword','').splitlines()
    if not keywords:
        keywords = folder.get('keywords','').splitlines()
        if not keywords:
            return
    data['keywords'] = keywords

    for keyname in keywords:
        keyw1 = get_keyword(keyname)
        if not keyw1:
            keyw1 = {'title': keyname}
        if 'rel_path' not in keyw1:
            keyw1['rel_path'] = clean_gen_path(f'keyword:{keyname}')
        rels = str(data['rel_path'])
        keyw1.setdefault('keyword', {})
        prod = keyw1['keyword']
        keyw1.setdefault('__ordinal', {})
        gather_gen_bits('__TOUCHED__', '1', source, prod, keyw1, rels)
        keyw1['__ordinal'][rels]= str(source)
        save_keyword(keyname, keyw1)


def gather_category(data, source, deps):
    folder = data.get('conf',{}).get('folder',{})
    categories = folder.get('category','').splitlines()
    relstr = str(data['rel_path'])
    if not categories:
        if IN_JOURNAL.fullmatch(relstr):
            categories = ['uncategorized']
        else:
            return
    data['categories'] = categories

    for keyname in categories:
        keyw1 = get_category(keyname)
        if not keyw1:
            keyw1 = {'title': keyname}
        if 'rel_path' not in keyw1:
            keyw1['rel_path'] = clean_gen_path(f'category:{keyname}')
        rels = str(data['rel_path'])
        keyw1.setdefault('category', {})
        prod = keyw1['category']
        keyw1.setdefault('__ordinal', {})
        gather_gen_bits('__TOUCHED__', '1', source, prod, keyw1, rels)
        keyw1['__ordinal'][rels]= str(source)
        save_category(keyname, keyw1)



def load(source, data):
    if not is_valid_folder(source):
        return None
    if data is None or data.get('loader') != 'folder':
        data = {}
    changed = 0
    data.setdefault('deps', {})
    deps = data['deps']
    data['loader'] = 'folder'

    rel = rel_from_root(source)
    data['rel_path'] = rel
    data['is_file'] = False

    folder_txt = source / 'folder.txt'
    if dep_changed(folder_txt, deps):
        dep_add(folder_txt, deps)
        changed += 1
        prefix = None
        if folder_txt.exists():
            prefix = folder_txt.read_text()
        if find_title_rst(prefix):
            data['prefix/rst'] = True
        data['prefix'] = prefix

    changed = 0
    relstr = str(rel)
    if gather_folder(data, source / 'folder.conf', deps):
        clean_redirect(source, data, data['conf'], data['folder'], deps)
        gather_product(data, source, deps)
        gather_service(data, source, deps)
        gather_review(data, source, deps)
        gather_keyword(data, source, deps)
        gather_category(data, source, deps)
        changed += 1
    else:
        gather_category(data, source, deps)

    #if not data or not data.get('folder'):
        #raise ValueError(repr([source, data]))
    return data, changed > 0


