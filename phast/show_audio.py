#!/usr/bin/env python3

from .config import get_cache


def showcase_audio(bucket, source, out, localcache):
    folder = bucket.get('folder', {})
    conf = bucket.get('conf', {})
    audio = conf.get('audio', {})
    def get_shared_field(audio_name, song_name, *lilypond_names):
        ret = None
        if audio_name and audio_name in audio:
            ret = audio[audio_name]
        elif song_name and song_name in song:
            ret = song[song_name]
        else:
            for lilypond_name in lilypond_names:
                if lilypond_name in lilypond:
                    ret = lilypond[lilypond_name]
                    break
        return ret

    cache = get_cache()
    song = {}
    lilypond = {}
    audio_shorts = {}
    a_song = None
    a_ly = None
    for f in conf:
        n = conf[f]
        if 'in-showcases' in n:
            if 'audio' in n.get('in-showcases').splitlines():
                audio_shorts[n.get('short', f)] = f
                if f.endswith('.song'):
                    a_song = f
                elif f.endswith('.ly'):
                    a_ly = f
    if a_song or a_ly and localcache:
        for k in localcache:
            ks = str(k)
            if a_song and ks.endswith(a_song):
                c = cache[ks]
                song = c['song']
            elif a_ly and ks.endswith(a_ly):
                c = cache[ks]
                lilypond = c['lilypond']

    if not audio_shorts:
        return

    out.nl()
    t = 'Audio'
    if 'genre/music' in folder:
        t = 'Music'
    out.section(t)
    out.nl()

    out.playable_audio(audio_shorts)

    if audio_shorts:
#       if len(audio_shorts) == 1:
#           lshort, lfilename = audio_shorts.popitem()
#           brief = conf[lfilename]
#           out.rel_link(source / lfilename, brief['title'], brief.get('abstract'), 
#                        is_file=True, add_suffix=False, context='Download: {}')
#       else:

        out.add('Download:')
        out.nl()
        for lshort in audio_shorts:
            lfilename = audio_shorts[lshort]
            if isinstance(lfilename, str):
                link = source / lfilename
                brief = conf[lfilename]
            else:
                link = lfilename
                brief = localcache[lfilename]
                lfilename = link.name
            title = brief.get('title', lfilename)
            abstract = brief.get('abstract', '')
            cntx = '    - {}'
            if lshort not in title and lshort not in abstract:
                cntx = '    - ({}) {}'.format(lshort, '{}')
            out.rel_link(link, title, abstract, only_title=True, # absolute
                            is_file=True, add_suffix=False, context=cntx)
        out.nl()

    #if lilypond:
        #raise ValueError(repr(list(lilypond.keys())))
    #if 'albumProduct' in lilypond:
    #    raise ValueError(repr(lilypond))
    for tags in [('Title', 'TITLE', 'TITLE', 'songTitle'),
                 ('Dedication', None, 'DEDICATION', 'songDedication'),
                 ('Inspired By', None, None, 'inspiredBy'),
                 ('Tune Of', None, None, 'tuneIs', 'tuneOf'),
                 ('Artist', 'ARTIST', None),
                 ('Duration', 'duration', None),
                 ('Album', 'ALBUM', 'ALBUMTITLE', 'albumTitle'),
                 ('Songbook', None, 'BOOKTITLE', 'bookTitle', 'albumProduct'),
                 ('Composer', 'COMPOSER', None, 'tuneComposer'),
                 ('Lyricist', 'POET', 'POET', 'songPoet'),
                 ('Arranger', None, None, 'songArranger', 'tuneArranger'),
                 ('Album Artist', 'ALBUMARTIST', None),
                 ('Music Genre', 'genre/music', None),
                 ('Audio Genre', 'genre/audio', None),
                 ('Date', 'DATE', 'POSTDATE', 'songCopyright')]:
        got = get_shared_field(*tags[1:])
        out.list_or_not(tags[0], got)
    out.nl()

