#!/usr/bin/env python3

from .config import get_project, save_project


def showcase_audio(bucket, out, localcache):
    folder = bucket.get('folder', {})
    conf = bucket.get('conf', {})
    audio = conf.get('audio', {})
    song = conf.get('song', {})
    lilypond = conf.get('lilypond', {})
    def get_shared_field(audio_name, song_name, *lilypond_names):
        ret = None
        if audio_name and audio_name in audio:
            ret = audio[audio_name]
        elif song_name and song_name in song:
            ret = song[song_name]
        else:
            for lilypond_name in lilypond_names:
                if lilypond_name in lilypond:
                    ret = lilypond[lilypond_name]
                    break
        return ret

    audio_shorts = {}
    for f in conf:
        n = conf[f]
        if 'in-showcases' in n:
            if 'audio' in n.get('in-showcases').splitlines():
                audio_shorts[n.get('short', f)] = f
    if 'showcase_audio' not in folder or not audio:
        return
        
    out.nl()
    t = 'Audio'
    if 'genre/music' in folder:
        t = 'Music'
    out.section(t)
    out.nl()

    out.playable_audio(audio_shorts)

    if audio_shorts:
        if len(audio_shorts) == 1:
            lshort, lfilename = audio_shorts.popitem()
            brief = conf[lfilename]
            out.rel_link(lfilename, brief['title'], brief['abstract'], 
                         is_file=True, context='Download: {}')
        else:
            out.add('Download:')
            out.nl()
            for lshort in audio_shorts:
                lfilename = audio_shorts[lshort]
                brief = conf[lfilename]
                title = brief['title']
                abstract = brief['abstract']
                cntx = '    - {}'
                if lshort not in title and lshort not in abstract:
                    cntx = '    - ({}) {}'.format(lshort, '{}')
                out.rel_link(lfilename, brief['title'], brief['abstract'], 
                             is_file=True, context=cntx)
            out.nl()

    prod = {}
    product = folder.get('product')
    prod_rel = None
    if product:
        prod = get_product(product)
        if not prod:
            prod['title'] = product
        prod.setdefault('entries', set())
        prod_rel = folder.get('rel_path')
        if prod_rel not in prod['entries']:
            prod['entries'].add(rel)
        prod_title = product
        if prod:
            if 'title' in prod:
                prod_title = prod['title']
        if prod_rel:
            lfilename = Path(prod_rel)
            out.rel_link(lfilename, prod_title, prod.get('abstract',''), 
                     is_file=False, context='Product:')


    for tags in [('Title', 'TITLE', 'TITLE', 'songTitle'),
                 ('Dedication', None, 'DEDICATION', 'songDedication'),
                 ('Inspired By', None, None, 'inspiredBy'),
                 ('Tune Of', None, None, 'tuneIs', 'tuneOf'),
                 ('Artist', 'ARTIST', None),
                 ('Duration', 'duration', None),
                 ('Album', 'ALBUM', 'ALBUMTITLE', 'albumTitle'),
                 ('Songbook', None, 'BOOKTITLE', 'bookTitle'),
                 ('Composer', 'COMPOSER', None, 'tuneComposer'),
                 ('Lyricist', 'POET', 'POET', 'songPoet'),
                 ('Arranger', None, None, 'songArranger', 'tuneArranger'),
                 ('Album Artist', 'ALBUMARTIST', None),
                 ('Music Genre', 'genre/music', None),
                 ('Audio Genre', 'genre/audio', None),
                 ('Date', 'DATE', 'POSTDATE', 'songCopyright')]:
        got = get_shared_field(*tags[1:])
        out.list_or_not(tags[0], got)
    out.nl()

