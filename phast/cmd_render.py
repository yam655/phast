#!/usr/bin/env python3

from docutils import core, io
from pathlib import Path
import sys
import time
import os
import json
import argparse
import shutil
import codecs
import textwrap

from .process import process_list
from .walk import build_list
from .config import get_config, verbose, get_cache, get_briefs, get_index, get_render_cache, read_config
from .syn_atom import atom
from .syndication import process_syndication

def render_file_list(inbox):
    totfiles = len(inbox)
    curfiles = 0
    dest_html = get_config('html', as_path=True)
    dest_gopher = get_config('gopher', as_path=True)
    dest_gemini = get_config('gemini', as_path=True)
    verbose('{} files total to process.'.format(totfiles))
    for data in process_list(inbox):
        curfiles += 1
        source = Path(data.get('rel_path'))
        if not source:
            raise RuntimeError('No "rel_path" in: {}'.format(repr(data)))
        srcstr = str(source)
        if len(srcstr) > 60:
            verbose('* {:02d}% ...{:70.57}'.format(curfiles * 100 // totfiles, srcstr[-57:]), end='\r')
        else:
            verbose('* {:02d}% {:70.70}'.format(curfiles * 100 // totfiles, srcstr), end='\r')

        if dest_html:
            if 'html' in data:
                destdir = dest_html / source
                if data.get('is_file'):
                    destdir.parent.mkdir(parents=True, exist_ok=True)
                    destfile = destdir.parent / (destdir.name + '.html')
                else:
                    destdir.mkdir(parents=True, exist_ok=True)
                    destfile = destdir / 'index.html'
                destfile.write_text(data['html'])

        if dest_gopher:
            if 'gophermap' in data:
                if data.get('is_file'):
                    raise ValueuError('{} is a file. Can not be a Gophermap.'.format(source))
                destdir = dest_gopher / source
                destdir.mkdir(parents=True, exist_ok=True)
                destfile = destdir / 'gophermap'
                destfile.write_text(data['gophermap'])
            elif 'gopher' in data:
                destdir = dest_gopher / source
                if data.get('is_file'):
                    destdir.parent.mkdir(parents=True, exist_ok=True)
                    destfile = destdir.parent / (destdir.name + '.txt')
                else:
                    raise ValueuError('Found Gopher data for {}, should be a file, not folder'.format(source))
                destfile.write_text(data['gopher'])

        if dest_gemini:
            if 'index.gmi' in data:
                if data.get('is_file'):
                    raise ValueuError('{} is a file. Index files should go in folders.'.format(source))
                destdir = dest_gemini / source
                destdir.mkdir(parents=True, exist_ok=True)
                destfile = destdir / 'index.gmi'
                destfile.write_text(data['index.gmi'])
            elif 'gmi' in data:
                destdir = dest_gemini / source
                if data.get('is_file'):
                    destdir.parent.mkdir(parents=True, exist_ok=True)
                    destfile = destdir.parent / (destdir.name + '.gmi')
                else:
                    raise ValueuError('Found Gemini data for {}, should be a file, not folder'.format(source))
                destfile.write_text(data['gmi'])

        if 'create' in data:
            v = data['create']
            if isinstance(v, str):
                v = {'https': v, 'gopher': v, 'gemini': v}
            if dest_html and v.get('https'):
                destfile = dest_html / source
                destdir = destfile.parent
                destdir.mkdir(parents=True, exist_ok=True)
                destfile.write_text(v['https'])
            if dest_gopher and v.get('gopher') and dest_gopher != dest_html:
                destfile = dest_gopher / source
                destdir = destfile.parent
                destdir.mkdir(parents=True, exist_ok=True)
                destfile.write_text(v['https'])
            if (dest_gemini and v.get('gemini') and dest_gopher != dest_gemini
                    and dest_gemini != dest_html):
                destfile = dest_gemini / source
                destdir = destfile.parent
                destdir.mkdir(parents=True, exist_ok=True)
                destfile.write_text(v['gemini'])

        dests = set()
        for d in (dest_html, dest_gopher, dest_gemini):
            if d:
                dests.add(d)

        if 'source' in data:
            dest = source
            if data.get('is_file'):
                d = dest.parent / (source.stem + '.rst')
            else:
                d = dest / 'index.rst'

            for d1 in dests:
                if d1 is None:
                    continue
                destfile = d1 / d
                destdir = destfile.parent
                destdir.mkdir(parents=True, exist_ok=True)
                destfile.write_text(data['source'])
            del d

        if data.get('copy'):
            copy_source, copy_rel = data['copy']
            copy_source = Path(copy_source)
            for d1 in dests:
                if not d1:
                    continue
                destfile = d1 / copy_rel
                destdir = destfile.parent
                destdir.mkdir(parents=True, exist_ok=True)
                if not destfile.exists() or destfile.stat().st_mtime < copy_source.stat().st_mtime:
                    shutil.copyfile(copy_source, destfile)

def render_from(start):
    filelist = build_list(start)
    verbose("Processing files ...")
    render_file_list(filelist)
    verbose(' {:10} {:60}'.format('', ''), end='\r')

def cmd_render(args):
    cache = get_cache()
    index = get_index()
    if args.force:
        print('Clearing caches...')
        cache.clear()
        index.clear()
        briefs = get_briefs()
        briefs.clear()
        render_cache = get_render_cache()
        render_cache.clear()
    print('Rendering...')
    verbose("Creating file list...")
    if args.local:
        render_from(Path('.').resolve())
    else:
        render_from(get_config('source'))
    if args.force:
        print('Syndication...')
        config = read_config()
        feeds = config.get('feeds', {})
        for filen in feeds:
            line = feeds[filen]
            l = line.split('|')
            title = l[0].strip()
            if len(l) < 2:
                continue
            cnt = int(l[1])
            tpes = None
            if len(l) > 2:
                tpes = l[-1].strip().split()
                if not tpes:
                    tpes = None
            process_syndication(atom, filen, cnt, tpes, title=title)


