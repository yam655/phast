#!/usr/bin/env python3

from urllib.parse import urlparse
from docutils import core, io
from pathlib import Path
import sys
import time
import os
import json
import argparse
import shutil
import codecs
import textwrap

from .process import process_list
from .walk import build_list
from .config import get_config, verbose, get_cache

def render_file_list(inbox):
    urlbase = get_config('url')
    totfiles = len(inbox)
    curfiles = 0
    dest_html = get_config('html')
    dest_gopher = get_config('gopher')
    verbose('{} files total to process.'.format(totfiles))
    for data in process_list(inbox):
        curfiles += 1
        source = data.get('rel_path')
        if not source:
            raise RuntimeError('No "rel_path" in: {}'.format(repr(data)))
        srcstr = str(source)
        if len(srcstr) > 60:
            verbose('* {:02d}% ...{:70.57}'.format(curfiles * 100 // totfiles, srcstr[-57:]), end='\r')
        else:
            verbose('* {:02d}% {:70.70}'.format(curfiles * 100 // totfiles, srcstr), end='\r')

        if 'html' in data:
            destdir = dest_html / source
            if data.get('is_file'):
                destdir.parent.mkdir(parents=True, exist_ok=True)
                destfile = destdir.parent / (destdir.name + '.html')
            else:
                destdir.mkdir(parents=True, exist_ok=True)
                destfile = destdir / 'index.html'
            destfile.write_text(data['html'])
        if 'gophermap' in data:
            if data.get('is_file'):
                raise ValueuError('{} is a file. Can not be a Gophermap.'.format(source))
            destdir = dest_gopher / source
            destdir.mkdir(parents=True, exist_ok=True)
            destfile = destdir / 'gophermap'
            destfile.write_text(data['gophermap'])
        elif 'gopher' in data:
            destdir = dest_gopher / source
            if data.get('is_file'):
                destdir.parent.mkdir(parents=True, exist_ok=True)
                destfile = destdir.parent / (destdir.name + '.txt')
            else:
                raise ValueuError('Found Gopher data for {}, should be a file, not folder'.format(source))
            destfile.write_text(data['gopher'])

        if 'source' in data:
            dest = source
            if data.get('is_file'):
                d = dest.parent / (source.stem + '.rst')
            else:
                d = dest / 'index.rst'
            for d1 in (dest_html, dest_gopher):
                if d1 is None:
                    continue
                destfile = d1 / d
                destdir = destfile.parent
                destdir.mkdir(parents=True, exist_ok=True)
                destfile.write_text(data['source'])
            del d

        if data.get('copy'):
            copy_source, copy_rel = data['copy']
            copy_source = Path(copy_source)
            for d1 in (dest_html, dest_gopher):
                if not d1:
                    continue
                destfile = d1 / copy_rel
                destdir = destfile.parent
                destdir.mkdir(parents=True, exist_ok=True)
                if not destfile.exists() or destfile.stat().st_mtime < copy_source.stat().st_mtime:
                    shutil.copyfile(copy_source, destfile)

def render_from(start):
    filelist = build_list(start)
    verbose("Processing files ...")
    render_file_list(filelist)
    verbose(' {:10} {:60}'.format('', ''), end='\r')

def cmd_render(args):
    if args.force:
        cache = get_cache()
        cache.clear()
    print('Rendering...')
    verbose("Creating file list...")
    render_from(get_config('source'))




