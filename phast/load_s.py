#!/usr/bin/env python3

import glob
from configparser import ConfigParser, ExtendedInterpolation

from .config import rel_from_root, get_base_name, unconf
from .load_folder import dep_add, dep_changed

def is_valid_folder(path):
    if not path.is_dir():
        return False
    json_path = path / 'service.json'
    if not json_path.exists():
        return False
    return True

def load_service(data, json_path, deps):
    source = json_path.parent
    changed = False
    if dep_changed(json_path, deps):
        changed = True
        dep_add(json_path, deps)
        rel = rel_from_root(source)
        data['rel_path'] = rel
        data['mode'] = 'service'

        conf = json.loads(source.read_text())
        conf['conf'] = conf

        meta = None
        for key in conf.keys():
            if key in ('abstract', 'title'):
                continue
            elif meta is None:
                meta = key
            else:
                meta = None
                break
        if meta:
            data['type'] = meta
            meta = conf[meta]
            data['norm'] = meta

        if 'abstract' in conf:
            data['abstract'] = conf['abstract']
        elif 'abstract' in meta:
            data['abstract'] = meta['abstract']

        if 'prefix' in conf:
            data['prefix'] = conf['prefix']
        elif 'prefix' in meta:
            data['prefix'] = meta['prefix']

        if 'title' in conf:
            data['title'] = conf['title']
        elif 'title' in meta:
            data['title'] = meta['title']
        elif not title:
            data['title'] = get_base_name(rel).replace('_',' '),
    return changed


def load(source, data):
    if not is_valid_folder(source):
        return None
    if data is None:
        data = {}
    data.setdefault('deps', {})
    deps = data['deps']
    changed = False

    json_path = path / 'service.json'
    if load_service(data, json_path, deps):
        changed = True

    folder_txt = source / 'folder.txt'
    if dep_changed(folder_txt, deps):
        changed = True
        dep_add(folder_txt, deps)
        prefix = None
        if folder_txt.exists():
            prefix = folder_txt.read_text()
        data['prefix'] = prefix

    return data, changed


